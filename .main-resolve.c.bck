#include "ecp-cell.h"
#include "ecp-solver.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <math.h>

static void
_print_usage( FILE * f
            , const char * appName) {
    fputs("ECAL LMS calibration app.\nUsage:\n\t $ "
         , f );
    fputs(appName, f);
    fputs("  -S <solver:str> solver expression.\n", f);
    fputs("  -n <nEvents:int> read only N events\n", f);
    fputs("  -f <fitFile:str> path to scaling factors file.\n", f);
    // ...
}

static void
_reset_event_data(struct ecp_EDepData * data) {
    for(struct ecp_EDepData * c = data; !c->isTerminal; ++c) {
        c->measuredValue = nan("0");
    }
}

static size_t
_read_file( const char * filePath
          , size_t nMaxEvents
          , struct ecp_Solver * solver
          , struct ecp_EDepData * evData
          , const unsigned short * segmentation
          ) {
    FILE * inFile = fopen(filePath, "r");
    if(!inFile) {
        perror("Could not open file for reading:");
        return 0;
    }

    uint8_t xIdx, yIdx, zIdx;
    uint32_t runNo, spillNo;
    uint64_t eventInSpillNo;
    double ampMax, sum;
    size_t cellOffset;

    size_t nEventInFile;
    size_t nHitsRead = 0;
    for(nEventInFile = 0; 0 == nMaxEvents || nEventInFile < nMaxEvents; ++nEventInFile) {
        fread(&xIdx, 1, 1, inFile);
        if(xIdx != 0xff) {
            /* Read hit */
            fread(&yIdx, 1, 1, inFile);
            fread(&zIdx, 1, 1, inFile);
            fread(&ampMax, sizeof(ampMax), 1, inFile);
            fread(&sum,    sizeof(sum),    1, inFile);
            /* Get the cell instance and set the data */
            cellOffset = ecp_idx2offset(xIdx, yIdx, zIdx, segmentation);
            evData[cellOffset].measuredValue = ampMax;
            /* normalize: */
            evData[cellOffset].measuredValue /= evData[cellOffset].calibCoeff;
            ++nHitsRead;
        } else {
            /* New event reading */
            if(0 != nHitsRead) {
                /* add event */
                solver->add_event(solver->state, evData);
            }
            fread(&runNo,   sizeof(runNo),   1, inFile);
            fread(&spillNo, sizeof(spillNo), 1, inFile);
            fread(&eventInSpillNo, sizeof(eventInSpillNo), 1, inFile);
            _reset_event_data(evData);
        }
    }
    fclose(inFile);
    return nEventInFile;
}

int
main(int argc, char * argv[]) {
    int opt, rc;
    unsigned short segmentation[3] = {5, 6, 2};  // TODO: configurable, support 6x6 design
    double dims[3] = {1, 1, 1};  // TODO: configurable, make use of it
    struct ecp_Solver * solver = NULL;
    char * solverExpr = NULL, * scalingFactorsFilePath = NULL, * c = NULL;
    size_t nEventsToRead = 0;

    /*
     * Configure application */
    while(-1 != (opt = getopt(argc, argv, "hS:n:"))) {
        switch (opt) {
        case 'h' :
            _print_usage(stdout, argv[0]);
            return EXIT_SUCCESS;
        case 'S' :
            if(solver) {
                fputs("Error: multiple solvers specified.\n", stderr);
                return EXIT_FAILURE;
            }
            solverExpr = optarg;
            break;
        case 'n' :
            nEventsToRead = strtoul(optarg, &c, 0);
            if(NULL != c && '\0' != *c) {
                fprintf(stderr, "Error: extra symbols on the tail for"
                        " -n option argument: \"%s\"\n", optarg);
                exit(EXIT_FAILURE);
            }
            break;
        default:
            fprintf( stderr
                   , "Unrecognized option -%c; run with -h to get help.\n"
                   , (char) opt);
            return EXIT_FAILURE;
        }
    }
    if(optind >= argc) {
        fputs("Error: one or more input files needed (none given).\n", stderr);
        return EXIT_FAILURE;
    }
    if(!solverExpr) {
        fputs("Error: option `-S <solver>' is not set.\n", stderr);
        return EXIT_FAILURE;
    }

    /* 
     * Init event data holding array */
    struct ecp_EDepData * evData;
    rc = ecp_init_cells(&evData, segmentation, dims);
    if(rc != 0) {
        fprintf(stderr, "Error: failed to init reading data with"
                " segmentation %ux%ux%u and dimensions (%fx%fx%f)...\n"
                , segmentation[0], segmentation[1], segmentation[2]
                , dims[0], dims[1], dims[2] );
        return EXIT_FAILURE;
    }

    /* 
     * Read the scaling factors file if provided, otherwise set the scaling
     * factors to 0.*/
    if(scalingFactorsFilePath) {
        FILE * scaleFactorsFile = fopen(scalingFactorsFilePath, "r");
        if(!scalingFactorsFilePath) {
            perror("Failed to read scales factor file:");
            return EXIT_FAILURE;
        }
        size_t nLine = 1;
        char lineBuf[512];
        unsigned short xIdx, yIdx;
        float scaleMain, sigmaMain, scalePrs, sigmaPrs;
        while(fgets(lineBuf, sizeof(lineBuf), scaleFactorsFile) == lineBuf) {
            if( *scalingFactorsFilePath == '#'
             || *scalingFactorsFilePath == '\0'
             || *scalingFactorsFilePath == '\n') {
                ++nLine;
                continue;
            }
            size_t nRead = sscanf(lineBuf, "%hu %hu %f %f %f %f", &xIdx, &yIdx
                    , &scalePrs, &sigmaPrs    //< NOTE: this order
                    , &scaleMain, &sigmaMain  //< comes from fit_peaks.C
                    );
            if(nRead != 6) {
                fprintf(stderr, "Error: wrong tokens parsed from %s:%zu (%zu"
                        " instead of 6).\n", scalingFactorsFilePath, nLine, nRead);
                return EXIT_FAILURE;
            }
            size_t offset;
            /* set preshower scale factor */
            if(isnan(scalePrs) || !isfinite(scalePrs)) {
                fprintf(stderr, "Error: line %s:%zu provided nan/not a finite"
                        " number for preshower part of cell %hux%hu).\n"
                        , scalingFactorsFilePath, nLine, xIdx, yIdx);
                return EXIT_FAILURE;
            }
            offset = ecp_idx2offset(xIdx, yIdx, 0, segmentation);
            evData[offset].calibCoeff = scalePrs;
            /* set main part scale factor */
            if(isnan(scaleMain) || !isfinite(scaleMain)) {
                fprintf(stderr, "Error: line %s:%zu provided nan/not a finite"
                        " number for main part of cell %hux%hu).\n"
                        , scalingFactorsFilePath, nLine, xIdx, yIdx);
                return EXIT_FAILURE;
            }
            offset = ecp_idx2offset(xIdx, yIdx, 1, segmentation);
            evData[offset].calibCoeff = scaleMain;
            /* ... todo: make use of sigmas somehow? */
            ++nLine;
        }
    } else {
        for(unsigned short i = 0; i < segmentation[0]; ++i) {
            for(unsigned short j = 0; j < segmentation[1]; ++j) {
                for(unsigned short k = 0; k < segmentation[2]; ++k) {
                    size_t offset = ecp_idx2offset(i, j, k, segmentation);
                    evData[offset].calibCoeff = 1;
                }
            }
        }
    }

    /* 
     * Instantiate the solver */
    printf("Info: creating a solver based on expression \"%s\" for"
            " segmentation %ux%ux%u and dimensions (%fx%fx%f)...\n"
            , solverExpr
            , segmentation[0], segmentation[1], segmentation[2]
            , dims[0], dims[1], dims[2]
            );
    solver = ecp_solver_create(solverExpr, segmentation);
    if(!solver) {
        fprintf(stderr, "Error: couldn't create a solver from expression"
                " \"%s\".\n", solverExpr);
        return EXIT_FAILURE;
    }

    /* 
     * Read the files */
    puts("# files in use:\n");
    for(; optind < argc; ++optind) {
        /* TODO: file may affect the filling stencil here with the interface
         * appendix not yet defined ... */
        /* Read events and show number of read events */
        size_t nEventsRead = _read_file(argv[optind], nEventsToRead, solver, evData, segmentation);
        printf("#  %s ... %zu\n", argv[optind], nEventsRead);
    }

    double * x;
    size_t nX;
    if(0 != (rc = solver->eval(solver->state, &x, &nX))) {
        fprintf(stderr, "Error: solver's eval() returned %d.\n", rc);
        exit(EXIT_FAILURE);
    }

    puts("# Obtained coefficients:\n");
    for(size_t i = 0; i < nX; ++i) {
        printf("x[%zu] = %14.7e\n", i, x[i]);
    }

    return EXIT_SUCCESS;
}
