#include <cstdlib>
#include <iostream>
#include <fstream>

#include <TFile.h>
#include <TH1.h>
#include <TF1.h>
#include <TCanvas.h>

struct CellScalingFactor {
    float factor, error;
};

int
fit_peaks( const char * inputFilesDir="/home/rdusaev/xxx/ecal-calib/"
         , const char * tableFilePath="./calib-runs-2023A-2.txt"
         , const char * outputFile="./ecalScalingFactors.dat"
         ) {
    CellScalingFactor factors[5][6][2];
    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 6; ++j) {
            for(int k = 0; k < 2; ++k) {
                factors[i][j][k].factor = factors[i][j][k].error = std::nan("0");
            }
        }
    }

    TCanvas * cnv = new TCanvas("fits", "Fitted maxAmp");
    cnv->Divide(3, 4);

    {
        std::ifstream ifs(tableFilePath);
        if(!ifs) {
            std::cerr << "Error opening file \"" << tableFilePath << "\"."
                << std::endl;
            return 1;
        }
        std::string line;
        unsigned int runNo, xIdx, yIdx;  // calib run number
        float calibMeans[2];
        float cx, cy;  // cells center x,y
        float chargeMaxRatioMain[2], chargeMaxRatioPrs[2];
        float fitRange[2], prsMean;
        int nRead;
        size_t lineNo = 0, padNo = 1;
        while(std::getline(ifs, line)) {
            std::cout << "xxx " << line << std::endl;  // XXX
            ++lineNo;
            if('#' == *line.begin()) continue;
            nRead = sscanf(line.c_str(), "%u %f,%f %ux%u %f %f %f %f %f %f %f %f %f"
                  , &runNo
                  , &cx, &cy
                  , &xIdx, &yIdx
                  , calibMeans, calibMeans + 1  // rough estimations from shift crew
                  , chargeMaxRatioMain, chargeMaxRatioMain + 1
                  , chargeMaxRatioPrs, chargeMaxRatioPrs + 1
                  , fitRange, fitRange + 1
                  , &prsMean
                  );
            if(nRead != 14) {
                std::cerr << "Failed to parse line " << lineNo << " of "
                    << tableFilePath << std::endl;
                break;
            }
            factors[xIdx][yIdx][0].factor = calibMeans[0];
            factors[xIdx][yIdx][1].factor = calibMeans[1];

            assert(xIdx < 5);
            assert(yIdx < 6);
            if( std::isnan(fitRange[0]) || std::isnan(fitRange[1])
             || 0 == xIdx || 0 == yIdx
             || 4 == xIdx || 5 == yIdx) {
                // for periphery use mean values from shift measurements. Less
                // reliable, but something at least
                if(!std::isnan(prsMean))
                    factors[xIdx][yIdx][0].factor = prsMean;
                continue;
            }

            //std::cout << xIdx << "x" << yIdx << ": " << prsMean << std::endl;
            char tfilePath[128];
            snprintf( tfilePath, sizeof(tfilePath), "%s/processed-%dx%d-%d.root"
                    , inputFilesDir, xIdx, yIdx, runNo );
            TFile * tf = TFile::Open(tfilePath);
            if(!tf) {
                std::cerr << "No file \"" << tfilePath << "\"; cells " << xIdx << "x"
                    << yIdx << " skipped." << std::endl;
                continue;
            }
            char hstPath[128];
            snprintf( hstPath, sizeof(hstPath)
                    , "/ECAL/x%u/y%u/max-value-%u-%u-1", xIdx, yIdx, xIdx, yIdx );
            TObject * hst_ = tf->Get(hstPath);
            if(!hst_) {
                std::cerr << "No histogram \"" << hstPath << "\" in file \""
                    << tfilePath << "\", cells " << xIdx << "x"
                    << yIdx << " skipped." << std::endl;
                //tf->Close();
                continue;
            }
            TH1 * hst = dynamic_cast<TH1*>(hst_);
            assert(hst);
            cnv->cd(padNo++);
            TF1 *f1 = new TF1("f1","gaus",fitRange[0], fitRange[1]);
            hst->Fit("f1","QR");
            hst->Draw();
            f1->Draw("SAME");
            double * fitPars = f1->GetParameters();
            factors[xIdx][yIdx][1].factor = fitPars[1];
            factors[xIdx][yIdx][1].error  = fitPars[2];
            if(!std::isnan(prsMean))
                factors[xIdx][yIdx][0].factor = prsMean;
            //std::cout << "sum: " << fitPars[0]
            //          << ", mean: " << fitPars[1]
            //          << ", sigma: " << fitPars[2]
            //          << std::endl;

            //delete f1;
            //tf->Close();
        }
    }

    {
        FILE * outFile = fopen(outputFile, "w");
        for(int i = 0; i < 5; ++i) {
            for(int j = 0; j < 6; ++j) {
                fprintf( outFile, "%3d %3d %12.5e %12.5e %12.5e %12.5e\n"
                       , i, j
                       , factors[i][j][0].factor, factors[i][j][0].error
                       , factors[i][j][1].factor, factors[i][j][1].error
                       );
            }
        }
        fclose(outFile);
    }
    return 0;
}

int
main(int argc, char * argv[]) {
    if(argc != 4) {
        std::cerr << "Usage:" << std::endl
            << "   $ " << argv[0] << "<input-files-dir> <table-file> <output-file>" << std::endl;
        return EXIT_FAILURE;
    }
    fit_peaks(argv[1], argv[2], argv[3]);
    return EXIT_SUCCESS;
}

