#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include <iostream>
#include <list>
#include <libgen.h>  // for dirname()


//template<typename T, typename ... ArgsT> TObject *
//sum_up_histos( std::list<TObject *> objs, ArgsT ... args ) {
//    T * newObj = new T(args...);
//    return newObj;
//}

int main(int argc, char * argv[]) {
    if( argc < 4 ) {
        std::cerr
            << "Usage:" << std::endl
            << "    $ " << argv[0]
            << " <out-file> <hst-tpath> <file1> [<file2> [<file3> ...]]"
            << std::endl;
        return 1;
    }

    //
    // Retrieve basic objects
    const std::string tPath = argv[2]
                    , outFile = argv[1]
                    ;
    std::list<std::string> inputs;
    for(int i = 4; i < argc; ++i) {
        inputs.push_back(argv[i]);
    }

    std::vector<TObject *> ptrs;
    for(auto inPath : inputs) {
        TFile * f = TFile::Open(inPath.c_str(), "READ");
        if(!f) {
            std::cerr << "Error: failed to open file \""
                << inPath << "\", skipped." << std::endl;
            continue;
        }
        TObject * hst_ = f->Get(tPath.c_str());
        if(!hst_) {
            std::cerr << "Error: no object \"" << tPath
                << "\" in file \"" << inPath << "\", skipped." << std::endl;
            continue;
        }
        ptrs.push_back(hst_);
    }

    //
    // Open output file
    TFile * outf = TFile::Open(outFile.c_str(), "UPDATE");
    //TObject * newObj = sum_up_histos<TH2>(ptrs, "result", "result", 10, 0, 1);
    TH1 * p = nullptr;
    for(auto & ptr : ptrs) {
        TH1 * th1Ptr = dynamic_cast<TH1*>(ptr);
        if(!th1Ptr) {
            std::cerr << "Error: object type cast failure" << std::endl;
            return -1;
        }
        if(!p) {
            p = th1Ptr;
            continue;
        }
        p->Add(th1Ptr);
    }
    if(p) {
        // get path for the object
        char * objpath_ = strdup(tPath.c_str())
           , * objpath = dirname(objpath_);
        outf->cd();
        //std::cout << "XXX \"" << objpath << "\"" << std::endl;  // XXX
        if(!outf->Get(objpath)) {
            outf->mkdir(objpath);
        }
        outf->cd(objpath);
        p->Write();
    }

    outf->Close();

    return 0;
}

