#include "ecp-cell.h"
#include "ecp-generator.h"
#include "ecp-types.h"
#include "ecp-file.h"
#include "ecp-solver.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <math.h>

#include <stddef.h>

static void
_print_usage( FILE * f
            , const char * appName) {
    fputs("ECAL LMS calibration testing app.\nUsage:\n\t $ "
         , f );
    fputs(appName, f);
    fputs("-i <inputFile>\n", f);
    fputs("  -i <inputFile:str> sets the file to read events.\n", f);
    fputs("  -s <solver:str> solver expression.\n", f);
    fputs("  -n <nEvents:int> read only N events\n", f);
    // ...
}

int
main(int argc, char * argv[]) {
    int opt;
    unsigned short segmentation[3];
    double dims[3];
    bool inIsBinary = false;
    FILE * inFile = stdout;
    struct ecp_Solver * solver = NULL;
    char * solverExpr = NULL, *c = NULL;
    size_t nEventsToRead = 0;
    while(-1 != (opt = getopt(argc, argv, "hbi:s:n:"))) {
        switch (opt) {
        case 'h' :
            _print_usage(stdout, argv[0]);
            return EXIT_SUCCESS;
        case 'b' :
            inIsBinary = true;
            break;
        case 's' :
            if(solver) {
                fputs("Error: multiple solvers specified.\n", stderr);
                return EXIT_FAILURE;
            }
            solverExpr = strdup(optarg);
            break;
        case 'i' :
            inFile = fopen(optarg, "r");
            if(!inFile) {
                int ec = errno;
                fprintf( stderr
                       , "Error opening file \"%s\": %s"
                       , *optarg == '+' ? optarg + 1 : optarg
                       , strerror(ec)
                       );
                exit(EXIT_FAILURE);
            }
            break;
        case 'n' :
            nEventsToRead = strtoul(optarg, &c, 0);
            if(NULL != c && '\0' != *c) {
                fprintf(stderr, "Error: extra symbols on the tail for"
                        " -N option argument: \"%s\"\n", optarg);
                exit(EXIT_FAILURE);
            }
            break;
        default:
            fprintf( stderr
                   , "Unrecognized option -%c; run with -h to get help.\n"
                   , (char) opt);
            return EXIT_FAILURE;
        }
    }

    if(!inFile) {
        fputs("Error: input file is not set.\n", stderr);
        exit(EXIT_FAILURE);
    }
    if(!solverExpr) {
        fputs("Error: -s <solver> argument is not provided.\n", stderr);
        exit(EXIT_FAILURE);
    }

    /*
     * Read data
     */

    struct ecp_EDepData * data = NULL;
    segmentation[0] = segmentation[1] = segmentation[2] = 0;
    dims[0] = dims[1] = dims[2];
    size_t nEvents = 0;
    int rc;
    while(true) {
        unsigned short segmentationNew[3];
        double dimsNew[3];
        if(inIsBinary) {
            assert(false);  // TODO: read binary header
        } else {
            rc = ecp_file_read_text_header(inFile, segmentationNew, dimsNew, &data);
            if(0 != rc) {
                fputs("Exit due to header reading failure.\n", stderr);
                exit(EXIT_FAILURE);
            }
        }
        if(0 == segmentation[0]) {
            memcpy(segmentation, segmentationNew, sizeof(segmentation));
            memcpy(dims, dimsNew, sizeof(dimsNew));
            solver = ecp_solver_create(solverExpr, segmentation);
            free(solverExpr);
            if(!solver) {
                fputs("Error: can't create a sover.\n", stderr);
                return EXIT_FAILURE;
            }
        } else {
            if( 0 != memcmp(segmentation, segmentationNew, sizeof(segmentation))
             || 0 != memcmp(dims, dimsNew, sizeof(dims))) {
                fputs("Error: new header in file does not match the previous"
                        " dimension/segmentation. Reading abrupt.\n", stderr);
                break;
            }
        }
        printf( "Info: resolve app is set up to read %ux%ux%u cells with"
                " dimensions %.2ex%.2ex%.2e\n"
              , segmentation[0], segmentation[1], segmentation[2]
              , dims[0], dims[1], dims[2]
              );
        bool newHeader = false;
        while(true) {
            size_t cPos = ftell(inFile);
            if(inIsBinary) {
                assert(false);  // TODO: binary data fmt
            } else {
                rc = ecp_file_read_text(inFile, data);
            }
            if(rc < 0) {
                fseek(inFile, cPos, SEEK_SET);
                newHeader = true;
                break;
            } else if(rc == 1) {
                break;
            }
            if(0 != (rc = solver->add_event(solver->state, data, nan("0")))) {
                fputs("Error: failed to add event to a solver.\n", stderr);
                exit(EXIT_FAILURE);
            }
            ++nEvents;
            if(0 != nEventsToRead && nEvents >= nEventsToRead) {
                newHeader = false;
                break;
            }
        }
        if(!newHeader) break;
    }
    printf("Info: %zu events read from file.\n", nEvents);


    /*
     * Resolve
     */
    double * x;
    const char ** names;
    size_t nX;
    if(0 != (rc = solver->eval(solver->state, &x, &nX, &names))) {
        fprintf(stderr, "Error: solver's eval() returned %d.\n", rc);
        exit(EXIT_FAILURE);
    }
    free(names);

    for(size_t i = 0; i < nX; ++i) {
        printf("x[%zu] = %14.7e\n", i, x[i]);
    }
    
    return 0;
}


