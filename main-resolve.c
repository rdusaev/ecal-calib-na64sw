#include "ecp-cell.h"
#include "ecp-cumulist.h"
#include "ecp-solver.h"
#include "ecp-profile.h"
#include "ecp-stencil.h"

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdint.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <libgen.h>
#include <regex.h>
#include <stdbool.h>

#ifndef ECP_RESOLVE_COLUMN_LABEL_BUF_LEN
#   define ECP_RESOLVE_COLUMN_LABEL_BUF_LEN 64
#endif


static void
_print_usage( FILE * f
            , const char * appName) {
    fputs("ECAL LMS calibration app.\nUsage:\n\t $ "
         , f );
    fputs(appName, f);
    fputs(" [OPTIONS] <inFile1> [<inFile2> [<inFile3> ...]]", f);
    fputs("  -h print this message and quit.\n", f);
    fputs("  -S <solver:str> solver expression.\n", f);
    fputs("  -o <outut file>\n", f);
    fputs("  -p <shower profile> in use\n", f);
    fputs("  -n <nEvents:int> read only N events\n", f);
    fputs("  -s <fitFile:str> path to scaling factors file.\n", f);
    fputs("  -b <beamEnergy:int> beam energy used for calibrations.\n", f);
    fputs("\nWith event peaks data provided with <inFileN> files this"
          " application fills matrix A in the overdetermined linear"
          " system A*x=b and tries to resolve it with requested"
          " method. Result affected by choosen type of solver, scaling"
          " factors, etc.\nSolver expression (provided with -S option"
          " must be ... .\nIf number of events is given with -N, only that"
          " number of events will be read from each <inFileN> -- this helps"
          " to reduce amount of memory consumed during the evaluation, but"
          " affects precision of the result.\n"
          , f);
    // ...
}

static void
_reset_event_data(struct ecp_EDepData * data) {
    for(struct ecp_EDepData * c = data; !c->isTerminal; ++c) {
        assert(c->columnLabel);
        c->measuredValue = nan("0");
        c->columnLabel[0] = '\0';
    }
}

static size_t
_read_file( const char * filePath
          , size_t nMaxEvents
          , struct ecp_Solver * solver
          , struct ecp_EDepData * evData
          , const unsigned short * segmentation
          , double bValue
          , const char * labelPrefix
          ) {

    FILE * inFile = fopen(filePath, "r");
    if(!inFile) {
        perror("Could not open file for reading:");
        return 0;
    }

    uint8_t xIdx, yIdx, zIdx;
    uint32_t runNo, spillNo;
    uint64_t eventInSpillNo;
    double ampMax, sum, width;
    size_t cellOffset;

    size_t nEventInFile = 0;
    size_t nHitsRead = 0;
    const size_t offsetMax = segmentation[0]*segmentation[1]*segmentation[2];
    //for(nEventInFile = 0; 0 == nMaxEvents || nEventInFile < nMaxEvents; ++nEventInFile) {
    while(true) {
        if(1 != fread(&xIdx, 1, 1, inFile)) {
            break;
        }
        /* check if first byte is event begin marker (otherwise it is hit) */
        if(xIdx != 0xff) {
            /* Read hit */
            fread(&yIdx, 1, 1, inFile);
            fread(&zIdx, 1, 1, inFile);
            fread(&ampMax, sizeof(double), 1, inFile);
            fread(&sum,    sizeof(double), 1, inFile);
            fread(&width,  sizeof(double), 1, inFile);
            if(ampMax > 1e9) {
                fprintf(stderr, "Error in file %s: suspiciously large value met for cell"
                        " %hux%hux%hu at event %u-%u.%lu: amp. value is %e.\n"
                        , filePath
                        , xIdx, yIdx, zIdx
                        , runNo, spillNo, eventInSpillNo
                        , ampMax );
                exit(EXIT_FAILURE);
            }
            if(!isfinite(ampMax)) continue;
            //if(!isfinite(sum)   ) continue;
            /*printf("Hit %hux%hux%hu amp.max=%e, sum=%e, width=%e\n"
                    , xIdx, yIdx, zIdx, ampMax, sum, width );*/
            /* Get the cell instance and set the data */
            cellOffset = ecp_idx2offset(xIdx, yIdx, zIdx, segmentation);
            if(cellOffset > offsetMax) {
                fprintf(stderr, "Error in file %s: an item of index %hux%hux%hu"
                        " (resulting linear offset of %zu) found while assumed"
                        " segmentation is %hux%hux%hu (resulting only in %zu"
                        " items. Item skept.\n"
                        , filePath
                        , xIdx, yIdx, zIdx, cellOffset
                        , segmentation[0], segmentation[1], segmentation[2], offsetMax
                        );
                exit(EXIT_FAILURE);
            }
            assert(isfinite(ampMax));
            assert(isfinite(evData[cellOffset].calibCoeff));
            evData[cellOffset].measuredValue = ampMax;
            /* normalize (`calibCoeff` might be set previously to scaling
             * factor): */
            evData[cellOffset].measuredValue /= evData[cellOffset].calibCoeff;
            assert(evData[cellOffset].columnLabel);
            snprintf(evData[cellOffset].columnLabel, ECP_RESOLVE_COLUMN_LABEL_BUF_LEN
                    , "%s %u %u %u", labelPrefix, zIdx, xIdx, yIdx);
            ++nHitsRead;
        } else {
            /* this is "event begin"; check if we're done with events count */
            if(0 != nMaxEvents && nMaxEvents == nEventInFile) break;
            ++nEventInFile;
            /* New event reading */
            if(0 != nHitsRead) {
                #ifndef NDEBUG
                for(const struct ecp_EDepData * c = evData; ; ++c) {
                    assert(c->columnLabel);
                    assert(isnan(c->measuredValue) || '\0' != c->columnLabel[0]);
                    if(c->isTerminal) break;
                }
                #endif
                assert(isfinite(bValue));
                assert(bValue != 0.);
                /* add event */
                solver->add_event(solver->state, evData, bValue);
            }
            fread(&runNo,   sizeof(runNo),   1, inFile);
            fread(&spillNo, sizeof(spillNo), 1, inFile);
            fread(&eventInSpillNo, sizeof(eventInSpillNo), 1, inFile);
            /*printf("Event %u-%u.%lu:\n", runNo, spillNo, eventInSpillNo);*/
            _reset_event_data(evData);
        }
    }
    fclose(inFile);
    return nEventInFile;
}

static int
_read_scaling_factors(
          const char * scalingFactorsFilePath
        , const unsigned short * segmentation
        , struct ecp_EDepData * evData
        ) {
    if(!scalingFactorsFilePath || '\0' == *scalingFactorsFilePath) return -1;
    FILE * scaleFactorsFile = fopen(scalingFactorsFilePath, "r");
    if(!scaleFactorsFile) {
        perror("Failed to read scales factor file:");
        return -1;
    }
    size_t nLine = 1;
    char lineBuf[512];
    unsigned short xIdx, yIdx, zIdx;
    float mean, sigma;
    while(fgets(lineBuf, sizeof(lineBuf), scaleFactorsFile) == lineBuf) {
        if( *scalingFactorsFilePath == '#'
         || *scalingFactorsFilePath == '\0'
         || *scalingFactorsFilePath == '\n') {
            ++nLine;
            continue;
        }
        //< Fields order comes from presets/get-primary-peak-width.yaml.in
        size_t nRead = sscanf(lineBuf, "%hu-%hu-%hu %f %f"
                , &xIdx, &yIdx, &zIdx
                , &mean, &sigma
                );
        if(nRead != 5) {
            fprintf(stderr, "Error: wrong tokens parsed from %s:%zu (%zu"
                    " instead of 5).\n", scalingFactorsFilePath, nLine, nRead);
            return -2;
        }
        size_t offset;
        /* set preshower scale factor */
        if((!isfinite(mean)) || (!isfinite(sigma))) {
            fprintf(stderr, "Error at %s:%zu: nan/not a finite"
                    " number for cell %hux%hux%hu).\n"
                    , scalingFactorsFilePath, nLine, xIdx, yIdx, zIdx);
            return -2;
        }
        if(xIdx > segmentation[0] || yIdx > segmentation[1] || zIdx > segmentation[2]) {
            fprintf(stderr, "Error at %s:%zu: cell %hux%hux%hu is outside of"
                    " segmentation %hux%hux%hu).\n"
                    , scalingFactorsFilePath, nLine
                    , xIdx, yIdx, zIdx
                    , segmentation[0], segmentation[1], segmentation[2]
                    );
            return -3;
        }
        offset = ecp_idx2offset(xIdx, yIdx, zIdx, segmentation);
        evData[offset].calibCoeff = mean;
        /* ... todo: make use of sigmas somehow? */
        printf( "Info: using scaling factor on %hux%hux%hu: %.3f\n"
              , xIdx, yIdx, zIdx, mean );
        ++nLine;
    }
    fclose(scaleFactorsFile);
    return 0;
}

int
main(int argc, char * argv[]) {
    int opt, rc;
    bool scalingUsed = false; 
    unsigned short segmentation[3] = {5, 6, 2};  // TODO: configurable, support 6x6 design
    double dims[3] = {1, 1, 1};  /* sizes (in [cm]) */  // TODO: configurable, make use of it
    struct ecp_Solver * solver = NULL;
    char * solverExpr = NULL, * c = NULL, * outFileName = NULL;
    size_t nEventsToRead = 0;
    double beamEnergy = 1;
    struct ecp_ShowerProfile profile = { {{0, 0}, {0, 0}, {0, 0}} };
    regex_t inFileRx;

    // ecal-calib-data-1x5.dat
    rc = regcomp( &inFileRx, "calib-dump.([0-9+])-([0-9+]).*\\.dat"
                , REG_EXTENDED | REG_ICASE );
    if(0 != rc) {
        puts("regcomp():");
        return EXIT_FAILURE;
    }

    /* 
     * Init event data array */
    struct ecp_EDepData * evData = NULL;
    unsigned int cellsFlags = 0x0
                            //| 0x1  // enables "leak terms"
                            ;
    rc = ecp_init_cells(&evData, segmentation, dims, cellsFlags);  // ... 0x1 to enable "leaks"
    for(struct ecp_EDepData * c = evData; ; ++c) {
        c->calibCoeff = 1.0;
        c->columnLabel = malloc(ECP_RESOLVE_COLUMN_LABEL_BUF_LEN);  /* TODO: configurable? cleanup? gets forwarded to columns set */
        c->columnLabel[0] = '\0';
        if(c->isTerminal) break;
    }
    if(rc != 0) {
        fprintf(stderr, "Error: failed to init reading data with"
                " segmentation %ux%ux%u and dimensions (%fx%fx%f)...\n"
                , segmentation[0], segmentation[1], segmentation[2]
                , dims[0], dims[1], dims[2] );
        return EXIT_FAILURE;
    }

    /*
     * Configure application */
    FILE * profileFile = NULL;
    while(-1 != (opt = getopt(argc, argv, "hs:S:n:b:o:p:"))) {
        switch (opt) {
        case 'h' :
            _print_usage(stdout, argv[0]);
            return EXIT_SUCCESS;
        case 'S' :
            if(solverExpr) {
                fputs("Error: multiple solvers specified.\n", stderr);
                exit(EXIT_FAILURE);
            }
            solverExpr = optarg;
            break;
        case 'b' :
            beamEnergy = strtod(optarg, &c);
            if(NULL != c && '\0' != *c) {
                fprintf(stderr, "Error: extra symbols on the tail for"
                        " -b option argument: \"%s\"\n", optarg);
                return EXIT_FAILURE;
            }
            break;
        case 'n' :
            nEventsToRead = strtoul(optarg, &c, 0);
            if(NULL != c && '\0' != *c) {
                fprintf(stderr, "Error: extra symbols on the tail for"
                        " -n option argument: \"%s\"\n", optarg);
                exit(EXIT_FAILURE);
            }
            break;
        case 's':
            if(0 != (rc = _read_scaling_factors(optarg, segmentation, evData))) {
                fprintf(stderr, "Exit due to error while parsing scales factor file \"%s\"\n"
                        , optarg );
                exit(EXIT_FAILURE);
            }
            scalingUsed = true;
            break;
        case 'o':
            if(outFileName) {
                fputs("Option -o provided twice", stderr);
                exit(EXIT_FAILURE);
            }
            outFileName = strdup(optarg);
            break;
        case 'p':
            printf("Info: shower profile in use: %s\n", optarg);
            profileFile = fopen(optarg, "r");
            if(!profileFile) {
                fprintf(stderr, "Exit due to error with opening profile file %s:"
                        " %s\n", optarg, strerror(errno));
                exit(EXIT_FAILURE);
            }
            if(0 != (rc = ecp_profile_read(profileFile, &profile, 0x0))) {
                fclose(profileFile);
                fprintf(stderr, "Exit due to error while reading profile file %s:"
                        " error code %d\n", optarg, rc);
                exit(EXIT_FAILURE);
            }
            fclose(profileFile);
            ecp_profile_print(&profile, stdout);
            break;
        default:
            fprintf( stderr
                   , "Unrecognized option -%c; run with -h to get help.\n"
                   , (char) opt);
            return EXIT_FAILURE;
        }
    }

    if(optind >= argc) {
        fputs("Error: one or more input files needed (none given).\n", stderr);
        return EXIT_FAILURE;
    }
    if(!solverExpr) {
        fputs("Error: option `-S <solver>' is not set.\n", stderr);
        return EXIT_FAILURE;
    }
    if(scalingUsed) {
        size_t nMax = segmentation[0]*segmentation[1]*segmentation[2];
        for(size_t n = 0; n < nMax; ++n) {
            if(fabs(1 - evData[n].calibCoeff) > 1e-8) continue;
            unsigned short xIdx, yIdx, zIdx;
            ecp_offset2idx(n, &xIdx, &yIdx, &zIdx, segmentation);
            fprintf(stderr, "Warning: scaling is used, but cell %hux%hux%hu"
                    " seemed to not have a scaling factor set.\n"
                    , xIdx, yIdx, zIdx );
        }
    }

    /* 
     * Instantiate the solver */
    printf("Info: creating a solver based on expression \"%s\" for"
            " segmentation %ux%ux%u and dimensions (%fx%fx%f)...\n"
            , solverExpr
            , segmentation[0], segmentation[1], segmentation[2]
            , dims[0], dims[1], dims[2]
            );
    solver = ecp_solver_create(solverExpr, segmentation);
    if(!solver) {
        fprintf(stderr, "Error: couldn't create a solver from expression"
                " \"%s\".\n", solverExpr);
        return EXIT_FAILURE;
    }

    FILE * outstream;
    if(outFileName) {
        outstream = fopen(outFileName, "w");
    } else {
        outstream = stdout;
    }

    /* 
     * Read the files */
    fputs("# files in use:\n", outstream);
    char filePathBuf[256];
    regmatch_t matches[3];
    size_t nEventsOverall = 0;
    for(; optind < argc; ++optind) {
        filePathBuf[0] = '\0';
        strncat(filePathBuf, argv[optind], sizeof(filePathBuf)-1);
        char * fileBaseName = basename(filePathBuf);
        unsigned short xIdx, yIdx;
        double bValueInUse = beamEnergy;
        assert(isfinite(bValueInUse));
        assert(bValueInUse != 0.);
        if(0 == regexec(&inFileRx, fileBaseName, sizeof(matches)/sizeof(*matches), matches, 0) ) {
            xIdx = atoi(fileBaseName + matches[1].rm_so);
            yIdx = atoi(fileBaseName + matches[2].rm_so);
            if(solver->set_primary_cell) {
                solver->set_primary_cell(solver->state, xIdx, yIdx);
            }
            /* if profile provided, it should affect the bValue
             * proportionally to the missed energy deposition */
            if(profile.index[0][0] != profile.index[0][1]) {
                const double missedFraction = ecp_profile_missed_fraction(&profile, xIdx
                        , yIdx, segmentation
                        , ecp_solver_get_filling_stencil(solver->state)
                        , true  /*< normalize, todo: do we need that to be a parameter?*/
                        );
                assert(missedFraction != 1.0);  /* out of the calorimeter?! */
                bValueInUse *= 1. - missedFraction;
            }
        } else {
            /* todo: make it more flexible? */
            fprintf(stderr, "File \"%s\" does not match expected pattern.\n"
                    , fileBaseName );
            exit(EXIT_FAILURE);
        }
        assert(isfinite(bValueInUse));
        assert(bValueInUse != 0.);
        /* Read events and show number of read events */
        size_t nEventsRead = _read_file(argv[optind], nEventsToRead, solver
                , evData, segmentation, bValueInUse, "ECAL");
        fprintf(outstream, "#  %s ... %hux%hu, %zu events, b=%.3f\n"
                , argv[optind], xIdx, yIdx, nEventsRead, bValueInUse );
        //ecp_stencil_print(stdout, ecp_solver_get_filling_stencil(solver->state));
        nEventsOverall += nEventsRead;
    }

    if(0 == nEventsOverall) {
        fputs("Error: no events read.\n", stderr);
        exit(EXIT_FAILURE);
    }

    double * x;
    size_t nX;
    const char ** xNames;
    if(0 != (rc = solver->eval(solver->state, &x, &nX, &xNames))) {
        fprintf(stderr, "Error: solver's eval() returned %d.\n", rc);
        exit(EXIT_FAILURE);
    }

    #if 1
    for(size_t i = 0; i < nX; ++i) {
        fprintf( outstream
               , "%s %10.4e\n"
               , xNames[i]
               , x[i]/evData[i].calibCoeff
               );
    }
    #else
    for(size_t i = 0; i < nX; ++i) {
        //printf("x[%zu] = %14.7e\n", i, x[i]);
        unsigned short xIdx, yIdx, zIdx;
        if(i < segmentation[0]*segmentation[1]*segmentation[2]) {
            rc = ecp_offset2idx(i, &xIdx, &yIdx, &zIdx, segmentation);
            assert(0 == rc);
            //printf("ECAL %2hu %2hu %2hu %.3f/%.3f = %10.4e\n", zIdx, xIdx, yIdx
            //        , x[i], evData[i].calibCoeff, x[i]/evData[i].calibCoeff);
            //printf("ECAL %2hu %2hu %2hu %.3f/%.3f = %10.4e\n", zIdx, xIdx, yIdx
            //        , x[i], evData[i].calibCoeff, x[i]/evData[i].calibCoeff);
            fprintf(outstream, "ECAL %2hu %2hu %2hu %10.4e\n", zIdx, xIdx, yIdx
                    , x[i]/evData[i].calibCoeff);
        } else {
            printf("# supplementary coefficient #%zu: %e\n", i, x[i]);
        }
    }
    #endif
    if(outstream != stdout)
        fclose(outstream);
    if(outFileName) free(outFileName);

    return EXIT_SUCCESS;
}
