#!/bin/bash

# Generate input file for batch script based on calibration runs ASCII table.
#   This input file represents list of arguments for each batch job and used
#   by `spread-jobs-table.sh` batch-submission script to assign arguments to
#   particular job.
# See py/generate-input.py for input table

# Syntax brief reference for `spread-jobs-table.sh':
#   -t <tag> -- a tag to label this batch
#   -i <local-file> -- ascii table to populate jobs
#   -s <filename-pattern> -- file to be collected
#   -T -- if given, works in "test local" mode
# other args are not permitted. Note, that if entry of certain syntax (`*out')
# is given, list of expected output files will be prepended.

#                                                                      _______
# ___________________________________________________________________/ Stage 1
# Run preliminary analysis to build raw (uncalibrated) spectra
#

TAG=2023mu.ecal-calib
ATTEMPT=00
CHUNK_TABLE=presets/input/2023B-40GeV.txt
CELLS_TABLE=presets/input/2023B-40GeV.cells.txt

#TAG=2024e.ecal-calib-HI
#ATTEMPT=00
#CHUNK_TABLE=presets/input/2024A-100GeV-HI.txt
#CELLS_TABLE=presets/input/2024A-100GeV-HI.cells.txt

#MAX_JOB_RUNTIME=600  # typically runs not more than 1-2min, set to 10 for safety
#/afs/cern.ch/work/r/rdusaev/htcondor-submit/scripts/spread-jobs-table.sh \
#    -t ${TAG}.1.1-${ATTEMPT} \
#    -i $CHUNK_TABLE \
#    -s 'processed.prelim.%(runID)%-%(chunkID)%.root' \
#    -s 'pedestals.%(runID)%-%(chunkID)%.dat' \
#    -- \
#        /afs/cern.ch/work/r/rdusaev/na64/analysis/ecal-calib/exec/stage-01.sh \
#            '/eos/experiment/na64/data/cdr/%(inFile)%' \
#            presets/run.d/run.01.1.get-pedestals.yaml.in \
#            'processed.prelim.%(runID)%-%(chunkID)%.root' \
#            'pedestals.%(runID)%-%(chunkID)%.dat'

#MAX_JOB_RUNTIME=600  # typically runs not more than 1-2min, set to 10 for safety
#/afs/cern.ch/work/r/rdusaev/htcondor-submit/scripts/spread-jobs-table.sh \
#    -t ${TAG}.1.2-${ATTEMPT} \
#    -i ${CELLS_TABLE} \
#    -s 'summary.prelim.%(cellX)%-%(cellY)%.root' \
#    -s 'fit-reports-%(cellX)%-%(cellY)%.pdf' \
#    -s 'fit-results-%(cellX)%-%(cellY)%.dat' \
#    -- \
#        /afs/cern.ch/work/r/rdusaev/na64/analysis/ecal-calib/exec/stage-01-aggregate.sh \
#            '%(cellX)%' '%(cellY)%' \
#            '%(chunks)%' \
#            presets/get-lsum.yaml.in \
#            ${TAG}.1.1-${ATTEMPT}

#                                                                      _______
# ___________________________________________________________________/ Stage 2
# Fit peaks, get time windows
#

#export MAX_JOB_RUNTIME=9000  # 2.5h, typically takes 30-40mins per chunk
#/afs/cern.ch/work/r/rdusaev/htcondor-submit/scripts/spread-jobs-table.sh \
#    -t ${TAG}.2.1-${ATTEMPT} \
#    -i ${CHUNK_TABLE} \
#    -s 'calib-dump.%(cellX)%-%(cellY)%.%(runID)%-%(chunkID)%.dat' \
#    -s 'summary.selection.%(cellX)%-%(cellY)%.%(runID)%-%(chunkID)%.root' \
#    -- \
#        /afs/cern.ch/work/r/rdusaev/na64/analysis/ecal-calib/exec/stage-02.sh \
#            '%(cellX)%' '%(cellY)%' \
#            '%(runID)%' '%(chunkID)%' \
#            '%(outFile_DST)%' \
#            ${TAG}.1.1-${ATTEMPT} \
#            ${TAG}.1.2-${ATTEMPT} \
#            '/eos/experiment/na64/data/cdr/%(inFile)%'

#MAX_JOB_RUNTIME=300  # typically runs not more than 1min, set to 5 for safety
#/afs/cern.ch/work/r/rdusaev/htcondor-submit/scripts/spread-jobs-table.sh \
#    -t ${TAG}.2.2-${ATTEMPT} \
#    -i ${CELLS_TABLE} \
#    -s 'time-fit-reports-%(cellX)%-%(cellY)%.pdf' \
#    -s 'time-peaks-%(cellX)%-%(cellY)%.dat' \
#    -- \
#        /afs/cern.ch/work/r/rdusaev/na64/analysis/ecal-calib/exec/stage-02-aggregate.sh \
#            '%(cellX)%' '%(cellY)%' \
#            '%(chunks)%' \
#            ${TAG}.2.1-${ATTEMPT}

#                                                                      _______
# ___________________________________________________________________/ Stage 3
# Clusterize pulses by time, select events, fit waveforms
#

export MAX_JOB_RUNTIME=1200  # 20min, typically takes 1-5min
/afs/cern.ch/work/r/rdusaev/htcondor-submit/scripts/spread-jobs-table.sh \
    -t ${TAG}.3.1-${ATTEMPT} \
    -i ${CHUNK_TABLE} \
    -s 'calib-dump.%(cellX)%-%(cellY)%.%(runID)%-%(chunkID)%.dat' \
    -s 'summary.calib-dump.%(cellX)%-%(cellY)%.%(runID)%-%(chunkID)%.root' \
    -- \
        /afs/cern.ch/work/r/rdusaev/na64/analysis/ecal-calib/exec/stage-03.sh \
            '%(cellX)%' '%(cellY)%' \
            '%(runID)%' '%(chunkID)%' \
            '%(outFile_DST)%' \
            ${TAG}.2.1-${ATTEMPT} \
            ${TAG}.2.2-${ATTEMPT}

#                                                                      _______
# ___________________________________________________________________/ Stage 4
# --- Get calibration data input in the format used by solver ----------------

# ... TODO (curently done locally)

