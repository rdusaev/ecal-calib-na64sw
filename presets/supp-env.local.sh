# To be sourced in case of local tests
export PATH="${PATH}:/home/rdusaev/projects/cern/na64/local/bin"
export P348_CONDDB_DIR=/home/rdusaev/projects/cern/na64/local/opt/p348-daq.current/p348reco/conddb
export NA64SW_PREFIX=/home/rdusaev/projects/cern/na64/local
export LD_LIBRARY_PATH=/home/rdusaev/projects/cern/na64/local/lib/
