# To be sourced by node running scripts
source /cvmfs/na64.cern.ch/sft/LCG_103/x86_64-centos7-gcc12-opt/this-env.sh
# TODO: should be brought by module (needed for libna64avro.so to load libna64avro-common.so)!
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/cvmfs/na64.cern.ch/sft/LCG_103/x86_64-centos7-gcc12-opt/na64sw/na64sw-0.4.dev/lib/na64sw-extensions/"
module load na64sw/0.4.dev 
