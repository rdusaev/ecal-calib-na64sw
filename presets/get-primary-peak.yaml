---
#
# Template file for na64sw-get-fts utility
#
# Used at the end of the second phase of stage-2 of ECAL
# calibration (stage-2.2) to obtain fitted primary peak position on the
# spectra of the cell being calibrated. Should be used on `processed.root`
# file containing spectra of filtered raw amplitudes (calibration input) and
# Moyal width fit parameter.
# For given xIdx and yIdx na64sw-get-fts with this configuration will
# retrieve highest peak and fit it within given range (maxGaussianFit.window).
# Results are:
#   <xIdx>-<yIdx>-peaks.dat -- CSV file containing peak mean and sigma
#   <xIdx>-<yIdx>-[01]-peaks.pdf -- fit result plot used for QA
#
#   <xIdx>-<yIdx>-width.dat -- CSV file containing profile's mean width and sigma
#   <xIdx>-<yIdx>-[01]-width.pdf -- width fit result plot used for QA
#
# NOTE: this file needs to be completed within %(...)% substitutions.

findMax:
  # NOTE: this procedure dumps multiple rows per item
  routine: featurePoints1D
  # Width of Gaussian convolution kernel to smooth the spectrum prior to
  # characteristic points lookup
  variance: 25
  ROOTClassName: TH1
  nameLookup:
    #expression: "^max-value-(([0-9])-([0-9])-([0-9]))$"
    expression: "^max-value-(3-2-([01]))$"
    groups:
      detIndex: 1
  output:
    path: "3-2-peaks.dat"
    # Following fields are available:
    #     {x} {y} {nSample} {pointType}
    # If `maxGaussianFit' is enabled also available:
    #     {gausConstant[Err]} {gausMean[Err]} {gausSigma[Err]}
    format: "{detIndex} {gausMean} {gausSigma}\n"
    # This node can be added to fit found maxima with Gaussian model within
  # given window
  maxGaussianFit:
    # Number of maxima to consider. Maxima will be considered in descending
    # order, so by specifying, say 2, the procedure will attempt to fit two
    # maxima with largest amplitude
    applyTo: 1
    # Relative window (in histogram's units) to do fit at
    window: [-500, 500]
    # If this parameter is set, it must be a template string for .pdf file
    # names where fitting results will be placed
    report: "{detIndex}-peaks.pdf"

findWidth:
  # NOTE: this procedure dumps multiple rows per item
  routine: featurePoints1D
  # Width of Gaussian convolution kernel to smooth the spectrum prior to
  # characteristic points lookup
  variance: 1.2
  ROOTClassName: TH1
  nameLookup:
    #expression: "^max-value-(([0-9])-([0-9])-([0-9]))$"
    expression: "^msadc-moyal-sigma-([\\w]+:3-2-([01]))$"
    groups:
      detector: 1
      zIdx: 2
  output:
    path: "3-2-width.dat"
    # Following fields are available:
    #     {x} {y} {nSample} {pointType}
    # If `maxGaussianFit' is enabled also available:
    #     {gausConstant[Err]} {gausMean[Err]} {gausSigma[Err]}
    format: "{detector} {gausMean} {gausSigma}\n"
    # This node can be added to fit found maxima with Gaussian model within
  # given window
  maxGaussianFit:
    # Number of maxima to consider. Maxima will be considered in descending
    # order, so by specifying, say 2, the procedure will attempt to fit two
    # maxima with largest amplitude
    applyTo: 1
    # Relative window (in histogram's units) to do fit at
    window: [-3, 3]
    # If this parameter is set, it must be a template string for .pdf file
    # names where fitting results will be placed
    report: "3-2-{zIdx}-width.pdf"
...
