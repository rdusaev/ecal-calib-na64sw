---

pipeline:
  #- _type: PlotEventTypeStats
  ## keep only events with beam and phys trigger bits set
  #- _type: DropIfTriggerBits
  #  _label: "Only phys+beam"
  #  bits: [phys, beam]
  #  requireAllSet: true
  #  invert: true
  #- _type: PlotEventTypeStats
  #  trigHistName: 'selected-triggers'
  #  evTypeHistName: 'selected-evTypes'

  # --- CUT --- CUT ---
  # Do time-based clusterization among discovered peaks
  #- _type: MSADCTimeClusterize
  #  _label: msadcTimeClusterize
  #  applyTo: "kin == ECAL"
  #  method:
  #    _type: simple
  #    timeDeltaNs: 12.5
  # TODO: qustionable, see same handler in stage 2.1...
  # Discriminate the event with similar highest amplitudes within concurrent
  # time clusters, or set the winning cluster's parameters to calo hit
  #- _type: MSADCTimeClusterConservativeBestAmp
  #  _label: msadcECALRemoveClusters
  #  applyTo: "kin == ECAL"
  #  invert: false
  #  strayPeaksErrorEstimate: noError  # "noError" means "as common" here
  #  sidePeaksErrorEstimate: noError  # "noError" means "as common" here
  #  commonPeaksErrorEstimate: byAmplitude  # this method will be used if above is "noError"
  #  ampErrorMultiplier: 1.
  #  uncErrorMultiplier: 1.
  #  ampRatioThreshold: 0.1
  #  setHitValues: true
  # --- CUT --- CUT ---

  #
  # Drop peaks with sigma outside of some expected mean (real signal has peak
  # width being close to some constant, proper to particular channel)
  #- _type: MSADCDropPeaksBySigma
  #  applyTo: "kin == ECAL"
  #  peaksErrorEstimate: byAmpAndUncertainty
  #  # Permitted deviation from mean in own std.err units (+/-)
  #  factor: 5
  #  ampErrorMultiplier: 1
  #  uncErrorMultiplier: 1
  #  overrideFile: widths.csv

  #
  # drop event if its abs.error exceeds certain threshold, in any of SADC hits
  - _type: Discriminate
    _label: rawMaxAmpDiscriminate
    applyTo: "kin == ECAL"
    expression: "sadcHits.rawData.maxAmpError > 50"
    discriminateEvent: true

  #
  # drop event if its abs.error exceeds certain threshold
  # (in any of SADC hits)
  - _type: Discriminate
    _label: rawMaxTimeDiscriminate
    applyTo: "kin == ECAL && xIdx == %(xIdx)% && yIdx == %(yIdx)%"
    expression: "sadcHits.timeError > 25"
    discriminateEvent: true

  # XXX, handler removed
  #- _type: MSADCPlotMoyalDistributions
  #  #applyTo: "kin == ECAL && xIdx == 2 && yIdx == 2 && zIdx == 1"
  #  baseNameDist: "msadc-after-moyal-{variable}-{kin}{statNum}:{xIdx}-{yIdx}-{zIdx}"
  #  baseDescriptionDist: "Distribution of MSADC Moyal fit parameter {variable} for {TBName} ; {variable} ; counts"
  #  baseNameCorr: "msadc-after-moyal-{variableX}-vs-{variableY}-{kin}{statNum}:{xIdx}-{yIdx}-{zIdx}"
  #  baseDescriptionCorr: "Correlated distributions of {variableX} vs. {variableY} for {TBName} ; {variableX} ; {variableY}"
  #  mu:
  #    range: [-120, 525]
  #    nBins: 645
  #  sigma:
  #    range: [0, 25]
  #    nBins: 400
  #  S:
  #    range: [0, 15000]
  #    nBins: 450
  #  maxAmp:
  #    range: [0, 4000]
  #    nBins: 400

  #
  # Discriminate the event with similar highest amplitudes within concurrent
  # time clusters, or set the winning cluster's parameters to calo hit
  #- _type: MSADCTimeClusterConservativeBestAmp
  #  _label: msadcECALRemoveClusters
  #  applyTo: "kin == ECAL"
  #  invert: false
  #  strayPeaksErrorEstimate: noError  # "noError" means "as common" here
  #  sidePeaksErrorEstimate: noError  # "noError" means "as common" here
  #  commonPeaksErrorEstimate: byAmpAndUncertainty  # this method will be used if above is "noError"
  #  ampErrorMultiplier: 1.
  #  uncErrorMultiplier: 1.
  #  ampRatioThreshold: 0.1
  #  setHitValues: true

  # Save survived events into AvroDST for further usage as calib. data input
  - _type: AvroSaveEvents
    schema: /afs/cern.ch/user/r/rdusaev/Projects/na64/ecal-calib/presets/tmp/na64sw-event-avro.json
    codec: "lzma"  # possible values: null, deflate, [lzma, snappy]
    file: "selected-peaks-%(cellIdx)%.events.db"
    blockSizeKb: 2048

  # This one is important as it is used to obtain values at peaks, that one
  # can use in some rough calibration or as scaling factors
  - _type: Histogram1D
    value: sadcHits.rawData.maxAmp
    histName: "max-value"
    histDescr: "Maximum Sample Value {TBName}"
    nBins: 500
    range: [0, 5000]

  # --- (OPTIONAL) ---

  # Plot time vs time error
  - _type: Histogram2D
    #applyTo: "kin == HCAL || kin == ECAL"
    histName: "hit-time-vs-time-error"
    histDescr: "MSADC time vs. time error, {TBName} ; time [ns] ; time err. [ns]"

    valueX: sadcHits.time
    nBinsX: 400
    rangeX: [0, 400]

    valueY: sadcHits.timeError
    nBinsY: 120
    rangeY: [0, 25]

  - _type: Histogram2D
    #applyTo: "kin == HCAL || kin == ECAL"
    histName: "hit-amp-vs-amp-error"
    histDescr: "MSADC raw amp vs. raw amp error, {TBName} ; amp [raw chan.] ; amp err. [raw chan.]"

    valueX: sadcHits.rawData.maxAmp
    nBinsX: 400
    rangeX: [0, 4096]

    valueY: sadcHits.rawData.maxAmpError
    nBinsY: 400
    rangeY: [0, 4096]

  #
  #- _type: ShowMSADCHits
  #  #applyTo: "kin == ECAL && zIdx == 1 && xIdx != 0 && xIdx != 4 && yIdx != 0 && yIdx != 5"
  #  applyTo: "kin == ECAL"
  #  #applyTo: "kin == ECAL && zIdx == 0"
  #  destination: default

  - _type: Histogram2D
    #applyTo: "kin == HCAL || kin == ECAL"
    histName: "hit-time-vs-max-val"
    histDescr: "MSADC raw amp vs time, {TBName} ; time [ns or sample #] ; raw amp msrmnt"

    valueX: sadcHits.time
    nBinsX: 400
    rangeX: [0, 400]

    valueY: sadcHits.rawData.maxAmp
    nBinsY: 4096
    rangeY: [0, 4096]

  - _type: Histogram1D
    #applyTo: "kin == HCAL || kin == ECAL"
    value: sadcHits.time
    histName: "hit-time"
    histDescr: "MSADC hit time, {TBName} ; time [ns or sample #] ; N [occurences]"
    nBins: 400
    range: [0, 400]
...
