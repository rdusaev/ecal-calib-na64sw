---
# This is a parameterised config for stage-2 of ECAL calibration procedure.
# Having atan(q/maxAmp) ratio defined for all the cells this script produces
# clean samples for every cell with well-separated non pile-up peak for
# further analysis. Following variables are expected to be substituted:
#   %(xIdx)%     -- x-index of the cell to be calibrated
#   %(yIdx)%     -- y-index of -"-
#   %(mainMin)%  -- minimum of atan(q/maxAmp) in the main part
#   %(mainMax)%  -- maximum -"-
#   %(prsMin)%   -- minimum of atan(q/maxAmp) in the preshower part
#   %(prsMax)%   -- maximum -"-"
pipeline:
  # Drop events with charge/maxAmp ratio exceeding permitted ranges on the
  # cell being calibrated
  - _type: Discriminate
    applyTo: "kin == ECAL && xIdx == %(xIdx)% && yIdx == %(yIdx)% && zIdx == 1"
    expression: %(mainMin)% < sadcHits.rawData.chargeVsMaxAmpRatio < %(mainMax)%
    invert: true
    discriminateEvent: true
  - _type: Discriminate
    applyTo: "kin == ECAL && xIdx == %(xIdx)% && yIdx == %(yIdx)% && zIdx == 0"
    expression: %(prsMin)% < sadcHits.rawData.chargeVsMaxAmpRatio < %(prsMax)%
    invert: true
    discriminateEvent: true
  # write events in the special file for solver
  - _type: SADCDumpMaxAndSum
    applyTo: "kin == ECAL"
    output: "./ecal-calib-data-%(xIdx)%x%(yIdx)%.dat"
...
