# Preliminary ECAL calibrations.
# - Obtained for 50GeV beam of 2023A
# - No shower profile fractions are taken into account (so peripherial cells
#   are unreliable)

# Range of 50GeV calibration runs
runs=9166-9200

# (we have no time calibrations so far anyway)
timeIsRelativeToMaster=false

# Factors are not taken into account in the LMS calibrations
factor.ECAL0=1.
factor.ECAL1=1.

columns=name,x,y,coeff

# Preshower
ECAL0 0  0    6.0454e-02
ECAL0 1  0   -8.9245e-05
ECAL0 2  0    4.7619e-03
ECAL0 3  0    9.1573e-03
ECAL0 4  0    3.6874e-02
ECAL0 0  1    4.3000e-03
ECAL0 1  1    5.6005e-03
ECAL0 2  1    6.3413e-03
ECAL0 3  1    6.2413e-03
ECAL0 4  1    6.0193e-04
ECAL0 0  2    6.0077e-03
ECAL0 1  2    4.3264e-03
ECAL0 2  2    3.0096e-03
ECAL0 3  2    2.9119e-03
ECAL0 4  2    7.5344e-03
ECAL0 0  3    2.4802e-03
ECAL0 1  3    3.5732e-03
ECAL0 2  3    2.5783e-03
ECAL0 3  3    3.5957e-03
ECAL0 4  3    3.1250e-03
ECAL0 0  4    5.1868e-03
ECAL0 1  4    3.0286e-03
ECAL0 2  4    3.1825e-03
ECAL0 3  4    3.7427e-03
ECAL0 4  4    7.4006e-03
ECAL0 0  5    1.4934e-02
ECAL0 1  5    3.9179e-02
ECAL0 2  5    4.3099e-03
ECAL0 3  5    1.4155e-02
ECAL0 4  5    1.7183e-02

# Main calorimeter
ECAL1 0  0    4.7477e-02
ECAL1 1  0    2.7726e-02
ECAL1 2  0    2.7127e-02
ECAL1 3  0    3.8908e-02
ECAL1 4  0    4.7924e-02
ECAL1 0  1    2.5359e-02
ECAL1 1  1    2.5345e-02
ECAL1 2  1    2.9662e-02
ECAL1 3  1    2.2486e-02
ECAL1 4  1    2.8110e-02
ECAL1 0  2    3.6074e-02
ECAL1 1  2    2.9353e-02
ECAL1 2  2    3.6913e-02
ECAL1 3  2    2.2497e-02
ECAL1 4  2    2.4694e-02
ECAL1 0  3    2.9930e-02
ECAL1 1  3    3.7014e-02
ECAL1 2  3    2.0470e-02
ECAL1 3  3    3.2735e-02
ECAL1 4  3    2.8919e-02
ECAL1 0  4    2.6037e-02
ECAL1 1  4    2.7990e-02
ECAL1 2  4    2.7408e-02
ECAL1 3  4    2.7889e-02
ECAL1 4  4    2.3937e-02
ECAL1 0  5    3.0720e-02
ECAL1 1  5    2.3611e-02
ECAL1 2  5    4.0978e-02
ECAL1 3  5    2.4725e-02
ECAL1 4  5    2.8980e-02
