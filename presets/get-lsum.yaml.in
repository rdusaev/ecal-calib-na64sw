---
# Pedestals
pedestals:
  routine: fitDistribution
  ROOTClassName: TH1
  nameLookup:
    expression: "^pedestals-([^\\-]+)-([\\w:-]+)$"  # for TH2
    groups:
      pedType1: 1
      detID1: 2
  description:
    expression: "^([\\w]+) pedestal values distribution for ([\\w:-]+).*"
    groups:
      pedType2: 1
      detName: 2
  fitFuncs:
    gaus:
      type: Gaussian
      value: mean
      range: [mu_minus_sigma_or_x_low, mu_plus_sigma_or_x_up]
      stddev:
        value: stddev
        range: [stddev01, stddev3]
      S:
        value: S
        range: [S01, S15]
      # Set boundaries
      range: [x_low, x_up]
  output:
    path: "pedestals-fit.csv"
    format: "{detName},{pedType1},{hist.mean},{hist.stddev},{gaus.mean},{gaus.stddev}\n"

# Used to obtain window for sq/max ratio
ecal-sadc-ratio:
  routine: featurePoints1D
  # Width of Gaussian convolution kernel to smooth the spectrum prior to
  # characteristic points lookup
  variance: 25
  ROOTClassName: TH1
  nameLookup:
    #expression: "^max-value-(([0-9])-([0-9])-([0-9]))$"
    expression: "^lin-sum-max-ratio-ECAL-([\\w:-]+)\\s*$"
    groups:
      partID: 1
  description:
    expression: "^S / A_max, ([\\w:-]+):([\\w:-]+)\\s*$"
    groups:
      kin: 1
      partID2: 2
  output:
    path: "r-peaks.dat"
    # Following fields are available:
    #     {x} {y} {nSample} {pointType}
    # If `maxGaussianFit' is enabled also available:
    #     {gausConstant[Err]} {gausMean[Err]} {gausSigma[Err]}
    format: "{kin}:{partID} {gausMean} {gausSigma}\n"
    # This node can be added to fit found maxima with Gaussian model within
  # given window
  maxGaussianFit:
    # Number of maxima to consider. Maxima will be considered in descending
    # order, so by specifying, say 2, the procedure will attempt to fit two
    # maxima with largest amplitude
    applyTo: 1
    # Relative window (in histogram's units) to do fit at. Real peak for cell
    # we're interested in is sharp an slightly assymetric making Gaussian curve
    # not the best option to fit, yet we only need approximate value
    window: [-.35, .35]
    # If this parameter is set, it must be a template string for .pdf file
    # names where fitting results will be placed
    report: "{kin}-{partID}-ratio-peaks.pdf"

# used
ecal-sums-fit:
  routine: featurePoints1D
  # Width of Gaussian convolution kernel to smooth the spectrum prior to
  # characteristic points lookup
  variance: 25
  ROOTClassName: TH1
  nameLookup:
    #expression: "^max-value-(([0-9])-([0-9])-([0-9]))$"
    expression: "^lin-sum-(%(cellX)%-%(cellY)%-[\\w:-]+)\\s*$"
    groups:
      partID: 1
  description:
    expression: "^Linear sum, (ECAL[\\w:-]+)\\s*$"
    groups:
      detName: 1
  output:
    path: "lsum-peaks.dat"
    # Following fields are available:
    #     {x} {y} {nSample} {pointType}
    # If `maxGaussianFit' is enabled also available:
    #     {gausConstant[Err]} {gausMean[Err]} {gausSigma[Err]}
    format: "{detName} {gausMean} {gausSigma}\n"
    # This node can be added to fit found maxima with Gaussian model within
  # given window
  maxGaussianFit:
    # Number of maxima to consider. Maxima will be considered in descending
    # order, so by specifying, say 2, the procedure will attempt to fit two
    # maxima with largest amplitude
    applyTo: 1
    # Relative window (in histogram's units) to do fit at
    #window: [-1750, 1750]  # main
    window: [-2500, 2500]  # prs
    # If this parameter is set, it must be a template string for .pdf file
    # names where fitting results will be placed
    report: "ECAL-{partID}-peaks.pdf"

...
