#include "ecp-cell.h"
#include "ecp-generator.h"
#include "ecp-types.h"
#include "ecp-file.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include <stddef.h>

static void
_print_usage( FILE * f
            , const char * appName) {
    fputs("ECAL Calibration Mock data generator app.\nUsage:\n\t $ "
         , f );
    fputs(appName, f);
    fputs("[-n nEvents] [-o outFile] [-r] -v <value-name> -g[+-*/]=[pars-list]...\n", f);
    fputs("  -n <events:int> sets the number of events to generate, default"
          "     is 1.\n", f);
    fputs("  -v <value-name>[+-*/]=<gen-name>,<pars-list> defines a generator\n"
          "     to be applied. Value referenced by name will be set or\n"
          "     modified with random numbers or a constant defined by\n"
          "     <gen-name> with <pars-list>.\n", f);
    fputs("  -r Causes last generator to re-run on warnings.\n", f);
    fputs("  -o <outFile> output file path.\n", f);
    fputs("  -i same as -v, but generator is ran only on 1st \"event\".\n", f);
    fputs("  -d [xyz]<size:float> sets size of volume, by x,y, or z.\n", f);
    fputs("  -s [xyz]<nItems:int> sets segmentation of volume, by x,y, or z.\n", f);
    fputs("Examples:\n"
          " - generate 9 entries without any errors with energy deposition\n"
          "   distributed by normal law (mean=12, sigma=6) with random uniform\n"
          "   calib coefficients in range 1000-1500:\n"
          "     $ ecp-generate -n 9 -dx3 -dy3 -veDep=gaus,12,6 -icalibCoef=uniform,1000,1500\n"
         , f);
    fputs(" - generate 100 entries with absolute and relative errors distributed\n"
          "   by normal law with energy deposition distributed by normal law\n"
          "   with random uniform calib coefficients:\n"
          "     $ ecp-generate -n 100 -dx3 -dy3 -veDep=gaus,12,6 -vabsModelErr=gaus,0,3.5 \\\n"
          "         -vrelModelErr=gaus,0,0.5 -icalibCoef=uniform,1000,1500\n"
         , f);
}

struct GeneratorListItem {
    struct ecp_Generator * generator;
    struct GeneratorListItem * next;
};

struct ecp_GeneratorItem {
    unsigned int atStartOnly:1;
    unsigned int reRunOnWarning:1;
    struct ecp_Generator generatorObj;
};

int
main(int argc, char * argv[]) {
    size_t nEvents = 1;
    int opt, nIdx;
    char * c;
    size_t nGenerators = 0;
    unsigned short segmentation[3] = {3, 3, 1};
    double dims[3] = {1, 1, 1};
    struct ecp_GeneratorItem generators[64];
    bzero(&generators, sizeof(generators));
    bool outIsBinary = false;
    FILE * outFile = stdout;
    while(-1 != (opt = getopt(argc, argv, "bhrn:i:v:s:d:o:"))) {
        switch (opt) {
        case 'h' :
            _print_usage(stdout, argv[0]);
            return EXIT_SUCCESS;
        case 'n' :
            nEvents = strtoul(optarg, &c, 0);
            break;
        case 'b' :
            outIsBinary = true;
            break;
        case 'r' :
            generators[nGenerators].reRunOnWarning = 0x1;
        case 'o' :
            if(*optarg == '+') {
                outFile = fopen(optarg + 1, "a");
            } else {
                outFile = fopen(optarg, "w");
            }
            if(NULL == outFile ) {
                int ec = errno;
                fprintf( stderr
                       , "Error opening file \"%s\": %s"
                       , *optarg == '+' ? optarg + 1 : optarg
                       , strerror(ec)
                       );
                exit(EXIT_FAILURE);
            }
            break;
        case 'i' :
        case 'v' :
            if(nGenerators == sizeof(generators)/sizeof(generators[0])) {
                fputs("Error: can't create more generators.\n", stderr);
                return EXIT_FAILURE;
            }
            generators[nGenerators].atStartOnly = (opt == 'i' ? 0x1 : 0x0);
            if(ecp_create_generator(optarg, &(generators[nGenerators++].generatorObj))) {
                fputs("Exit due to generator expression error.\n", stderr);
                return EXIT_FAILURE;
            }
            /* append list */
            break;
        case 's':
            if('x' == *optarg)      nIdx = 0;
            else if('y' == *optarg) nIdx = 1;
            else if('z' == *optarg) nIdx = 2;
            else {
                fprintf(stderr, "Error: can't interpret '%c' as axis prefix"
                        " for -s option.\n", '\0' != *optarg ? *optarg : '0' );
                return EXIT_FAILURE;
            }
            if( optarg[1] == '\0' ) {
                fputs("Error: number expected after axis suffix prefix in -s"
                        " option argument.", stderr);
                return EXIT_FAILURE;
            }
            segmentation[nIdx] = strtold(optarg+1, &c);
            if(*c != '\0') {
                fprintf(stderr, "Error: extra symbols on the tail of \"%s\""
                        " in -s option arg.\n"
                        , optarg+1 );
                return EXIT_FAILURE;
            }
            break;
        case 'd':
            if('x' == *optarg)      nIdx = 0;
            else if('y' == *optarg) nIdx = 1;
            else if('z' == *optarg) nIdx = 2;
            else {
                fprintf(stderr, "Error: can't interpret '%c' as axis prefix"
                        " for -d option.\n", '\0' != *optarg ? *optarg : '0' );
                return EXIT_FAILURE;
            }
            if( optarg[1] == '\0' ) {
                fputs("Error: number expected after axis suffix prefix in -d"
                        " option argument.", stderr);
                return EXIT_FAILURE;
            }
            dims[nIdx] = strtod(optarg+1, &c);
            if(*c != '\0') {
                fprintf(stderr, "Error: extra symbols on the tail of \"%s\""
                        " in -d option arg.\n"
                        , optarg );
                return EXIT_FAILURE;
            }
            break;
        default:
            fprintf( stderr
                   , "Unrecognized option -%c; run with -h to get help.\n"
                   , (char) opt);
            return EXIT_FAILURE;
        }
    }

    printf( "Info: generator app initialized to run %zu generators %zu times"
            " on grid of %ux%ux%u cells with dimensions %.2ex%.2ex%.2e\n"
          , nGenerators, nEvents
          , segmentation[0], segmentation[1], segmentation[2]
          , dims[0], dims[1], dims[2]
          );

    struct ecp_EDepData * data = NULL;
    int rc = ecp_init_cells(&data, segmentation, dims, 0x0);
    if(0 != rc) {
        fprintf(stderr, "Error: init cells procedure returned %d. Evaluation abrupt.\n"
                , rc );
    }

    for(size_t nEvt = 0; nEvt < nEvents; ++nEvt) {
        for(size_t nGen = 0; nGen < nGenerators; ++nGen) {
            if(0x1 == generators[nGen].atStartOnly && 0 != nEvt) continue;
            ecp_generate_all( &generators[nGen].generatorObj, data
                            , generators[nGen].reRunOnWarning);
            //generators[nGen].generatorObj.generate(data, generators[nGen].generatorObj.userdata);
        }

        for(struct ecp_EDepData * cell = data; ; ++cell ) {
            ecp_eval_cell(cell);
            if(cell->isTerminal) break;
        }

        if(0 == nEvt) {
            if(outIsBinary) {
                assert(false);  // TODO: binary header
            } else {
                ecp_file_write_text_header(outFile, segmentation, dims, data);
            }
        }
        if(outIsBinary) {
            assert(false);  // TODO: binary data fmt
        } else {
            ecp_file_write_text(outFile, data);
        }
    }

    return 0;
}


    #if 0
    const unsigned short segmentation[3] = {5, 3, 4};
    for(unsigned short i = 0; i < segmentation[0]; ++i) {
        for(unsigned short j = 0; j < segmentation[1]; ++j) {
            for(unsigned short k = 0; k < segmentation[2]; ++k) {
                size_t offset = ecp_idx2offset(i, j, k, segmentation);
                unsigned short xIdx, yIdx, zIdx;
                ecp_offset2idx(offset, &xIdx, &yIdx, &zIdx, segmentation);
                if(xIdx != i || yIdx != j || zIdx != k) {
                    fprintf(stderr, "Bad offset-to-index conversion:\n"
                            "  original: %ux%ux%u\n"
                            "  offset: %zu\n"
                            "  restored: %ux%ux%u\n"
                            , i, j, k
                            , offset
                            , xIdx, yIdx, zIdx
                            );
                    return -1;
                }
                fprintf(stdout, "Conversion ok:\n"
                            "  original: %ux%ux%u\n"
                            "  offset: %zu\n"
                            "  restored: %ux%ux%u\n"
                            , i, j, k
                            , offset
                            , xIdx, yIdx, zIdx
                            );
            }
        }
    }
    #endif
