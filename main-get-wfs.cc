#include <TFile.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TF1.h>

#if 0
Double_t
moyal_f(Double_t * x_, Double_t * par_) {
    static const double sqrt2Pi = sqrt(2*M_PI);
    const double x = *x_
               , w = par_[0]  // 1 or 4 "width" value
               , s = par_[1]  // 2 or 5 "shape" value
               ;
    double v = exp( -(x*w + exp(-x*w))/2 )/sqrt2Pi;
    return pow(v, s);
}
#else
Double_t
moyal_f2(Double_t * x_, Double_t * par_) {
    static const double sqrt2Pi = sqrt(2*M_PI);
    const double x = *x_
               , w = par_[0]  // 1 or 4 "width" value
               ;
    double v = exp( -(x/(2*w) + exp(-x/w))/2 )/sqrt2Pi;
    return v;
}
#endif

Double_t
enhanced_moyal_f(Double_t * x_, Double_t * par_) {
    const double S   = par_[0]  // 1
               , mu  = par_[1]  // 2
               , ped = par_[2]  // 3
               ;
    Double_t xNew = *x_ - mu;
    return ped + S*moyal_f2(&xNew, par_ + 3);
}

void get_wfs() {
    TFile * f = TFile::Open("../na64sw.build/processed-9184-2x3-50k-recd-full-tight.root");
    const int cx = 2, cy = 3;
    TObject * p_[19];
    char namebf[64];
    for(int i = -1; i < 2; ++i) {
        for(int j = -1; j < 2; ++j) {
            for(int k = 0; k < 2; ++k) {
                size_t nObj = (1+i)*6 + (1+j)*2 + k;
                snprintf(namebf, sizeof(namebf), "/ECAL/x%d/y%d/amps-raw-%d-%d-%d"
                        , i + cx, j + cy
                        , i + cx, j + cy, k );
                assert(nObj < 19);
                p_[nObj] = f->Get(namebf);
                assert(p_[nObj]);
                //printf("...%d %d %d");
            }
        }
    }
    p_[18] = nullptr;
    // get some profiles:
    //TObject * p_[] = {
    //        f->Get("/ECAL/x2/y3/amps-raw-2-3-0"),
    //        f->Get("/ECAL/x3/y3/amps-raw-3-3-0"),
    //        f->Get("/ECAL/x3/y4/amps-raw-3-4-0"),
    //        f->Get("/ECAL/x2/y3/amps-raw-2-3-1"),
    //        f->Get("/ECAL/x3/y3/amps-raw-3-3-1"),
    //        f->Get("/ECAL/x3/y4/amps-raw-3-4-1"),
    //        nullptr
    //    };
    // dump
    #if 0
    for(TObject ** ptr = p_; ptr; ++ptr) {
        auto p = dynamic_cast<TProfile*>(*ptr);
        assert(p);
        std::cout << std::endl;
        for(Int_t i = 0; i < p->GetNbinsX(); ++i) {
            std::cout
                << i << " "
                << p->GetBinCenter(i) << " "
                << p->GetBinContent(i) << " "
                << p->GetBinError(i)
                << std::endl;
        }
    }
    #endif
    TCanvas * cnv = new TCanvas("wf-fits", "Waveform fitting");
    cnv->Divide(6, 3);
    Int_t fitRange[2] = {8, 17};
    for(int i = 0; i < 18; ++i) {
        cnv->cd(i+1);
        assert(p_[i]);
        
        auto profile = dynamic_cast<TProfile*>(p_[i]);
        profile->Draw();
        char namebf[32];
        snprintf(namebf, sizeof(namebf), "fit-%d", i);
        auto func = new TF1(namebf, enhanced_moyal_f, -5, 32, 4);
        Double_t sum = 0;
        for(int nbin = 1; nbin < 33; ++nbin) {
            sum += profile->GetBinContent(nbin);
        }
        func->SetParameters( sum //profile->GetIntegral()  // 1. initial "integral"
                    , 10  // 2. initial center
                    , (profile->GetBinContent(0) + profile->GetBinContent(1) + profile->GetBinContent(2))/3  // 3. initial "pedestal"
                    , 2/3.  // 4. initial "width"
                    //, 1.  // "shape"
                    );
        // NOTE: somehow affects the convergence, in a bad way:
        //func->SetParLimits(0, 1, 4096*64);  // limits for sum
        //func->SetParLimits(1, -16, 16);  // limits for center (more than nSamples because of width)
        //func->SetParLimits(2, 0, 4096.);  // limits for constant "pedestal" term
        //func->SetParLimits(3, 1e-2, 64);  // limits for "width"
        //func->SetParLimits(4, 0.5, 2);  // limits for "shape"
        
        profile->Fit(func, "", "", fitRange[0], fitRange[1]);
        //func->Draw("SAME");

        // Appendix to study non-Moyal residual factor/term
        //   we put markers on ratio/residual within the range of fit
        snprintf(namebf, sizeof(namebf), "resid-%d", i);
        //auto residualsProfile = new TProfile(namebf, "Residuals profile", 32, 0, 32);
        snprintf(namebf, sizeof(namebf), "resid-%d.dat", i);
        FILE * residFile = fopen(namebf, "w");
        fprintf( residFile, "# S=%e (%e), c=%e (%e), ped=%e (%e), w=%e (%e)\n"
               , func->GetParameter(0), func->GetParError(0)  // S
               , func->GetParameter(1), func->GetParError(1)  // c
               , func->GetParameter(2), func->GetParError(2)  // ped
               , func->GetParameter(3), func->GetParError(3)  // w
               //, func->GetParameter(4), func->GetParError(4)  // shape
               );
        for(Int_t i = 1 /*fitRange[0]*/; i < 33 /*fitRange[1]*/; ++i) {
            #if 1
            Float_t x = profile->GetBinCenter(i)
                  , ratio = (profile->GetBinContent(i)) / (func->Eval(i));
            fprintf( residFile, "  %2d %9.3e %9.3e %9.3e %9.3e\n"
                   , i, x
                   , profile->GetBinContent(i)
                   , ratio, profile->GetBinError(i));
            //residualsProfile->SetBinContent(i, ratio);
            //residualsProfile->SetBinError(i, x, profile->GetBinError(i));
            #else
            if( i < fitRange[0] || i > fitRange[1] ) {
                (*residualsProfile)[i] = (*profile)[i];    // copy bin y values 
            } else {
                (*residualsProfile)[i] = (*profile)[i] - func->Eval(i);    // copy subt. bin y values
                printf("  %e\n", (*residualsProfile)[i]);
            }
            (*residualsProfile->GetSumw2())[i] =  (*profile->GetSumw2())[i];   // copy bin y*y values
            residualsProfile->SetBinEntries(i, profile->GetBinEntries(i) );    // copy bin entries
            // copy (if needed) bin sum of weight square
            if( profile->GetBinSumw2()->fN > i ) { 
                residualsProfile->Sumw2();
                (*residualsProfile->GetBinSumw2())[i] = (*profile->GetBinSumw2())[i];
            }
            #endif
        }
        fclose(residFile);
        //residualsProfile->Draw("SAME");
        //residualsProfile->Draw();
    }
}
