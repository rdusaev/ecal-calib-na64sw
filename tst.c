static const struct {
    const char name[32];
    const char idxPrs[256], idxMain[256];
} _gPatternedStencils[] = {
    { "5x5-star-01",

        " + ,"
        "+++,"
        " + ",

        " +++ ,"
        "+++++,"
        "+++++,"
        "+++++,"
        " +++ "
    },
};

static int
_parse_bitmask( unsigned long * bitmask
              , unsigned short * nCellsX, unsigned short * nCellsY
              , const char * strprs
              ) {
    /* count dimensions */
    unsigned short ncCellX = 0;
    *nCellsX = 0;
    *nCellsY = 0;
    for(const char * c = strprs; '\0' != *c; ++c) {
        if(',' == *c) {  /* row delimiter */
            ++nCellsY;
            if(ncCellX > *nCellsX) *nCellsX = ncCellX;
            ncCellX = 0;
            continue;
        }
    }
    if(*nCellsX*(*nCellsY) > sizeof(unsigned long)*8) return -1;  /*stencil is too large*/
    if((*nCellsX)%2 != 1) return -2;  /* number of x cells is not odd */
    if((*nCellsY)%2 != 1) return -3;  /* number of y cells is not odd */
    /* set bitmask */
    ncCellX = 0;
    *bitmask = 0x0;
    for(const char * c = strprs; '\0' != *c; ++c) {
        *bitmask |= (((unsigned long) 0x1) << (strprs - c));
    }
    return 0;
}

static int
_test_index( const unsigned long mask
           , const unsigned short dimX, const unsigned short dimY
           , short relXIdx, short relYIdx
           ) {
    short xIdx = relXIdx + dimX/2
        , yIdx = relYIdx + dimY/2;
    return mask & (0x1 << (yIdx*dimX + xIdx));
}
