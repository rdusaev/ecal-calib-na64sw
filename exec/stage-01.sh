#!/bin/bash

BASE_DIR="$( cd "$( dirname "`readlink -f ${BASH_SOURCE[0]}`" )/.." && pwd )"
source $BASE_DIR/exec/environment.sh

IN_FILE_RAW=$1   # $1 -- /eos/experiment/na64/data/cdr/%(inFile)%
PED_RCFG=$2      # $2 -- presets/run.d/run.01.1.get-pedestals.yaml.in -> run01.1.get-pedestals.yaml
RPT_FILE_OUT=$3  # $3 -- processed.prelim.%(runID)%-%(chunkID)%.root
PEDESTALS_OUT=$4 # $4 -- 'pedestals.%(runID)%-%(chunkID)%.dat', output pedestals file (valid for this chunk)

# generate histograms with distributions we're interested in. Do not use full
# chunk data yet, 10-20k events should be enough
na64sw-pipe -r $PED_RCFG -o $RPT_FILE_OUT $IN_FILE_RAW -N 20000
# extract pedestals
# - produces `pedestals-fit.csv' as defined in $NA64SW_PREFIX/share/na64sw/get-fts-config.yaml 
na64sw-get-fts \
    pedestals \
    $BASE_DIR/presets/get-pedestals.yaml \
    $RPT_FILE_OUT
# - turn depedstals-fit.csv into pedestals.*.dat
python3 ${BASE_DIR}/py/getPedestals.py pedestals-fit.csv $PEDESTALS_OUT
rm -fv pedestals-fit.csv
