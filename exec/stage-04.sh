#!/bin/bash

#TAG=2023mu.ecal-calib
#ATTEMPT=00
#INPUT_DUMP_DIR=/home/rdusaev/data/cern/na64/2023mu/ecal-calib/40GeV  # TODO

TAG=2024e.ecal-calib-selected-HI
ATTEMPT=00
INPUT_DUMP_DIR=/home/rdusaev/data/cern/na64/2024e/calib/100GeV-HI

OUT_DIR=out/${TAG}.${ATTEMPT}  # TODO
mkdir $OUT_DIR  # XXX

#                                                                      _______
# ___________________________________________________________________/ Phase 0
# get coarse profile estimate based mostly on central cells

# Run ecp-solve to get coarse estimation of shower profile (normalized)
#exec/ecp-resolve \
#    -Slms-svd,full \
#    -o ${OUT_DIR}/out.0.dat \
#    -b 1 \
#    ${INPUT_DUMP_DIR}/calib-dump.[123]-[1234].*.dat
# Get coarse profile estimates: mean, stddev, etc
#python3 py/profile.py \
#    --calibrations ${OUT_DIR}/out.0.dat \
#    --report ${OUT_DIR}/shower-report.0.pdf \
#    --output ${OUT_DIR}/profile.0.dat \
#    ${INPUT_DUMP_DIR}/calib-dump.2-[23].*.dat

#                                                                      _______
# ___________________________________________________________________/ Phase 1
# Use coarse convoluted profile estimate to resolve calib coefficients in the
# entire volume, including periphery cells
#exec/ecp-resolve \
#    -Slms-svd,full \
#    -o ${OUT_DIR}/out.1.dat \
#    -b 1 \
#    ${INPUT_DUMP_DIR}/calib-dump.*-*.*.dat \
#    -p ${OUT_DIR}/profile.0.dat
# Obtain finer convoluted profile estimate
#python3 py/profile.py \
#    --calibrations ${OUT_DIR}/out.1.dat \
#    --report ${OUT_DIR}/shower-report.1.pdf \
#    --output ${OUT_DIR}/profile.1.dat \
#    ${INPUT_DUMP_DIR}/calib-dump.*.dat

#                                                                      _______
# ___________________________________________________________________/ Phase 2
# Use finer convoluted profile estimate to resolve calib coefficients in the
# entire volume, including periphery cells
#exec/ecp-resolve \
#    -Slms-svd,full \
#    -o ${OUT_DIR}/out.2.dat \
#    -b 1 \
#    ${INPUT_DUMP_DIR}/calib-dump.*-*.*.dat \
#    -p ${OUT_DIR}/profile.1.dat
# Obtain finer convoluted profile estimate
#python3 py/profile.py \
#    --calibrations ${OUT_DIR}/out.2.dat \
#    --report ${OUT_DIR}/shower-report.2.pdf \
#    --output ${OUT_DIR}/profile.2.dat \
#    ${INPUT_DUMP_DIR}/calib-dump.*.dat

#
# Output: renormalize coefficeints in beam units, generate SDC-compatible
# ASCII output

# Get profile as an input for ecp-resolve
#
echo -e "runs=10338-10516\nfactor.ECAL0=100\nfactor.ECAL1=100\ncolumns=name,x,y,peakpos" ; grep -v '^#' ${OUT_DIR}/out.2.dat | awk '{print $1$2 " " $3 " " $4 " " sprintf("%.4e", 1./$5)}' >> ../../../opt/p348-daq/p348reco/conddb/calib/2024/ecal.txt
# XXX, snippets
# To get only main part
#sed -n '/^# mean$/,/^# stddev$/p' profile.0.dat | sed -n '/^# main:$/,/^# stddev$/p'
