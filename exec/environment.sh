#!/bin/bash

#
# Versions in use

source /cvmfs/na64.cern.ch/sft/x86_64-el9-gcc11/this-env.sh
module load na64sw/0.6.116-dbg

# Supplementary variables
export NA64AVRO_EVENT_SCHEMA=$NA64SW_PREFIX/share/na64sw/na64sw-event-avro.json

