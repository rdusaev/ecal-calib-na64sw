#!/bin/bash

BASE_DIR="$( cd "$( dirname "`readlink -f ${BASH_SOURCE[0]}`" )/.." && pwd )"
source $BASE_DIR/exec/environment.sh

cellX=$1                # 0,1,2,3, etc
cellY=$2                # 0,1,2,3, etc
IN_ITEMS_LIST=$3        # one:two:...
GET_FTS_CFG=$4          # get-lsum.yaml.in
ITEMS_DIR=/eos/user/${USER:0:1}/${USER}/na64/batch.out/$5

#OUT_SUMMARY_FILE=$4     # 

FILES_TO_MERGE=()
IFS=: read -ra IN_ITEMS <<< "$IN_ITEMS_LIST"
for item in ${IN_ITEMS[@]} ; do
    itemFile="${ITEMS_DIR}/processed.prelim.${item}.root"
    if [ ! -f ${itemFile} ] ; then
        >&2 echo "File $itemFile does not exist"
        continue
    fi
    #if [ $(stat -f %s ${itemFile}) -lt 1024 ]; then
    #    >&2 echo "File $itemFile has size <1kb, skip"
    #    continue
    #fi
    # push into the array
    FILES_TO_MERGE+=($itemFile)
done

SUMMARY_FILE=summary.prelim.$cellX-$cellY.root
hadd $SUMMARY_FILE ${FILES_TO_MERGE[@]}

# sum/max ratio peak fit, produces:
# - r-peaks.dat
# - {kin}-{part}-ratio-peaks.pdf -- fit report plot, for inspection
na64sw-get-fts \
    ecal-sadc-ratio \
    $GET_FTS_CFG \
    $SUMMARY_FILE

echo "Ratio peak fit results:"
cat r-peaks.dat

# linear sum fit peak, produces:
# - lsum-peaks.dat
# - ECAL-{partID}-peaks.pdf -- fit report, for inspection
na64sw-get-fts \
    ecal-sums-fit \
    $GET_FTS_CFG \
    $SUMMARY_FILE

echo "Linear sum fit results:"
cat lsum-peaks.dat

# Join fit results
{
    echo "# name ratio-mean ratio-sigma lsum-mean lsum-sigma"
    join <(sort r-peaks.dat) <(sort lsum-peaks.dat )
} > fit-results-$cellX-$cellY.dat

gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite \
    -sOutputFile=fit-reports-$cellX-$cellY.pdf \
    $(ls ECAL*$cellX-$cellY-?-ratio-peaks.pdf | sort) \
    $(ls ECAL*$cellX-$cellY-?-peaks.pdf | sort)

rm -fv \
    r-peaks.dat \
    lsum-peaks.dat \
    ECAL*$cellX-$cellY-?-ratio-peaks.pdf \
    ECAL*$cellX-$cellY-?-peaks.pdf 

echo "Job done."

