#!/bin/bash

BASE_DIR="$( cd "$( dirname "`readlink -f ${BASH_SOURCE[0]}`" )/.." && pwd )"
source $BASE_DIR/exec/environment.sh

cellX=$1                # 0,1,2,3, etc
cellY=$2                # 0,1,2,3, etc
IN_ITEMS_LIST=$3        # one:two:...
ITEMS_DIR=/eos/user/${USER:0:1}/${USER}/na64/batch.out/$4

FILES_TO_MERGE=()
IFS=: read -ra IN_ITEMS <<< "$IN_ITEMS_LIST"
for item in ${IN_ITEMS[@]} ; do
    itemFile="${ITEMS_DIR}/summary.selection.$cellX-$cellY.${item}.root"
    if [ ! -f ${itemFile} ] ; then
        >&2 echo "File $itemFile does not exist"
        continue
    fi
    #if [ $(stat -f %s ${itemFile}) -lt 1024 ]; then
    #    >&2 echo "File $itemFile has size <1kb, skip"
    #    continue
    #fi
    # push into the array
    FILES_TO_MERGE+=($itemFile)
done

SUMMARY_FILE=summary.prelim.$cellX-$cellY.root
hadd $SUMMARY_FILE ${FILES_TO_MERGE[@]}

cat <<EOF > get-fts-config.yaml
fit-abs-time-peak:
  routine: featurePoints1D
  # Width of Gaussian convolution kernel to smooth the spectrum prior to
  # characteristic points lookup
  variance: 25
  ROOTClassName: TH2
  projection: { axis: x }
  nameLookup:
    #expression: "^max-value-(([0-9])-([0-9])-([0-9]))$"
    expression: "^fitted-abs-time-vs-max-($cellX-$cellY-([01]))$"
    groups:
      detIndex: 1
  output:
    path: "time-peaks.dat"
    format: "{detIndex} {gausMean} {gausSigma}\n"
  maxGaussianFit:
    applyTo: 1  # Nth max to fit
    window: [-50, 50]  # samples units for the fit
    report: "./time-fit-{detIndex}.pdf"
EOF

# sum/max ratio peak fit, produces:
# - r-peaks.dat
# - {kin}-{part}-ratio-peaks.pdf -- fit report plot, for inspection
na64sw-get-fts \
    fit-abs-time-peak \
    get-fts-config.yaml \
    $SUMMARY_FILE

echo "Time peak fit results:"
cat time-peaks.dat
mv time-peaks.dat time-peaks-$cellX-$cellY.dat

gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite \
    -sOutputFile=time-fit-reports-$cellX-$cellY.pdf \
    $(ls time-fit-*.pdf | sort)

rm -fv \
    time-fit-$cellX-$cellY-?-peaks.pdf \
    $SUMMARY_FILE \
    get-fts-config.yaml

echo "Job done."

