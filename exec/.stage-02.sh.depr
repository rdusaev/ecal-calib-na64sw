#!/bin/bash

# NOTE: This is a node-running script meant to be ran on the batch node
# Using zero information from stage-01, this script runs 2.1 config on the
# calibration data producing AvroDST output with fitted peaks. Pedestals
# are explicitly specified (not brought as calib. data).
#
# Environment variables affecting this script:
#   - HTCONDOR_JOBINDEX, required -- this job ID used to figure out the data to run on.
#   - SUPP_ENV, optional -- if given, will be source'd prior to na64sw utils execution.
#       Should be a shell script file providing additional environment definitions.
#   - BASE_DIR, required -- directory where this script is located, to infer paths to assets,
#       configs, etc.
# Inputs:
#   - dev. mode switch (enabled by `-d' option)
#   - list of input files (provided with `-s' option)
#   - runs list ASCII table (provided with `-r' option)
#   - output dir path (provided with `-o' option)
#   - presets/run.d/run.02.fit-peaks.yaml.in
# Outputs:
#   - ./fitted-peaks.events.db in cwd, moved to <out-dir>/<cell-idx>/fitted-peaks-<xIdx>x<yIdx>.root
#     at the end of the script
# Note, that processed.root is not of use at this (sub-)stage and gets removed.
# Example of running locally in dev mode:
#   $  HTCONDOR_JOBINDEX=18 BASE_DIR=$(readlink -f ..) \
#           SUPP_ENV=../supp-env.local.sh ../exec/stage-02.sh \
#           -d -s ../files-list.xxx.txt \
#           -r ../presets/calib-runs-2023A-2.txt \
#           -o ./out

if [ -z "${HTCONDOR_JOBINDEX}" ] ; then
    echo "Error: env. var HTCONDOR_JOBINDEX is not set" >&2
    exit 2
fi

if [ -z "${BASE_DIR}" ] ; then
    echo "Error: BASE_DIR environment variable is not set." >&2
    exit 2
fi

if [ ! -d ${BASE_DIR} ] ; then
    echo "Error: base dir \"${BASE_DIR}\" does not exist." >&2
    exit 2
fi

if [ ! -z "${SUPP_ENV}" ] && [ ! -f ${SUPP_ENV} ] ; then
    echo "Error: supp.env file \"${SUPP_ENV}\" does not exist." >&2
    exit 2
fi

#./na64sw-pipe -m na64avro -m exp-handlers -m msadc-viewer-window \
#       -r ../na64sw/utils/ecal-calib-lms/run.01.get-pedestals.yaml \
#       ~/data/cern/na64/2023e/ecal-calib/9184-2x3/* \
#       -o processed-2x3-pedestals.root -N 200000

#
# Parse command line options
usage() { echo "Usage: $0 [-d] [-s <src-dat-files-list>] [-r <calibs-runs-list>] [-o <out-dir>]" 1>&2; exit 1; }

while getopts ":ds:r:o:" opt; do
    case "${opt}" in
        d)
            DEVMODE=1
            ;;
        s)
            SRC_DATA_FILES_LIST=${OPTARG}
            ;;
        r)
            RUNS_LIST=${OPTARG}
            ;;
        o)
            OUT_DIR_BASE=${OPTARG}
            ;;
        *)
            echo "Error: unkown option \"${opt}\"" >&2
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${SRC_DATA_FILES_LIST}" ] || [ -z "${RUNS_LIST}" ] || [ -z "{OUT_DIR_BASE}" ] ; then
    echo "Error: at least one of mandatory opts is not set" >&2
    usage
fi

#
# Assure files exist, create output dir

if [ ! -f ${SRC_DATA_FILES_LIST} ] ; then
    echo "Error: source data files list \"${SRC_DATA_FILES_LIST}\" does not exist." >&2
    exit 1
fi

if [ ! -f ${RUNS_LIST} ] ; then
    echo "Error: runs list file \"${RUNS_LIST}\" does not exist." >&2
    exit 1
fi

if [ ! -d ${OUT_DIR_BASE} ] ; then
    if ! mkdir -p ${OUT_DIR_BASE} ; then
        echo "Error: failed to create \"${OUT_DIR_BASE}\"" >&2
        exit 1
    fi
fi

#
# Print initial config report

cat << EOF
Scenario init. config:
    Dev.mode ................... ${DEVMODE}
    Source data files list ..... ${SRC_DATA_FILES_LIST}
    Calib. run settings list ... ${RUNS_LIST}
    Output dir ................. ${OUT_DIR_BASE}
    job id ..................... ${HTCONDOR_JOBINDEX}
EOF

#
# Get cell parameters

read -r RUN_NO CELL_IDX ENABLE_VETO < <(grep -v '^#' $RUNS_LIST | sed -e "$(( $HTCONDOR_JOBINDEX + 1 ))q;d" | awk '{print $1 " " $2 " " $3}')
if [ -z "${RUN_NO}" ] ; then
    echo "Error: empty run number (HTCONDOR_JOBINDEX=\"${HTCONDOR_JOBINDEX}\")" >&2
    echo "  line:" $(grep -v '^#' $RUNS_LIST | sed -e "$(( $HTCONDOR_JOBINDEX + 1 ))q;d")
    exit 1
fi
IFS=x read -r CELL_X CELL_Y < <(echo "${CELL_IDX}")  # TODO: sometimes (?) doesn't work
# This is conventional COMPASS/NA64 raw data files naming:
INPUT_FILE_LOOKUP_PATTERN="cdr.....-$(printf "%06d" ${RUN_NO}).dat$"
cat << EOF
Obtained settings:
    Run no ..................... ${RUN_NO}
    Cell ID .................... ${CELL_IDX} (=${CELL_X}, ${CELL_Y})
    Input files pattern ........ "${INPUT_FILE_LOOKUP_PATTERN}"
EOF
# Get input files list
INPUT_FILES=($(grep -G ${INPUT_FILE_LOOKUP_PATTERN} ${SRC_DATA_FILES_LIST}))
echo "    files to read .............. ${#INPUT_FILES[@]}"

if [ ${#INPUT_FILES[@]} -eq 0 ] ; then
    echo "Error: no files to read." >&2
    exit 2
fi

OUT_DIR=$(realpath "${OUT_DIR_BASE}/${CELL_IDX}")
mkdir -p ${OUT_DIR}
if [ ! -d ${OUT_DIR} ] ; then
    echo "Error: failed to create \"${OUT_DIR}\"" >&2
    exit 1
fi

#                                                                  ___________
# _______________________________________________________________/ Processing

#
# Check pedestal file exists
PEDESTALS_FILE=${OUT_DIR}/pedestals.dat
if [ ! -f ${PEDESTALS_FILE} ] ; then
    echo "Error: no pedestals file. Aborted." >&2
    exit 3
fi

#                                                     _________________________
# __________________________________________________/ Generate Runtime Configs

#
# Make pipeline config 2.1 conditionally enabling/disabling vetoing (vetoing should
# be disabled for periphery cells lik 0x0, 6x6, etc)
if [[ ${ENABLE_VETO} == 0 ]] ; then
    # remove vetoing handler if vetoing disabled
    sed -r '/^\s+#\s?%\(enableVetoing/,/^\s+#\s?enableVetoing\)%/d' ${BASE_DIR}/presets/run.d/run.02.fit-peaks.yaml.in \
        > run.02.yaml
else
    # just copy config with vetoing handler as is
    cp ${BASE_DIR}/presets/run.d/run.02.fit-peaks.yaml.in run.02.yaml
fi

if [ ! -f run.02.yaml ] ; then
    echo "Error: no cfg 2.1 file. Aborted." >&2
    exit 4
fi

# Copy pedestals to local dir
cp $PEDESTALS_FILE ./pedestals.dat

# Make get-fts config to fit peak and get width for pile-up removal
sed "s/%(xIdx)%/${CELL_X}/g" ${BASE_DIR}/presets/get-primary-peak-width.yaml.in \
    | sed -e "s/%(yIdx)%/${CELL_Y}/g" \
    > get-primary-peak-width.yaml
if [ ! -f get-primary-peak-width.yaml ] ; then
    echo "Error: failed to produce peak retrieval config file. Aborted." >&2
    exit 4
fi


#                                                                  ___________
# _______________________________________________________________/ Processing

#
# Activate supplementary environment, if need
if [ ! -z "${SUPP_ENV}" ] ; then
    source ${SUPP_ENV}
fi

#
# Stage 2, phase 1: Generate/reuse AvroDST with fitted peaks
#
# The AvroDST that must be produced here is quite large. Essentially, it is
# pre-reconstructed raw data (fitted peaks) with no cuts applied. Once
# procedure passes this phase it is doubtful that user might want to
# re-generate those files, se we prefer to re-use this file whenever possible
# and make note for the user in log that if one would like to have this file
# re-generated it has to be be moved or explicitly deleted. In dev. mode
# we use the file directly from output dir, for batch we make a copy...
LOCAL_AVRO_DST_FILE=fitted-peaks.events.db  # (as in config file)
AVRO_DST_FILE=${OUT_DIR}/${LOCAL_AVRO_DST_FILE}  # path at the output dir
AVRO_DST_FILE_PATH=${LOCAL_AVRO_DST_FILE}  # this var should be set to the input file path
if [ ! -f ${AVRO_DST_FILE} ] ; then
    echo -e "\nInfo: generating fitted peaks AvroDST for ECAL..."
    na64sw-pipe -m exp-handlers -m na64avro             \
        -m ${BASE_DIR}/libECALCalibHandlers.so          \
        -r ./run.02.yaml                                \
        ${INPUT_FILES[@]}                               \
        -o prefit-processed-${CELL_IDX}.root -N 50000
    rm -f pedestals.dat
    # Check results and die on failure or copy it to share. Note that local
    # copy will be deleted on next phase
    if [ ! -f ${LOCAL_AVRO_DST_FILE} ] ; then
        echo "Error: AvroDST file is not generated; abort." >&2
        exit 1
    else
        echo "Info: copying AvroDST file with fitted peaks"
        cp -v ${LOCAL_AVRO_DST_FILE} ${AVRO_DST_FILE}
    fi
else
    if [ -z ${DEVMODE} ] || [ ${DEVMODE} -eq 0 ] ; then
        # make local copy if prod run
        echo "Info: using existing AvroDST file with fitted peaks: ${AVRO_DST_FILE}"
        cp -v ${AVRO_DST_FILE} ${LOCAL_AVRO_DST_FILE}
    else
        # re-use remote if dev.mode
        AVRO_DST_FILE_PATH=${OUT_DIR}/${LOCAL_AVRO_DST_FILE}
        echo "Info: using existing AvroDST file with fitted peaks from share: ${AVRO_DST_FILE_PATH}"
    fi
fi
# Retrieve width information of the cell being calibrated
WIDTH_FIT_RESULT=${CELL_X}-${CELL_Y}-width.dat
#PEAK_FIT_REPORT_PRS=${CELL_X}-${CELL_Y}-0-peaks.pdf
echo -e "\nInfo: fitting width..."
na64sw-get-fts findWidth get-primary-peak-width.yaml prefit-processed.root
if [ ! -f ${WIDTH_FIT_RESULTS} ] ; then
    echo "Error: ${WIDTH_FIT_RESULTS} file is not generated; abort." >&2
    exit 1
fi

#
# Copy output data for further analysis
if [ -z ${DEVMODE} ] || [ ${DEVMODE} -eq 0 ] ; then
    mv -v ${WIDTH_FIT_RESULT}           ${OUT_DIR}
    # These files might be generated on peaks retrieval stage (yet we can
    # survive without this QA...)
    if [ -f prefit-processed.root ] ; then
        mv prefit-processed.root ${OUT_DIR}
    fi
    if [ -f "${CELL_X}-${CELL_Y}-?-width.pdf" ] ; then
        mv -v "${CELL_X}-${CELL_Y}-?-width.pdf" ${OUT_DIR}
    fi
fi

