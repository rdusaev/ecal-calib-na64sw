#!/bin/bash

# NOTE: This is a node-running script meant to be ran on the batch node
# Using zero information from stage-01, this script runs 2.1 config on the
# calibration data producing AvroDST output with fitted peaks. Pedestals
# are explicitly specified (not brought as calib. data).
#
# Environment variables affecting this script:
#   - HTCONDOR_JOBINDEX, required -- this job ID used to figure out the data to run on.
#   - SUPP_ENV, optional -- if given, will be source'd prior to na64sw utils execution.
#       Should be a shell script file providing additional environment definitions.
#   - BASE_DIR, required -- directory where this script is located, to infer paths to assets,
#       configs, etc.
# Inputs:
#   - dev. mode switch (enabled by `-d' option)
#   - list of input files (provided with `-s' option)
#   - runs list ASCII table (provided with `-r' option)
#   - output dir path (provided with `-o' option)
#   - presets/run.d/run.02.1.fit-peaks.yaml.in
# Outputs:
#   - ./fitted-peaks.events.db in cwd, moved to <out-dir>/<cell-idx>/fitted-peaks-<xIdx>x<yIdx>.root
#     at the end of the script
# Note, that processed.root is not of use at this (sub-)stage and gets removed.
# Example of running locally in dev mode:
#   $  HTCONDOR_JOBINDEX=18 BASE_DIR=$(readlink -f ..) \
#           SUPP_ENV=../supp-env.local.sh ../exec/stage-03.sh \
#           -d -s ../files-list.xxx.txt \
#           -r ../presets/calib-runs-2023A-2.txt \
#           -o ./out

if [ -z "${HTCONDOR_JOBINDEX}" ] ; then
    echo "Error: env. var HTCONDOR_JOBINDEX is not set" >&2
    exit 2
fi

if [ -z "${BASE_DIR}" ] ; then
    echo "Error: BASE_DIR environment variable is not set." >&2
    exit 2
fi

if [ ! -d ${BASE_DIR} ] ; then
    echo "Error: base dir \"${BASE_DIR}\" does not exist." >&2
    exit 2
fi

if [ ! -z "${SUPP_ENV}" ] && [ ! -f ${SUPP_ENV} ] ; then
    echo "Error: supp.env file \"${SUPP_ENV}\" does not exist." >&2
    exit 2
fi

#./na64sw-pipe -m na64avro -m exp-handlers -m msadc-viewer-window \
#       -r ../na64sw/utils/ecal-calib-lms/run.01.get-pedestals.yaml \
#       ~/data/cern/na64/2023e/ecal-calib/9184-2x3/* \
#       -o processed-2x3-pedestals.root -N 200000

#
# Parse command line options
usage() { echo "Usage: $0 [-d] [-s <src-dat-files-list>] [-r <calibs-runs-list>] [-o <out-dir>]" 1>&2; exit 1; }

while getopts ":ds:r:o:" opt; do
    case "${opt}" in
        d)
            DEVMODE=1
            ;;
        s)
            SRC_DATA_FILES_LIST=${OPTARG}
            ;;
        r)
            RUNS_LIST=${OPTARG}
            ;;
        o)
            OUT_DIR_BASE=${OPTARG}
            ;;
        *)
            echo "Error: unkown option \"${opt}\"" >&2
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${SRC_DATA_FILES_LIST}" ] || [ -z "${RUNS_LIST}" ] || [ -z "{OUT_DIR_BASE}" ] ; then
    echo "Error: at least one of mandatory opts is not set" >&2
    usage
fi

#
# Assure files exist, create output dir

if [ ! -f ${SRC_DATA_FILES_LIST} ] ; then
    echo "Error: source data files list \"${SRC_DATA_FILES_LIST}\" does not exist." >&2
    exit 1
fi

if [ ! -f ${RUNS_LIST} ] ; then
    echo "Error: runs list file \"${RUNS_LIST}\" does not exist." >&2
    exit 1
fi

if [ ! -d ${OUT_DIR_BASE} ] ; then
    if ! mkdir -p ${OUT_DIR_BASE} ; then
        echo "Error: failed to create \"${OUT_DIR_BASE}\"" >&2
        exit 1
    fi
fi

#
# Print initial config report

cat << EOF
Scenario init. config:
    Dev.mode ................... ${DEVMODE}
    Source data files list ..... ${SRC_DATA_FILES_LIST}
    Calib. run settings list ... ${RUNS_LIST}
    Output dir ................. ${OUT_DIR_BASE}
    job id ..................... ${HTCONDOR_JOBINDEX}
EOF

#
# Get cell parameters

read -r RUN_NO CELL_IDX ENABLE_VETO < <(grep -v '^#' $RUNS_LIST | sed -e "$(( $HTCONDOR_JOBINDEX + 1 ))q;d" | awk '{print $1 " " $2 " " $3}')
if [ -z "${RUN_NO}" ] ; then
    echo "Error: empty run number (HTCONDOR_JOBINDEX=\"${HTCONDOR_JOBINDEX}\")" >&2
    echo "  line:" $(grep -v '^#' $RUNS_LIST | sed -e "$(( $HTCONDOR_JOBINDEX + 1 ))q;d")
    exit 1
fi
IFS=x read -r CELL_X CELL_Y < <(echo "${CELL_IDX}")  # TODO: sometimes (?) doesn't work
# This is conventional COMPASS/NA64 raw data files naming:
INPUT_FILE_LOOKUP_PATTERN="cdr.....-$(printf "%06d" ${RUN_NO}).dat$"
cat << EOF
Obtained settings:
    Run no ..................... ${RUN_NO}
    Cell ID .................... ${CELL_IDX} (=${CELL_X}, ${CELL_Y})
    Input files pattern ........ "${INPUT_FILE_LOOKUP_PATTERN}"
EOF
# Get input files list
INPUT_FILES=($(grep -G ${INPUT_FILE_LOOKUP_PATTERN} ${SRC_DATA_FILES_LIST}))
echo "    files to read .............. ${#INPUT_FILES[@]}"

if [ ${#INPUT_FILES[@]} -eq 0 ] ; then
    echo "Error: no files to read." >&2
    exit 2
fi

OUT_DIR=$(realpath "${OUT_DIR_BASE}/${CELL_IDX}")
mkdir -p ${OUT_DIR}
if [ ! -d ${OUT_DIR} ] ; then
    echo "Error: failed to create \"${OUT_DIR}\"" >&2
    exit 1
fi

#                                                     _________________________
# __________________________________________________/ Generate Runtime Configs

# Make pipeline config substituting cell index for stage 2.2's config
sed "s/%(xIdx)%/${CELL_X}/g" ${BASE_DIR}/presets/run.d/run.02.2.get-raw-spectra.yaml.in \
    | sed -e "s/%(yIdx)%/${CELL_Y}/g" \
    | sed -e "s/%(cellIdx)%/${CELL_IDX}/g" \
    > run.02.2.yaml
if [ ! -f run.02.2.yaml ] ; then
    echo "Error: no cfg 2.2 file. Aborted." >&2
    exit 4
fi

# Make get-fts config to fit peak and get width for pile-up removal
sed "s/%(xIdx)%/${CELL_X}/g" ${BASE_DIR}/presets/get-primary-peak-width.yaml.in \
    | sed -e "s/%(yIdx)%/${CELL_Y}/g" \
    > get-primary-peak-width.yaml
if [ ! -f get-primary-peak-width.yaml ] ; then
    echo "Error: failed to produce peak retrieval config file. Aborted." >&2
    exit 4
fi

#                                                                  ___________
# _______________________________________________________________/ Processing

# Activate supplementary environment, if need
if [ ! -z "${SUPP_ENV}" ] ; then
    source ${SUPP_ENV}
fi

#
# Stage 2, phase 2: Do the time clustering, generate AvroDST with clean data (with
#       no pile-ups) and keep processed.root to get scale transform/QA.
#       Comparing to previous phase this one is not too long, but we still tend
#       to re-use existing results in dev.mode.
# TODO: avro source file config is not installed in NA64sw. Temporarily, put
#       the schema file and config here to ${BASE_DIR}/presets/tmp/
#       while it should be at ${NA64SW_PREFIX}/share/...
CLEAN_TIME_CLUSTER_REPORT=prime-ECAL-cluster-${CELL_IDX}.root
CLEAN_TIME_AVRO_DST=selected-peaks-${CELL_IDX}.events.db
# if in dev.mode, copy files to local dir
if [ -f ${OUT_DIR}/${CLEAN_TIME_AVRO_DST} ] && [ -f ${OUT_DIR}/${CLEAN_TIME_CLUSTER_REPORT} ] ; then
    echo "Info: copying clean DST locally (dev.mode, result exists)"
    cp -v ${OUT_DIR}/${CLEAN_TIME_AVRO_DST} ./
    cp -v ${OUT_DIR}/${CLEAN_TIME_CLUSTER_REPORT} ./
fi
# if no clean DST or no report, do
if [ ! -f ${CLEAN_TIME_AVRO_DST} ] || [ ! -f ${CLEAN_TIME_CLUSTER_REPORT} ]  ; then
    echo -e "\nInfo: clustering fitted peaks based on AvroDST for ECAL..."
    na64sw-pipe -m exp-handlers -m na64avro -r ./run.02.2.yaml    \
        -i ${BASE_DIR}/presets/tmp/source-avro.yaml \
        ${AVRO_DST_FILE_PATH} \
        -o ${CLEAN_TIME_CLUSTER_REPORT}
    # Remove local copy of pre-reconstructed AvroDST
    if [ -f ${LOCAL_AVRO_DST_FILE} ] ; then
        rm ${LOCAL_AVRO_DST_FILE}
    fi
    # Check results and die on failure
    if [ ! -f ${CLEAN_TIME_CLUSTER_REPORT} ] ; then
        echo "Error: ${CLEAN_TIME_CLUSTER_REPORT} file is not generated; abort." >&2
        exit 1
    fi
    if [ ! -f ${CLEAN_TIME_AVRO_DST} ] ; then
        echo "Error: ${CLEAN_TIME_AVRO_DST} file is not generated; abort." >&2
        exit 1
    fi
fi

#
# Retrieve peaks information at the cell being calibrated
PEAK_FIT_RESULTS=${CELL_X}-${CELL_Y}-peaks.dat
WIDTH_FIT_RESULT=${CELL_X}-${CELL_Y}-width.dat
#PEAK_FIT_REPORT_PRS=${CELL_X}-${CELL_Y}-0-peaks.pdf
if [ -z ${DEVMODE} ] || [ ${DEVMODE} -eq 0 ] || [ ! -f ${PEAK_FIT_RESULTS} ]  ; then
    echo -e "\nInfo: fitting primary peaks..."
    na64sw-get-fts findMax get-primary-peak-width.yaml ${CLEAN_TIME_CLUSTER_REPORT}
    if [ ! -f ${PEAK_FIT_RESULTS} ] ; then
        echo "Error: ${PEAK_FIT_RESULTS} file is not generated; abort." >&2
        exit 1
    fi
    na64sw-get-fts findWidth get-primary-peak-width.yaml ${CLEAN_TIME_CLUSTER_REPORT}
    if [ ! -f ${WIDTH_FIT_RESULTS} ] ; then
        echo "Error: ${WIDTH_FIT_RESULTS} file is not generated; abort." >&2
        exit 1
    fi
    rm get-primary-peak-width.yaml
else
    echo "Info: peak fitting result file exists, phase skept."
fi

#
# Stage 2.3: Use fit mean and sigma estimate to restrict raw energy deposition
#            with mu +/- 3*sigma in the main cell ang generate input for LMS
#            procedure
# - retrieve values from fit result
AMP_RANGE_MIN=$(grep -e '.-.-1' ${PEAK_FIT_RESULTS} | awk '{print $2 - 3*$3}')
AMP_RANGE_MAX=$(grep -e '.-.-1' ${PEAK_FIT_RESULTS} | awk '{print $2 + 3*$3}')
# - make pipeline config substituting cell index for stage 2.2's config
sed "s/%(xIdx)%/${CELL_X}/g" ${BASE_DIR}/presets/run.d/run.02.3.get-input-values.yaml.in \
    | sed -e "s/%(yIdx)%/${CELL_Y}/g" \
    | sed -e "s/%(mainMinAmp)%/${AMP_RANGE_MIN}/g" \
    | sed -e "s/%(mainMaxAmp)%/${AMP_RANGE_MAX}/g" \
    > run.02.3.yaml
if [ ! -f run.02.3.yaml ] ; then
    echo "Error: no cfg 2.3 file. Aborted." >&2
    exit 4
fi

# Generate LMS input file
LMS_INPUT_FILE=ecal-calib-data-${CELL_X}x${CELL_Y}.dat
if [ -z ${DEVMODE} ] || [ ${DEVMODE} -eq 0 ] || [ ! -f ${LMS_INPUT_FILE} ]  ; then
    echo -e "\nInfo: generating LMS input file..."
    na64sw-pipe -m exp-handlers -m na64avro             \
        -m ${BASE_DIR}/libECALCalibHandlers.so          \
        -r ./run.02.3.yaml                              \
        -i ${BASE_DIR}/presets/tmp/source-avro.yaml     \
        ${CLEAN_TIME_AVRO_DST}                          \
        -o processed-lms-input-${CELL_X}-${CELL_Y}.root
    if [ ! -f ${LMS_INPUT_FILE} ] ; then
        echo "Error: ${LMS_INPUT_FILE} file is not generated; abort." >&2
        exit 1
    fi
else
    echo "Info: dev. mode is on and LMS input file exists, skept."
fi

#
# Copy output data for further analysis
if [ -z ${DEVMODE} ] || [ ${DEVMODE} -eq 0 ] ; then
    mv -v ${CLEAN_TIME_CLUSTER_REPORT}  ${OUT_DIR}
    mv -v ${CLEAN_TIME_AVRO_DST}        ${OUT_DIR}
    mv -v ${PEAK_FIT_RESULTS}           ${OUT_DIR}
    mv -v ${WIDTH_FIT_RESULT}           ${OUT_DIR}
    mv -v ${LMS_INPUT_FILE}             ${OUT_DIR}
    # These files might be generated on peaks retrieval stage (yet we can
    # survive without this QA...)
    if [ -f prefit-processed.root ] ; then
        mv prefit-processed.root ${OUT_DIR}
    fi
    if [ -f "${CELL_X}-${CELL_Y}-?-peaks.pdf" ] ; then 
        mv -v "${CELL_X}-${CELL_Y}-?-peaks.pdf" ${OUT_DIR}
    fi
    if [ -f "${CELL_X}-${CELL_Y}-?-width.pdf" ] ; then
        mv -v "${CELL_X}-${CELL_Y}-?-width.pdf" ${OUT_DIR}
    fi
    if [ -f processed-lms-input-${CELL_X}-${CELL_Y}.root ] ; then
        mv processed-lms-input-${CELL_X}-${CELL_Y}.root ${OUT_DIR}
    fi
fi


