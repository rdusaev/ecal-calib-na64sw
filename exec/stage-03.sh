#!/bin/bash

BASE_DIR="$( cd "$( dirname "`readlink -f ${BASH_SOURCE[0]}`" )/.." && pwd )"
source $BASE_DIR/exec/environment.sh

cellX=$1
cellY=$2
runID=$3
chunkID=$4
srcDataFile=/eos/user/${USER:0:1}/${USER}/na64/batch.out/$6/$5
timeFitResults=/eos/user/${USER:0:1}/${USER}/na64/batch.out/$7/time-peaks-$cellX-$cellY.dat

if [ ! -f $timeFitResults ] ; then
    >&2 echo "Error: no file $timeFitResults"
    exit 1
fi

read -r mainTimeMean mainTimeSigma <<<$(
    grep -v '#' $timeFitResults \
        | grep $cellX-$cellY-1 \
        | awk '{print $2 " " $3 }' )
# TODO: RawECALTimeClusterChecks should make difference bw/ main and prs values
#read -r prsTimeMean prsTimeSigma <<<$(
#    grep -v '#' $timeFitResults \
#        | grep $cellX-$cellY-1 \
#        | awk '{print $2 " " $3 }' )

SF=3
timeMainMin=$(python3 -c "print('%e'%($mainTimeMean - $SF*$mainTimeSigma))")
timeMainMax=$(python3 -c "print('%e'%($mainTimeMean + $SF*$mainTimeSigma))")


cat <<EOF > run-config.yaml
---
pipeline:
  - _type: SADCPlotWaveform
    baseName: "amps-raw-before"
    ampNBins: 120
    ampRange: [0, 2400]

  - _type: SADCClusterByTime
    applyTo: "kin == ECAL"
    method:
      _type: simple
      timeDeltaNs: 90  #10 for e runs
      nMaxItems: 60

  - _type: RawECALTimeClusterChecks
    #invert: true
    setHitValues: true
    applyTo: "kin == ECAL"
    xIdx: $cellX
    yIdx: $cellY
    timeWindow: [$timeMainMin, $timeMainMax]
    ratio: 0.1

  - _type: SADCPlotWaveform
    baseName: "amps-raw-after"
    ampNBins: 120
    ampRange: [0, 2400]

  - _type: DumpLMSInput
    applyTo: "kin == ECAL"
    output: "calib-dump.${cellX}-${cellY}.${runID}-${chunkID}.dat"
...
EOF

echo "===>>> Dump of run-config.yaml:"
cat run-config.yaml
echo "<<<=== End of fump."

na64sw-pipe \
    -m na64avro-common \
    -m na64avro \
    -m exp-handlers \
    -m ${BASE_DIR}/libECALCalibHandlers.so \
    -i $NA64SW_PREFIX/share/na64sw/source-avro.yaml \
    -r run-config.yaml -o summary.calib-dump.${cellX}-${cellY}.${runID}-${chunkID}.root \
    $srcDataFile

rm -fv run-config.yaml

