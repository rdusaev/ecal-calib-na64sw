#!/bin/bash

# ECAL calibration deployment script
# Creates files and directories to facilitate ECAL calibration job and submits
# it. Usage:
#   $ submit-calib.sh <tag> <runs-table>
# Where:
#   - <tag>         must be unique identifier for the calibration task
#   - <runs-table>  a .txt file containing run number and cell index
# Environment variables affecting script's behaviour:
#   - RUN_WS        base directory where <tag>/ subdir will be created to keep
#                   scripts, logs, etc. Typically, is your /afs/work/ dir
#   - DAT_WS        base directory where <tag>/ subdir will be created to keep
#                   all the data.
#   - FILES_LIST    A raw data files list cache

# Retrieve script location (needed to calc abs. paths). Note, that exec scripts
# and other assets are considered to be relative to this path.
BASE_DIR="$( cd "$( dirname "`readlink -f ${BASH_SOURCE[0]}`" )/.." && pwd )"

# User confirmation helper function
confirm() {
    # Usage
    #if confirm "Please confirm (y/n): " ; then
    #    echo "Confirmed."
    #else
    #    echo "Denied."
    #fi
    while true ; do
        echo -e "\033[1;33m$1\033[0m"
        read YN
        case ${YN} in
            [Nn] )  return 1 ;;
            [Yy] )  return 0 ;;
            * ) echo "Couldn't interpret \"$YN\""
        esac
    done
}

#
# Entry point:

# Check command line arguments
if [ "$#" -ne 2 ] ; then
    echo "Error: wrong number of command line arguments." >&2
    echo -e "Expected usage:\n $0 <tag> <runs-table-file>"
    exit 1
fi

TAG=$1
RUNS_TABLE=$(readlink -f $2)

if [ ! -f ${RUNS_TABLE} ] ; then
    echo "Error: file $RUNS_TABLE does not exist."
    exit 1
fi

#
# (Re)create dirs

RUNWS_DIR=${RUN_WS:-/afs/cern.ch/work/${USER:0:1}/${USER}/na64/ecal-calib}/${TAG}
DATWS_DIR=${DAT_WS:-/eos/user/${USER:0:1}/${USER}/na64/ecal-calib}/${TAG}

# ...check for collisions, ask user to confirm deletion, if need
if [ -d $RUNWS_DIR ] ; then
    if confirm "Directory \"$RUNWS_DIR\" exists. Delete?" ; then
        rm -rf ${RUNWS_DIR}
    else
        echo "Error: exit due to file collision(s)." >&2
        exit 1
    fi
fi

if [ -d $DATWS_DIR ] ; then
    if confirm "Directory \"$DATWS_DIR\" exists. Delete?" ; then
        rm -rf ${DATWS_DIR}
    else
        echo "Error: exit due to file collision(s)." >&2
        exit 1
    fi
fi
# ...create dirs
mkdir -p "${RUNWS_DIR}"
if [ ! -d ${RUNWS_DIR} ] ; then
    echo "Error: failed to create ${RUNWS_DIR}" >&2
    exit 1
fi
mkdir -p "${DATWS_DIR}"
if [ ! -d ${DATWS_DIR} ] ; then
    echo "Error: failed to create ${DATWS_DIR}" >&2
    exit 1
fi

# ...copy runs list table to run-dir
THIS_RUNS_TABLE=${RUNWS_DIR}/runs.txt
cp ${RUNS_TABLE} ${THIS_RUNS_TABLE}

#
# Assure files list cache exists
: ${FILES_LIST:=~/.na64-files-list.txt}
if [ ! -f ${FILES_LIST} ] ; then
    echo "Info: caching list of raw data files into \"${FILES_LIST}\", may take a while..."
    # Retrieve list of files greater than 100kb
    ls -l /eos/experiment/na64/data/cdr/ | awk '{ if ( $5+0 > 100 ) print "/eos/experiment/na64/data/cdr/" $9}' > ${FILES_LIST}
else
    echo "Info: using files list cache from \"${FILES_LIST}\"."
fi
# Get list of files of only relevant runs
RUNS=()
# Read the "run table" file assuming the run number being at the first column
while IFS="" read -r LINE || [ -n "$LINE" ] ; do
    RUNS+=($(printf 'cdr.*-%06d.dat' $(printf '%s\n' "$LINE" | awk '{print $1}')))
done < <(grep -o '^[^#]*' "$2")
NCELLS=$(grep -o '^[^#]*' "$2" | wc -l)  # effectively, number of jobs
RUNS_PATTERN=$(printf "|%s" "${RUNS[@]}")  # concatenate array of patterns into OR-string template
RUNS_PATTERN=${RUNS_PATTERN:1}  # strip initial `|'
# Filter files list with obtained pattern and put it into runs dir
# Output must be sorted with respect to run number, then chunk number. It is
# in general importantant for some handlers to provide spills in order...
THIS_FILES_LIST=${RUNWS_DIR}/chunks.txt
grep -E "${RUNS_PATTERN}" ${FILES_LIST} \
    | sort -n -t '-' -k2,3 \
    > ${THIS_FILES_LIST}

#
# Stage 01, extract per-run pedestals and zero-suppression calibration info

mkdir -p ${RUNWS_DIR}/stage-1.d/exec
mkdir -p ${RUNWS_DIR}/stage-1.d/logs
cp ${BASE_DIR}/exec/stage-01.sh             ${RUNWS_DIR}/stage-1.d/exec
cp ${BASE_DIR}/exec/stage-01-aggregate.sh   ${RUNWS_DIR}/stage-1.d/exec

mkdir -p ${RUNWS_DIR}/stage-2.d/exec
mkdir -p ${RUNWS_DIR}/stage-2.d/logs
cp ${BASE_DIR}/exec/stage-02.sh ${RUNWS_DIR}/stage-2.d/exec

# ...

#
# HTCondor ClassAd for 1st stage, "getting base levels"
STAGE1_SUB=${RUNWS_DIR}/stage-1.sub
cat <<-EOF > ${STAGE1_SUB}
	output                  = ${RUNWS_DIR}/stage-1.d/logs/\$(Process).out.txt
	error                   = ${RUNWS_DIR}/stage-1.d/logs/\$(Process).err.txt
	executable              = ${RUNWS_DIR}/stage-1.d/exec/stage-01.sh
	arguments               = -s ${THIS_FILES_LIST} -r ${THIS_RUNS_TABLE} -o ${DATWS_DIR}
	universe                = vanilla
	should_transfer_files   = NO
	transfer_output_files   =
	on_exit_remove          = (ExitBySignal == False) && (ExitCode == 0)
	max_retries             = 3
	requirements            = Machine =!= LastRemoteHost
	max_transfer_output_mb  = 2048
	+MaxRuntime             = 28800
	+AccountingGroup        = "group_u_COMPASS.u_vy"
	userLog                 = ${RUNWS_DIR}/stage-1.htcondor-log
	environment             = "HTCONDOR_JOBINDEX=\$(Process) BASE_DIR=${BASE_DIR} SUPP_ENV=${BASE_DIR}/presets/supp-env.cernbatch.sh"
	MY.WantOS               = "el7"
	queue                   ${NCELLS}
	EOF
STAGE1AGG_SUB=${RUNWS_DIR}/stage-1agg.sub
cat <<-EOF > ${STAGE1AGG_SUB}
	output                  = ${RUNWS_DIR}/stage-1.d/logs/\$(Process)-agg.out.txt
	error                   = ${RUNWS_DIR}/stage-1.d/logs/\$(Process)-agg.err.txt
	executable              = ${RUNWS_DIR}/stage-1.d/exec/stage-01-aggregate.sh
	arguments               = -r ${THIS_RUNS_TABLE} -o ${DATWS_DIR}
	universe                = vanilla
	should_transfer_files   = NO
	transfer_output_files   =
	on_exit_remove          = (ExitBySignal == False) && (ExitCode == 0)
	max_retries             = 3
	requirements            = Machine =!= LastRemoteHost
	max_transfer_output_mb  = 2048
	+MaxRuntime             = 1800
	+AccountingGroup        = "group_u_COMPASS.u_vy"
	userLog                 = ${RUNWS_DIR}/stage-1.htcondor-log
	environment             = "BASE_DIR=${BASE_DIR} SUPP_ENV=${BASE_DIR}/presets/supp-env.cernbatch.sh"
	MY.WantOS               = "el7"
	queue                   1
	EOF
#
# HTCondor ClassAd for 2nd stage "fitting peaks"
STAGE2_SUB=${RUNWS_DIR}/stage-2.sub
cat <<-EOF > ${STAGE2_SUB}
	output                  = ${RUNWS_DIR}/stage-2.d/logs/\$(Process).out.txt
	error                   = ${RUNWS_DIR}/stage-2.d/logs/\$(Process).err.txt
	executable              = ${RUNWS_DIR}/stage-2.d/exec/stage-02.sh 
	arguments               = -r ${THIS_RUNS_TABLE} -o ${DATWS_DIR}
	universe                = vanilla
	should_transfer_files   = NO
	transfer_output_files   =
	on_exit_remove          = (ExitBySignal == False) && (ExitCode == 0)
	max_retries             = 3
	requirements            = Machine =!= LastRemoteHost
	max_transfer_output_mb  = 4096
	+MaxRuntime             = 14400
	+AccountingGroup        = "group_u_COMPASS.u_vy"
	userLog                 = ${RUNWS_DIR}/stage-2.htcondor-log
	environment             = "HTCONDOR_JOBINDEX=\$(Process) BASE_DIR=${BASE_DIR} SUPP_ENV=${BASE_DIR}/presets/supp-env.cernbatch.sh"
	MY.WantOS               = "el7"
	queue                   ${NCELLS}
	EOF

#
# Render DAG
JOB_DAG=${RUNWS_DIR}/EC-${TAG}.dag
cat <<-EOF > ${JOB_DAG}
	JOB EC-${TAG}-STG1 ${STAGE1_SUB}
	JOB EC-${TAG}-STG1AGG ${STAGE1AGG_SUB}
	JOB EC-${TAG}-STG2 ${STAGE2_SUB}
	PARENT EC-${TAG}-STG1 CHILD EC-${TAG}-STG1AGG
	PARENT EC-${TAG}-STG1AGG CHILD EC-${TAG}-STG2
	EOF

# Submit DAG
condor_submit_dag ${JOB_DAG}

# For per-stage submission use condor_submit. For instance, submit jobs for
# 1st stage only:
#condor_submit -batch-name EC-${TAG}-STAGE-1 ${STAGE1_SUB}

