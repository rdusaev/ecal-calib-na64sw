#!/bin/bash

BASE_DIR="$( cd "$( dirname "`readlink -f ${BASH_SOURCE[0]}`" )/.." && pwd )"
source $BASE_DIR/exec/environment.sh

cellX=$1
cellY=$2
runID=$3
chunkID=$4
outDSTFile=$5
PEDESTALS_DIR=/eos/user/${USER:0:1}/${USER}/na64/batch.out/$6
FIT_RESULTS_DIR=/eos/user/${USER:0:1}/${USER}/na64/batch.out/$7
IN_FILE_RAW=$8
 
# Copy pedestals file corresponding to current chunk
#
cp -v $PEDESTALS_DIR/pedestals.$runID-$chunkID.dat pedestals.dat

# Read fit results to obtain discrimination boundaries
#

FIT_RESULTS_FILE=$FIT_RESULTS_DIR/fit-results-$cellX-$cellY.dat
if [ ! -f $FIT_RESULTS_FILE ] ; then
    >&2 echo "Error: no file $FIT_RESULTS_FILE"
    exit 1
fi

read -r rMainMean rMainSigma sumMainMean sumMainSigma <<<$(
    grep -v '#' $FIT_RESULTS_FILE \
        | grep $cellX-$cellY-1 \
        | awk '{print $2 " " $3 " " $4 " " $5 }' )
read -r rPrsMean rPrsSigma sumPrsMean sumPrsSigma <<<$(
    grep -v '#' $FIT_RESULTS_DIR/fit-results-$cellX-$cellY.dat \
        | grep $cellX-$cellY-0 \
        | awk '{print $2 " " $3 " " $4 " " $5 }' )

SF=3
sumMainMin=$(python3 -c "print('%e'%($sumMainMean - $SF*$sumMainSigma))")
sumMainMax=$(python3 -c "print('%e'%($sumMainMean + $SF*$sumMainSigma))")
sumPrsMin=$( python3 -c "print('%e'%($sumPrsMean  - $SF*$sumPrsSigma ))")
sumPrsMax=$( python3 -c "print('%e'%($sumPrsMean  + $SF*$sumPrsSigma ))")

LF=1
rMainMin=$(python3 -c "print('%e'%($rMainMean - $SF*$rMainSigma))")
rMainMax=$(python3 -c "print('%e'%($rMainMean + $SF*$rMainSigma))")
rPrsMin=$( python3 -c "print('%e'%($rPrsMean  - $SF*$rPrsSigma ))")
rPrsMax=$( python3 -c "print('%e'%($rPrsMean  + $SF*$rPrsSigma ))")

# Render basic config for stage-02 using values
#

DETS2PLOT="kin == ECAL || kin == VETO || kin == VHCAL"

# TODO: in principle, at this stage we can obtain time shifts (tmean) values
#       for cells by running coarse time clustering and analyzing mean time
#       within largest cluster). These values can be then used for finer time
#       clustering in the produce-input stage where best clusters are obtained
cat <<EOF > run-config.yaml
---
pipeline:
  #
  # Get initial estimate of the pedestal value (we deliberately
  - _type: SADCGetPedestalsByFront
    nSamples: 3  # applied spearetely to even and odd samples
    useMinIfDeltaExceeds: 20
  #- _type: MSADCFindPedestalsLagrangeTriplets
  #  #dumpBadWaveformsTo: "bad-wfs.dat"
  #  #onFailure: quiet  # TODO
  #  inlineSubtract: true
  #  cutOff: 25
  # If calculated pedestal exceeds N*\sigma with respect to previously obtained
  # mean values, use mean values
  - _type: SADCAssignPedestals
    source: "pedestals.dat"
    setNonExistingToNan: false
    ifCurrentExceedsNSigma: 3
  - _type: SADCSubtractPedestals
    nullifyNegative: false  # keep negative waveforms

  # Estimate linear sum
  - _type: SADCLinearSum
    zeroNegativeValues: true
    applyTo: "$DETS2PLOT"

  # Select abs max
  - _type: SADCFindMaxSimple
    applyTo: "$DETS2PLOT"

  # plot waveforms before discrimination
  - _type: SADCPlotWaveform
    applyTo: "$DETS2PLOT"
    baseName: "amps-raw"
    ampNBins: 120
    ampRange: [0, 2400]

  # Discriminate by linear sum on main part
  - _type: Discriminate
    _label: "main-linsum-cut"
    applyTo: "kin == ECAL && xIdx == $cellX && yIdx == $cellY && zIdx == 1"
    expression: '$sumMainMin < sadcHits.rawData.sum < $sumMainMax'
    invert: true
    discriminateEvent: true

  # Discriminate by linear sum on preshower part
  - _type: Discriminate
    _label: "prs-linsum-cut"
    applyTo: "kin == ECAL && xIdx == $cellX && yIdx == $cellY && zIdx == 0"
    expression: '$sumPrsMin < sadcHits.rawData.sum < $sumPrsMax'
    invert: true
    discriminateEvent: true

  # Discriminate by sum/max ratio main part
  - _type: Discriminate
    _label: "main-r-cut"
    applyTo: "kin == ECAL && xIdx == $cellX && yIdx == $cellY && zIdx == 1"
    expression: '$rMainMin < sadcHits.rawData.chargeVsMaxAmpRatio < $rMainMax'
    invert: true
    discriminateEvent: true

  # Discriminate by sum/max ratio preshower part
  - _type: Discriminate
    _label: "prs-r-cut"
    applyTo: "kin == ECAL && xIdx == $cellX && yIdx == $cellY && zIdx == 0"
    expression: '$rPrsMin < sadcHits.rawData.chargeVsMaxAmpRatio < $rPrsMax'
    invert: true
    discriminateEvent: true

  # plot WFs after discrimination
  - _type: SADCPlotWaveform
    applyTo: "$DETS2PLOT"
    baseName: "amps-raw-filtered"
    ampNBins: 120
    ampRange: [0, 2400]

  - _type: Histogram1D
    applyTo: "$DETS2PLOT"
    value: sadcHits.rawData.sum
    histName: "lin-sum"
    histDescr: "Linear sum, {TBName}"
    nBins: 500
    range: [0, 50000]

  - _type: Histogram1D
    applyTo: "$DETS2PLOT"
    value: sadcHits.rawData.chargeVsMaxAmpRatio
    histName: "lin-sum-max-ratio"
    histDescr: "S / A_max, {TBName}"
    nBins: 150
    range: [0, 15]

  # Fit with Moyal model
  - _type: SADCFitWaveforms
    applyTo: "$DETS2PLOT"
    absNoiseThreshold: 25

  - _type: Histogram2D
    applyTo: "$DETS2PLOT"
    histName: "hit-lsum-vs-max-val"
    histDescr: "MSADC raw sum vs raw max, {TBName} ; sum [raw chan] ; max [raw chan]"

    valueX: sadcHits.rawData.sum
    nBinsX: 500
    rangeX: [0, 50000]

    valueY: sadcHits.rawData.maxAmp
    nBinsY: 410
    rangeY: [0, 4100]

  # Save the results
  - _type: AvroSaveEvents
    codec: "lzma"  # possible values: null, deflate, [lzma, snappy]
    file: "$outDSTFile"
    blockSizeKb: 2048

  # Plot absolute fitted time vs max amp
  # Commented lines don't work as nested type is used (should rely on
  # HDQL-based getter)
  #- _type: Histogram2D
  #  applyTo: "kin == ECAL || kin == VETO || kin == VHCAL"
  #  histName: "hit-lsum-vs-max-val"
  #  histDescr: "MSADC raw sum vs raw max, {TBName} ; sum [raw chan] ; max [raw chan]"
  #  valueX: sadcHits.rawData.sum
  #  nBinsX: 500
  #  rangeX: [0, 50000]
  #  valueY: sadcHits.rawData.maxAmp
  #  nBinsY: 410
  #  rangeY: [0, 4100]
  #- _type: Histogram1D
  #  applyTo: "kin == ECAL || kin == VETO || kin == VHCAL"
  #  histName: "time-fitted"
  #  value: "sadcHits.rawData.maxima.time"
  #  range: [0, 400]
  #  nBins: 400
  # use ad-hoc instead
  - _type: SADCPlotMaxs
    applyTo: "$DETS2PLOT"
    ampNBins: 1024
    ampRange: [0, 4096]
    baseName: fitted-abs-time-vs-max
...
EOF

echo "===>>> Dump of run-config.yaml:"
cat run-config.yaml
echo "<<<=== End of fump."

#gdb -batch -ex "run" -ex "bt" --args
na64sw-pipe \
    -m na64avro-common \
    -m na64avro \
    -m exp-handlers \
    -r run-config.yaml -o summary.selection.$cellX-$cellY.$runID-$chunkID.root \
    $IN_FILE_RAW

rm -v run-config.yaml
rm -v pedestals.dat

