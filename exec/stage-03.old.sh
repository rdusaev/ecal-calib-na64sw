#!/bin/bash

# (un-)comment to hide (see) commands being executed
set -o xtrace

BASE_DIR=~/cernbox/autosync/na64/ecal-calib.2analyze/
STENCIL=5x5-wide-spot

# 50GeV 2023
#TAG=28-01-024-50GeV-001
#BEAM_E=50
# 100Gev 2023
TAG=27-01-024-100GeV-001
BEAM_E=100

#
# iteration 0
echo -e "\033[1;34mIteration #0\033[0m (preliminary, full stencil):"

# Get preliminary calibrations using wide profile, not much affected by
# periphery cells:
./exec/ecp-resolve -b ${BEAM_E} -Slms-svd,full \
    ${BASE_DIR}/${TAG}/ecal-calib-data-2x[34].dat \
    -o ${TAG}.0.dat
if [ $? -ne 0 ]; then
    echo "Iterative solution stopped due to solver process exit code $?" >&2
    exit 1
fi
# Use preliminary calibrations to get coarse estimates of shower profile
echo -e "\033[0;34mIteration #0.2\033[0m getting conv.profile values:"
python3 py/profile.py \
        ${TAG}.0.dat \
        ${BASE_DIR}/${TAG}/ecal-calib-data-2x[34].dat \
    | tee profile.0.dat

#
# iteration 1
echo -e "\033[1;34mIteration #1\033[0m (using coarse conv.shower profile, full stencil):"

# Get more accurate calibrations based on the coarse shower profile, do not
# include the whole data set so far
./exec/ecp-resolve -p profile.0.dat -b ${BEAM_E} -Slms-svd,full \
        ${BASE_DIR}/${TAG}/ecal-calib-data-2x[34].dat \
        -o ${TAG}.1.dat
if [ $? -ne 0 ]; then
    echo "Iterative solution stopped due to solver process exit code $?" >&2
    exit 1
fi
# Extract more accurate shower profile
echo -e "\033[0;34mIteration #1.2\033[0m getting conv.profile values:"
python3 py/profile.py \
        ${TAG}.1.dat \
        ${BASE_DIR}/${TAG}/ecal-calib-data-2x[34].dat \
    | tee profile.1.dat

#
# iteration 2
echo -e "\033[1;34mIteration #2\033[0m (using coarse conv.shower profile; extended set, no periphery; full stencil):"

# Get more accurate calibrations based on the coarse shower profile, based on
# larger data set
./exec/ecp-resolve -p profile.1.dat -b ${BEAM_E} -Slms-svd,full \
        ${BASE_DIR}/${TAG}/ecal-calib-data-[123]x[1234].dat \
        -o ${TAG}.2.dat
if [ $? -ne 0 ]; then
    echo "Iterative solution stopped due to solver process exit code $?" >&2
    exit 1
fi
# Extract more accurate shower profile, based on broader data set
echo -e "\033[0;34mIteration #2.2\033[0m getting conv.profile values:"
python3 py/profile.py \
        ${TAG}.2.dat \
        ${BASE_DIR}/${TAG}/ecal-calib-data-[123]x[1234].dat \
    | tee profile.2.dat

#
# iteration 3
echo -e "\033[1;34mIteration #3\033[0m (using coarse conv.shower profile; full set; ${STENCIL} stencil):"

# Get more accurate calibrations based on the coarse shower profile, based on
# full data set
# TODO: include "no-veto leak" term here!
./exec/ecp-resolve -p profile.2.dat -b ${BEAM_E} -Slms-svd,${STENCIL} \
        ${BASE_DIR}/${TAG}/ecal-calib-data-?x?.dat \
        -o ${TAG}.3.dat
if [ $? -ne 0 ]; then
    echo "Iterative solution stopped due to solver process exit code $?" >&2
    exit 1
fi
# Extract more accurate shower profile, based on full data set
echo -e "\033[0;34mIteration #3.2\033[0m getting conv.profile values:"
python3 py/profile.py \
        ${TAG}.3.dat \
        ${BASE_DIR}/${TAG}/ecal-calib-data-?x?.dat \
    | tee profile.3.dat

#
# iteration 4
echo -e "\033[1;34mIteration #4\033[0m (using fine conv.shower profile; full set; ${STENCIL} stencil):"

# Finalize calibrations
# TODO: include "no-veto leak" term here!
./exec/ecp-resolve -p profile.3.dat -b ${BEAM_E} -Slms-svd,${STENCIL} \
        ${BASE_DIR}/${TAG}/ecal-calib-data-?x?.dat \
        -o ${TAG}.4.dat
if [ $? -ne 0 ]; then
    echo "Iterative solution stopped due to solver process exit code $?" >&2
    exit 1
fi
# Finalize shower profile
echo -e "\033[0;34mIteration #4.2\033[0m getting conv.profile values:"
python3 py/profile.py \
        ${TAG}.4.dat \
        ${BASE_DIR}/${TAG}/ecal-calib-data-?x?.dat \
    | tee profile.4.dat
