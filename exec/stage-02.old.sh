#!/bin/bash
#
# NOTE: This is a node-running script meant to be ran on the batch node
#
# Environment variables affecting this script:
#   - HTCONDOR_JOBINDEX, required -- this job ID used to figure out the data to run on.
#   - SUPP_ENV, optional -- if given, will be source'd prior to na64sw utils execution.
#       Should be a shell script file providing additional environment definitions.
#   - BASE_DIR, required -- directory where this script is located, to infer paths to assets,
#       configs, etc.
# Inputs:
#   - dev. mode switch (enabled by `-d' option)
#   - list of input files (provided with `-s' option)
#   - runs list ASCII table (provided with `-r' option)
#   - output dir path (provided with `-o' option)
#
# Example of running locally in dev mode:
#   $  HTCONDOR_JOBINDEX=18 BASE_DIR=$(readlink -f ..) \
#           SUPP_ENV=../supp-env.local.sh ../exec/stage-01.sh \
#           -d -s ../files-list.xxx.txt \
#           -r ../presets/calib-runs-2023A-2.txt \
#           -o ./out
#
#                                                      _______________________
# ___________________________________________________/ Scenario Configuration
if [ -z "${HTCONDOR_JOBINDEX}" ] ; then
    echo "Error: env. var HTCONDOR_JOBINDEX is not set" >&2
    exit 2
fi

if [ -z "${BASE_DIR}" ] ; then
    echo "Error: BASE_DIR environment variable is not set." >&2
    exit 2
fi

if [ ! -d ${BASE_DIR} ] ; then
    echo "Error: base dir \"${BASE_DIR}\" does not exist." >&2
    exit 2
fi

if [ ! -z "${SUPP_ENV}" ] && [ ! -f ${SUPP_ENV} ] ; then
    echo "Error: supp.env file \"${SUPP_ENV}\" does not exist." >&2
    exit 2
fi

#
# Parse command line options
usage() { echo "Usage: $0 [-d] [-s <src-dat-files-list>] [-r <calibs-runs-list>] [-o <out-dir>]" 1>&2; exit 1; }

while getopts ":dr:o:" opt; do
    case "${opt}" in
        d)
            DEVMODE=1
            ;;
        r)
            RUNS_LIST=${OPTARG}
            ;;
        o)
            OUT_DIR_BASE=${OPTARG}
            ;;
        *)
            echo "Error: unkown option \"${opt}\"" >&2
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${RUNS_LIST}" ] || [ -z "{OUT_DIR_BASE}" ] ; then
    echo "Error: at least one of mandatory opts is not set" >&2
    usage
fi

#
# Assure files exist, create output dir

if [ ! -f ${RUNS_LIST} ] ; then
    echo "Error: runs list file \"${RUNS_LIST}\" does not exist." >&2
    exit 1
fi

if [ ! -d ${OUT_DIR_BASE} ] ; then
    if ! mkdir -p ${OUT_DIR_BASE} ; then
        echo "Error: failed to create \"${OUT_DIR_BASE}\"" >&2
        exit 1
    fi
fi

#
# Print initial config report

cat << EOF
Stage 1 initial configuration:
    Dev.mode ................... ${DEVMODE}
    Calib. run settings list ... ${RUNS_LIST}
    Output dir ................. ${OUT_DIR_BASE}
    job id ..................... ${HTCONDOR_JOBINDEX}
EOF

#
# Get cell parameters

read -r RUN_NO CELL_IDX ENABLE_VETO < <(grep -v '^#' $RUNS_LIST | sed -e "$(( $HTCONDOR_JOBINDEX + 1 ))q;d" | awk '{print $1 " " $2 " " $3}')
if [ -z "${RUN_NO}" ] ; then
    echo "Error: empty run number (HTCONDOR_JOBINDEX=\"${HTCONDOR_JOBINDEX}\")" >&2
    echo "  line:" $(grep -v '^#' $RUNS_LIST | sed -e "$(( $HTCONDOR_JOBINDEX + 1 ))q;d")
    exit 1
fi
IFS=x read -r CELL_X CELL_Y < <(echo "${CELL_IDX}")  # TODO: sometimes (?) doesn't work
cat << EOF
Obtained settings:
    Run no ..................... ${RUN_NO}
    Cell ID .................... ${CELL_IDX} (=${CELL_X}, ${CELL_Y})
EOF

OUT_DIR=$(realpath "${OUT_DIR_BASE}/${CELL_IDX}")
if [ ! -d ${OUT_DIR} ] ; then
    echo "Error: dir \"${OUT_DIR}\" does not exist." >&2
    exit 1
else
    echo "    output directory ........... ${OUT_DIR}"
fi

#                                                     _________________________
# __________________________________________________/ Generate Runtime Configs

if [ ! -f ${OUT_DIR_BASE}/widths.csv ] ; then
    echo "Error: no ${OUT_DIR_BASE}/widths.csv file found." >&2
    exit 3
fi
cp -v ${OUT_DIR_BASE}/widths.csv .

#
# Make pipeline config 2.1
sed "s/%(xIdx)%/${CELL_X}/g" ${BASE_DIR}/presets/run.d/run.02.1.get-raw-spectra.yaml.in \
    | sed -e "s/%(yIdx)%/${CELL_Y}/g" \
    | sed -e "s/%(cellIdx)%/${CELL_IDX}/g" \
    > run.02.1.get-raw-spectra.yaml
if [ ! -f run.02.1.get-raw-spectra.yaml ] ; then
    echo "Error: failed to produce peak retrieval config file. Aborted." >&2
    exit 4
fi

#
# Make get-fts config to fit peak for pile-up removal
sed "s/%(xIdx)%/${CELL_X}/g" ${BASE_DIR}/presets/get-primary-peak.yaml.in \
    | sed -e "s/%(yIdx)%/${CELL_Y}/g" \
    > get-primary-peak.yaml
if [ ! -f get-primary-peak.yaml ] ; then
    echo "Error: failed to produce peak retrieval config file. Aborted." >&2
    exit 4
fi


#
# Make pipeline config 2.1 (TODO: should be postponed)
#sed "s/%(xIdx)%/${CELL_X}/g" ${BASE_DIR}/presets/run.d/run.02.2.get-input-values.yaml.in \
#    | sed -e "s/%(yIdx)%/${CELL_Y}/g" \
#    | sed -e "s/%(cellIdx)%/${CELL_IDX}/g" \
#    | sed -e "s/%(mainMinAmp)%/${}" \
#    | sed -e "s/%(mainMaxAmp)%/${}" \
#    > get-input-values.yaml
#if [ ! -f get-input-values.yaml ] ; then
#    echo "Error: failed to produce peak input producer config file. Aborted." >&2
#    exit 4
#fi


#                                                                  ___________
# _______________________________________________________________/ Processing

#
# Activate supplementary environment, if need
if [ ! -z "${SUPP_ENV}" ] ; then
    source ${SUPP_ENV}
fi

#
# Stage 2, phase 2: Do the time clustering, generate AvroDST with clean data (with
#       no pile-ups) and keep processed.root to get scale transform/QA.
#       Comparing to previous phase this one is not too long, but we still tend
#       to re-use existing results in dev.mode.
# TODO: avro source file config is not installed in NA64sw. Temporarily, put
#       the schema file and config here to ${BASE_DIR}/presets/tmp/
#       while it should be at ${NA64SW_PREFIX}/share/...
CLEAN_TIME_CLUSTER_REPORT=prime-ECAL-cluster-${CELL_IDX}.root
CLEAN_TIME_AVRO_DST=selected-peaks-${CELL_IDX}.events.db
# if no clean DST or no report, do
if [ ! -f ${CLEAN_TIME_AVRO_DST} ] || [ ! -f ${CLEAN_TIME_CLUSTER_REPORT} ]  ; then
    cp -v ${OUT_DIR}/fitted-peaks.events.db . 
    echo -e "\nInfo: clustering fitted peaks based on AvroDST for ECAL..."
    na64sw-pipe -m exp-handlers -m na64avro -r ./run.02.1.get-raw-spectra.yaml    \
        -m ${BASE_DIR}/libECALCalibHandlers.so          \
        -i ${BASE_DIR}/presets/tmp/source-avro.yaml     \
        ./fitted-peaks.events.db \
        -o ${CLEAN_TIME_CLUSTER_REPORT}
    # Remove local copy of pre-reconstructed AvroDST
    rm -f ./fitted-peaks.events.db
    # Check results and die on failure
    if [ ! -f ${CLEAN_TIME_CLUSTER_REPORT} ] ; then
        echo "Error: ${CLEAN_TIME_CLUSTER_REPORT} file is not generated; abort." >&2
        exit 1
    fi
    if [ ! -f ${CLEAN_TIME_AVRO_DST} ] ; then
        echo "Error: ${CLEAN_TIME_AVRO_DST} file is not generated; abort." >&2
        exit 1
    fi
fi

PEAK_FIT_RESULTS=${CELL_X}-${CELL_Y}-peaks.dat
echo -e "\nInfo: fitting primary peaks..."
na64sw-get-fts findMax get-primary-peak.yaml ${CLEAN_TIME_CLUSTER_REPORT}
if [ ! -f ${PEAK_FIT_RESULTS} ] ; then
    echo "Error: ${PEAK_FIT_RESULTS} file is not generated; abort." >&2
    exit 1
fi

#
# Stage 2.2: Use fit mean estimate to restrict raw energy deposition
#            with mu +/- 3*sigma in the main cell ang generate input for LMS
#            procedure
# - retrieve values from fit result
AMP_RANGE_MIN=$(grep -e '.-.-1' ${PEAK_FIT_RESULTS} | awk '{print $2 - 4*$3}')
AMP_RANGE_MAX=$(grep -e '.-.-1' ${PEAK_FIT_RESULTS} | awk '{print $2 + 4*$3}')
echo -e "\nInfo: cutting max amps in range ($AMP_RANGE_MIN, $AMP_RANGE_MAX)"
# - make pipeline config substituting cell index for stage 2.2's config
sed "s/%(xIdx)%/${CELL_X}/g" ${BASE_DIR}/presets/run.d/run.02.2.get-input-values.yaml.in \
    | sed -e "s/%(yIdx)%/${CELL_Y}/g" \
    | sed -e "s/%(mainMinAmp)%/${AMP_RANGE_MIN}/g" \
    | sed -e "s/%(mainMaxAmp)%/${AMP_RANGE_MAX}/g" \
    > run.02.3.yaml
if [ ! -f run.02.3.yaml ] ; then
    echo "Error: no cfg 2.3 file. Aborted." >&2
    exit 4
fi

# Generate LMS input file
LMS_INPUT_FILE=ecal-calib-data-${CELL_X}x${CELL_Y}.dat
if [ -z ${DEVMODE} ] || [ ${DEVMODE} -eq 0 ] || [ ! -f ${LMS_INPUT_FILE} ]  ; then
    echo -e "\nInfo: generating LMS input file..."
    na64sw-pipe -m exp-handlers -m na64avro             \
        -m ${BASE_DIR}/libECALCalibHandlers.so          \
        -r ./run.02.3.yaml                              \
        -i ${BASE_DIR}/presets/tmp/source-avro.yaml     \
        ${CLEAN_TIME_AVRO_DST}                          \
        -o processed-lms-input-${CELL_X}-${CELL_Y}.root
    if [ ! -f ${LMS_INPUT_FILE} ] ; then
        echo "Error: ${LMS_INPUT_FILE} file is not generated; abort." >&2
        exit 1
    fi
else
    echo "Info: dev. mode is on and LMS input file exists, skept."
fi

#                                       ______________________________________
# ____________________________________/ Copy output data for further analysis
if [ -z ${DEVMODE} ] || [ ${DEVMODE} -eq 0 ] ; then
    mv -v ${CLEAN_TIME_CLUSTER_REPORT}  ${OUT_DIR}
    mv -v ${CLEAN_TIME_AVRO_DST}        ${OUT_DIR}
    mv -v ${PEAK_FIT_RESULTS}           ${OUT_DIR}
    mv -v ${LMS_INPUT_FILE}             ${OUT_DIR}
    # These files might be generated on peaks retrieval stage (yet we can
    # survive without this QA...)
    if [ -f "*-peaks.pdf" ] ; then 
        mv -v "*-peaks.pdf" ${OUT_DIR}
    fi
    if [ -f processed-lms-input-${CELL_X}-${CELL_Y}.root ] ; then
        mv processed-lms-input-${CELL_X}-${CELL_Y}.root ${OUT_DIR}
    fi
fi

