#include "Rtypes.h"
#include "RtypesCore.h"
#include "TCanvas.h"
#include "TFitResult.h"
#include "TMath.h"
#include "TSpline.h"
#include <TFile.h>
#include <TH2.h>
#include <TH1.h>
#include <TF1.h>
#include <TApplication.h>
#include <TMath.h>
#include <TMarker.h>

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <cassert>

#include <limits>
#include <unistd.h>

//
// Model functions

static Double_t _gaussian_model(Double_t * x, Double_t * p) {
    const Double_t mu = p[0]
                 , sigma = p[1]
                 , S = p[2]
                 ;
    Double_t xx = (x[0] - mu)/sigma;
    xx *= xx;
    return S*TMath::Exp(-xx/2);
}

static Double_t _exp_exp_bg_model(Double_t * x, Double_t * p) {
    return p[0]*TMath::Exp(-p[1]*TMath::Log(x[0]));
}

static Double_t _gaussian_exp_model(Double_t * x, Double_t * p) {
    const Double_t g  = _gaussian_model(x, p)
                 , bg = _exp_exp_bg_model(x, p + 3)
                 ;
    return g + bg;
}

template<int nT>
Double_t
cbf( double x
   , double mu, double sigma
   , double alpha
   , double S
   ) {
    # if 0
    double u   = (x-mu)/width;
    double A2  = TMath::Power(p/TMath::Abs(a),p)*TMath::Exp(-a*a/2);
    double B2  = p/TMath::Abs(a) - TMath::Abs(a);

    double result(1);
    //if      (u<-a1) result *= A1*TMath::Power(B1-u,-p1);
    if (u<a)  result *= s*TMath::Exp(-u*u/2);
    else      result *= A2*TMath::Power(B2+u,-p);
    return result;
    #else
    const Double_t alpha2 = alpha*alpha
                 , absAlpha = fabs(alpha)
                 , sq2 = sqrt(2.)
                 , u = (mu - x)/sigma
                 , C = nT*exp(-alpha2/2)/((absAlpha)*(nT-1))
                 , D = sqrt(M_PI/2)*(1 + TMath::Erf(absAlpha/sq2))
                 , N = 1/(sigma*(C+D))
                 ;
    Double_t r;
    if( u <= -alpha ) {
        // sq tail
        const Double_t A = pow((nT/absAlpha), nT)*exp(-alpha2/2)
                     , B = nT/absAlpha - absAlpha
                     ;
        r = A*TMath::Power((B - (mu - x)/sigma), - nT);
    } else {
        // Gaussian
        r = TMath::Exp( -u*u/2 );
    }
    #endif
    return S*r*N;
}


static Double_t _cb_model(Double_t * x, Double_t *par) {
    return(cbf<4>(x[0], par[0], par[1], par[2], par[3]));
}

//
// B-spline approximation wrapper (GSL)


//
// Routines

struct SliceFitResult {
    Double_t mean, spread;
    TF1 * fitFunc;
    // ... 
};

// Tries to fit the slice with different models: pure Gaussian, Gaussian plus
// exponential and right-winged Crystall Ball function. Winner will define mean
// and spread of the returned value.
SliceFitResult
fit_slice(TH1D * hst, Double_t meanPtX) {
    // get histogram mean and spread to set the initials
    Double_t hMean = hst->GetMean()
           , hSpread = hst->GetStdDev()
           , hS = hst->GetEntries()*hst->GetBinWidth(1)
           , hXMax = hst->GetXaxis()->GetXmax()
           , xMin = 3
           , xMax = 25
           ;
    if(xMin < 1e-6) xMin = 1e-6;
    if(xMax > hXMax) xMax = hXMax;

    // fit histogram with available models
    char nameBf[128];
    snprintf(nameBf, sizeof(nameBf), "gaus-%p", hst);
    TF1 * gausModel     = new TF1(nameBf, _gaussian_model,      xMin, xMax, 3);
    snprintf(nameBf, sizeof(nameBf), "gaus-exp-%p", hst);
    TF1 * gausExpModel  = new TF1(nameBf, _gaussian_exp_model,     6.75 - 1/(1 + meanPtX/300), xMax, 5);  // TODO: sigma lower limit
    snprintf(nameBf, sizeof(nameBf), "cb-%p", hst);
    TF1 * cbModel       = new TF1(nameBf, _cb_model,            xMin, xMax, 4);
    snprintf(nameBf, sizeof(nameBf), "bg-exp2-%p", hst);
    TF1 * exp2BgModel   = new TF1(nameBf, _exp_exp_bg_model,       6.75, xMax, 2);  // TODO: sigma lower limit

    // Gaussian model fits well for clean data samples with low pile-up and
    // low noise, usually at the range of middle energy
    if(gausModel) {
        gausModel->SetParameter(0, hMean);
        gausModel->SetParameter(1, hSpread);
        gausModel->SetParameter(2, hS);
    }

    // Gaus-exp model (double-exp, in fact) describes reasonably well cases
    // with high noise, yet does not provide stable convergence...
    if(gausExpModel) {

        // prefit bg with exponent
        exp2BgModel->SetParameter(0, hS*100);
        exp2BgModel->SetParameter(1, 3.6);
        auto exp2BgResult = hst->Fit(exp2BgModel, "SQRLN", "");
        //return SliceFitResult{5
        //    , 10
        //    , exp2BgModel
        //    };

        gausExpModel->SetParameter(0, hMean + 3*hSpread/4);
        gausExpModel->SetParameter(1, hSpread/2);
        gausExpModel->SetParameter(2, hst->GetEntries()*hst->GetBinWidth(1)/4);
        gausExpModel->SetParameter(3, exp2BgModel->GetParameter(0));
        gausExpModel->SetParameter(4, exp2BgModel->GetParameter(1));
        //gausExpModel->SetParameter(5, 3);

        gausExpModel->SetParLimits(0, 4.75, 60);
    }

    // CrystallBall model fits well for cases suffering from pile-ups.
    if(cbModel) {
        cbModel->SetParameter(0, hMean);
        cbModel->SetParameter(1, hSpread);
        cbModel->SetParameter(2, .9);
        cbModel->SetParameter(3, hS);
    }

    #if 1
    // fit options recap:
    // S - The result of the fit is returned in the TFitResultPtr
    // Q - Quiet mode (minimum printing)
    // L - Use log likelihood method (default is chi-square method). To be used when the histogram represents counts
    // R - Use the range specified in the function range
    // N - Do not store the graphics function, do not draw
    auto gausResult     = hst->Fit(gausModel,    "SQRLN", "")
       , gausExpResult  = hst->Fit(gausExpModel, "SQRLN", "")
       , cbResult       = hst->Fit(cbModel,      "SQRLN", "")
       ;

    #if 0
    return SliceFitResult{gausExpModel->GetParameter(0)
            , gausExpModel->GetParameter(1)
            , gausExpModel
            };
    #else
    std::map<Double_t, std::pair<TFitResultPtr, TF1*>> frs;
    if(gausResult.Get()) {
        auto fr = gausResult.Get();
        const Double_t pval = fr->Chi2();  // TMath::Prob(fr->Chi2(), fr->Ndf());
        frs.emplace(pval, std::pair<TFitResultPtr, TF1*>(gausResult, gausModel));
        //std::cout << "  gaus err.sq.: " << pval << std::endl;  // XXX
    }
    if(gausExpResult.Get()) {
        auto fr = gausExpResult.Get();
        const Double_t pval = fr->Chi2();  // TMath::Prob(fr->Chi2(), fr->Ndf());
        frs.emplace(pval, std::pair<TFitResultPtr, TF1*>(gausExpResult, gausExpModel));
        //std::cout << "  gausExp err.sq.: " << pval << std::endl;  // XXX
    }
    if(cbResult.Get()) {
        auto fr = cbResult.Get();
        const Double_t pval = fr->Chi2();  // TMath::Prob(fr->Chi2(), fr->Ndf());
        frs.emplace(pval, std::pair<TFitResultPtr, TF1*>(cbResult, cbModel));
        //std::cout << "  gaus CB err.sq.: " << pval << std::endl;  // XXX
    }

    if(frs.empty()) {
        return SliceFitResult{std::numeric_limits<Double_t>::quiet_NaN()
            , std::numeric_limits<Double_t>::quiet_NaN()
            , nullptr
            };
    }
    auto bestFitResult = frs.begin();
    //std::cout << "=> winning err.sq.: " << bestFitResult->first << std::endl;  // XXX
    return SliceFitResult{bestFitResult->second.second->GetParameter(0)
            , bestFitResult->second.second->GetParameter(1)
            , bestFitResult->second.second
            };
    #endif
    #else
    return SliceFitResult{gausExpModel->GetParameter(0)
            , gausExpModel->GetParameter(1)
            , gausExpModel
            };
    #endif
}

// Element of sub-region description. Typically, we have 2-3 sub-ranges on the
// plots where 
struct MaxAmpSubrangeForSigmaFitting {
    Float_t rangeLimits[2];
    Int_t nSlices;  // set to -1 for sentinel in an array

    // Obtained results, should be of `nSlices' size max (but not necessarily
    // -- some slices can be omitted if empty)
    std::map<Double_t, SliceFitResult> results;

    MaxAmpSubrangeForSigmaFitting() {}
};

static const Int_t gColorSequence[] = {
    kBlue,      kMagenta,       kGreen + 1, kCyan + 1,
    kBlue + 1,  kMagenta + 1,   kGreen + 2, kCyan + 2,
    kBlue + 2,  kMagenta + 2,   kGreen + 3, kCyan + 3,
    kBlue + 3,  kMagenta + 3,   kGreen + 4, kCyan + 4,
};

// Sigma distribution fitting procedure
// Parameterised with the subject histogram
// ...
void
fit_hst(TH2 * hst, MaxAmpSubrangeForSigmaFitting * subranges, TCanvas * cnv=nullptr) {
    if(cnv) {
        Int_t nPadsNeeded = 1;  // 1 is for orig 2D plot
        for(auto * sr = subranges; sr->nSlices != -1; ++sr, ++nPadsNeeded) {}
        Int_t nPadsX = std::ceil(sqrt(nPadsNeeded))
            , nPadsY = std::ceil(sqrt(nPadsNeeded))  // todo
            ;
        std::cout << "Splitting canvas onto " << nPadsX << "x" << nPadsY
            << " to show " << nPadsNeeded << " plots." << std::endl;
        assert(nPadsNeeded <= nPadsX*nPadsY);
        cnv->Divide(nPadsX, nPadsY);
        cnv->cd(1);
        hst->Draw("colz");
        cnv->GetPad(1)->SetLogz();
    }
    Int_t nSubrange = 0;
    size_t nPts = 0;
    const size_t nColors = sizeof(gColorSequence)/sizeof(*gColorSequence);
    for(auto * sr = subranges; sr->nSlices != -1; ++sr, ++nSubrange) {
        Float_t sliceWidth = ((std::isfinite(sr->rangeLimits[1]) ? sr->rangeLimits[1] : hst->GetXaxis()->GetXmax())
                        - sr->rangeLimits[0])/sr->nSlices;
        if(cnv) {
            cnv->cd(2 + nSubrange);
            cnv->GetPad(2 + nSubrange)->SetLogy();
        }
        for(int nSlice = 0; nSlice < sr->nSlices; ++nSlice) {
            const Int_t sliceColor = gColorSequence[nSlice%nColors];
            Float_t srBgn = sr->rangeLimits[0] + nSlice*sliceWidth
                  , srEnd = sr->rangeLimits[0] + (nSlice + 1)*sliceWidth
                  ;
            Int_t nBinStart = hst->GetXaxis()->FindBin(srBgn)
                , nBinEnd   = hst->GetXaxis()->FindBin(srEnd)
                ;
            char projHstNameBf[256];
            snprintf( projHstNameBf, sizeof(projHstNameBf), "%s-proj-%d-%d"
                    , hst->GetName(), nSubrange, nSlice );
            TH1D * projHst = hst->ProjectionY(projHstNameBf, nBinStart, nBinEnd, "");
            std::cout << "Using slice #" << nSubrange + 1 << "."
                << nSlice << ": from " << srBgn << " (bin #"
                << nBinStart << ") to " << srEnd << " (bin #"
                << nBinEnd << "), projection resulted in "
                << projHst->GetEntries() << " entries-"
                << (projHst->GetEntries() >= 2.5e3 ? "..." : " (ignored).")
                << std::endl;
            if(2.5e3 > projHst->GetEntries()) continue;
            //projHst->Write();
            const Double_t meanPtX = (srBgn + srEnd)/2;
            auto bestFit = fit_slice(projHst, meanPtX);
            if(std::isfinite(bestFit.mean)) {
                sr->results.emplace(meanPtX, bestFit);
                ++nPts;

                if(cnv) {
                    cnv->cd(1);

                    auto meanMarker = new TMarker(meanPtX, bestFit.mean, 3);
                    meanMarker->SetMarkerColor(sliceColor);
                    meanMarker->Draw();

                    auto upMarker = new TMarker(meanPtX, bestFit.mean + 5*bestFit.spread, 23);
                    upMarker->SetMarkerColor(sliceColor);
                    upMarker->Draw();

                    auto loMarker = new TMarker(meanPtX, bestFit.mean - 5*bestFit.spread, 22);
                    loMarker->SetMarkerColor(sliceColor);
                    loMarker->Draw();
                }
            } else {
                std::cerr << "Error: nothing converged for slice #"
                    << nSubrange << "." << nSlice << std::endl;
            }
            if(cnv) {
                cnv->cd(2 + nSubrange);
                projHst->SetLineColor(sliceColor);
                projHst->Draw(nSlice ? "SAME" : "");
                bestFit.fitFunc->SetLineColor(sliceColor);
                bestFit.fitFunc->Draw("SAME");
            } else {
                delete projHst;
            }
        }  // loop over slices
    }  // loop over subranges
    if(cnv) {  // overlay spline approximation
        Double_t * xValues  = new Double_t [nPts]
               , * meanY    = new Double_t [nPts]
               , * spreadY  = new Double_t [nPts]
               ;
        nSubrange = 0;
        size_t nPoint = 0;
        for(auto * sr = subranges; sr->nSlices != -1; ++sr, ++nSubrange) {
            for(const auto & pt : sr->results) {
                xValues[nPoint] = pt.first;
                meanY[nPoint] = pt.second.mean;
                spreadY[nPoint] = pt.second.spread;
                ++nPoint;
            }
        }

        char interpNameBf[256];
        snprintf( interpNameBf, sizeof(interpNameBf), "%s-meanSigma"
                , hst->GetName() );

        if(cnv) cnv->cd(1);
        TSpline3 * meanSpline   = new TSpline3(interpNameBf, xValues, meanY, nPoint)
               , * spreadSpline = new TSpline3(interpNameBf, xValues, spreadY, nPoint)
               ;
        meanSpline->Draw("SAME");
        //spreadSpline->Draw();


        delete [] xValues;
        delete [] meanY;
        delete [] spreadY;
    }  // if-canvas, end of approximation calculus
}

// Retrieves histograms for main and preshower part of ECAL from file.
// NOTE: the particular histogram name and the paths are the subject of
//       pipeline runtime config. Perhaps, one has to foresee here a way
//       to retrieve it from cfg or at least parameterise it from app's
//       iface (cmd line?).
std::pair<TH2 *, TH2 *>
get_hst(const char * tfileName, TFile * f, int xIdx, int yIdx) {
    char nameBf[128];
    std::pair<TH2 *, TH2 *> r(nullptr, nullptr);

    snprintf( nameBf, sizeof(nameBf), "/ECAL/x%d/y%d/msadc-moyal-maxAmp-vs-sigma-ECAL0:%d-%d-%d"
            , xIdx, yIdx
            , xIdx, yIdx, 0 );
    TObject * hstPrs_ = f->Get(nameBf);
    if(!hstPrs_) {
        std::cerr << "Error in " << tfileName << ": can't get "
            << nameBf << std::endl;
        return r;
    }
    TH2 * hstPrs = dynamic_cast<TH2*>(hstPrs_);
    if(!hstPrs) {
        std::cerr << "Error in " << tfileName << ": object \""
            << hstPrs_->GetName() << " is not a subclass of TH2"
            << std::endl;
        return r;
    }
    r.first = hstPrs;

    snprintf( nameBf, sizeof(nameBf), "/ECAL/x%d/y%d/msadc-moyal-maxAmp-vs-sigma-ECAL0:%d-%d-%d"
            , xIdx, yIdx
            , xIdx, yIdx, 1 );
    TObject * hstMain_ = f->Get(nameBf);
    if(!hstMain_) {
        std::cerr << "Error in " << tfileName << ": can't get "
            << nameBf << std::endl;
        return r;
    }
    TH2 * hstMain = dynamic_cast<TH2*>(hstMain_);
    if(!hstMain) {
        std::cerr << "Error in " << tfileName << ": object \""
            << hstMain_->GetName() << " is not a subclass of TH2"
            << std::endl;
        return r;
    }
    r.second = hstMain;

    return r;
}

int
main(int argc, char * argv[]) {
    std::string outFileName, reportFile, pointsOutFile;
    unsigned short nX = 5, nY = 6;  // TODO: configurable?
    // TODO: configurable splitting? Currently we split preshower sigma dist
    //       on two subranges and main part on two/three, based on some
    //       qualitative look...
    MaxAmpSubrangeForSigmaFitting prsSubranges[3]
                                , mainSubranges[4];
    prsSubranges[2].nSlices = -1;  // sentinel
    mainSubranges[3].nSlices = -1;  // sentinel

    // 0-like amplitudes (up to, roughly 500-700 raw amp.chan) has complex
    // behaviour and are populated very well. We use here rather frequent
    // slicing to follow it
    prsSubranges[0].rangeLimits[0] = 5;
    prsSubranges[0].rangeLimits[1] = 600;
    prsSubranges[0].nSlices = 5;
    
    mainSubranges[0].rangeLimits[0] = 15;
    mainSubranges[0].rangeLimits[1] = 70;
    mainSubranges[0].nSlices = 4;

    // mid part of the plot shows rather linear behaviour with sigma varying
    // subletly, so we slice it scarsely
    prsSubranges[1].rangeLimits[0] = 600;
    prsSubranges[1].rangeLimits[1] = std::numeric_limits<Double_t>::infinity();
    prsSubranges[1].nSlices = 3;

    mainSubranges[1].rangeLimits[0] = 70;
    mainSubranges[1].rangeLimits[1] = 600;
    mainSubranges[1].nSlices = 3;

    // for main part higher amplitudes populated not so well...
    mainSubranges[2].rangeLimits[0] = 600;
    mainSubranges[2].rangeLimits[1] = std::numeric_limits<Double_t>::infinity();
    mainSubranges[2].nSlices = 4;

    // parse cmd line args
    int opt;
    TApplication * rootApp = nullptr;
    Int_t argcDecoy = 1;
    const char * argvDecoy[] = {"get_width",};
    int txIdx = -1, tyIdx = -1;
    while((opt = getopt(argc, argv, "tO:o:c:r:")) != -1) {
        switch(opt) {
        case 'o' : {
            // most important file: will contain fitted values (per detector
            // item), if empty, cout will be used
            pointsOutFile = optarg;
        } break;
        case 'O' : {
            // ROOT file for QA, FIXME: seemed to be mandatory because of dangling clones...
            outFileName = optarg;
        } break;
        case 't' : {
            // whether to run TApplication, turns on TCanvases for dev. tuning
            rootApp = new TApplication("app", &argcDecoy, const_cast<char**>(argvDecoy));
        } break;
        case 'r' : {
            // expected to be set to .pdf file (for batch report rendering)
            reportFile = optarg;
        } break;
        #if 1  // one cell (prs + main)
        case 'c' : {
            // sets cell number
            sscanf(optarg, "%dx%d", &txIdx, &tyIdx);
            if(txIdx < 0 || txIdx >= nX || tyIdx < 0 || tyIdx > nY) {
                std::cerr << "Error: bad value for cell index \"" << optarg << "\"." << std::endl;
                exit(EXIT_FAILURE);
            }
        } break;
        #endif
        default:
            std::cerr << "Error: cmd line args parsing failure." << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    if(optind >= argc) {
        std::cerr << "Error: no input files." << std::endl;
        exit(EXIT_FAILURE);
    }
    if(txIdx < 0 || tyIdx < 0) {
        std::cerr << "Error: no cell set." << std::endl;
        exit(EXIT_FAILURE);
    }
    assert(optind > 1);
    // create out file

    TFile * outFile = nullptr;
    if(!outFileName.empty()) {
        outFile = TFile::Open(outFileName.c_str(), "RECREATE");
        if(!outFile) {
            std::cerr << "Error: can't (re-)create output file \""
                << outFileName << "\"." << std::endl;
        }
    } else {
        std::cerr << "FIXME: without -o option cloned histograms seem to"
            " not be treaten by ROOT properly (or we forgot something), segfault"
            " ahead. To fix this specify -O to .root file somewhere, even if"
            " you don't need output this file, but experience crashes..."
            << std::endl;
    }

    #if 0  // multiple histograms at once
    // allocate histograms
    TH2 **** histograms;
    histograms = new TH2 *** [nX];
    for(int i = 0; i < nX; ++i) {
        histograms[i] = new TH2 ** [nY];
        for(int j = 0; j < nY; ++j) {
            histograms[i][j] = new TH2 * [2];
            histograms[i][j][0] = nullptr;
            histograms[i][j][1] = nullptr;
        }
    }
    #else  // histograms for particular cell
    TH2 * histograms[2] = {nullptr, nullptr};
    #endif

    for(int nFile = optind; nFile < argc; ++nFile) {
        std::cout << "Info: reading file \"" << argv[nFile] << "\"..." << std::endl;
        TFile * f = TFile::Open(argv[nFile], "READ");
        if(!f) {
            std::cerr << "Error: coulnd't open file \"" << 
                argv[nFile] << "\" as ROOT file." << std::endl;
            continue;
        }
        #if 0  // multiple histograms at once
        for(int i = 0; i < nX; ++i) {
            for(int j = 0; j < nY; ++j) {
                auto r = get_hst(argv[nFile], f, i, j);
                if(!(r.first && r.second)) continue;
                if(!histograms[i][j][0]) {
                    assert(!histograms[i][j][1]);

                    char nameBf[128];
                    outFile->Cd("/");
                    snprintf( nameBf, sizeof(nameBf), "width-ECAL0:%d-%d-%d"
                            , i, j, 0 );
                    histograms[i][j][0] = static_cast<TH2*>(r.first->Clone(nameBf));
                    histograms[i][j][0]->SetDirectory(outFile);

                    snprintf( nameBf, sizeof(nameBf), "width-ECAL0:%d-%d-%d"
                            , i, j, 1 );
                    histograms[i][j][1] = static_cast<TH2*>(r.second->Clone(nameBf));
                    histograms[i][j][1]->SetDirectory(outFile);
                } else {
                    assert(histograms[i][j][1]);

                    histograms[i][j][0]->Add(r.first);
                    histograms[i][j][1]->Add(r.second);
                }
            }
        }
        #else
        auto r = get_hst(argv[nFile], f, txIdx, tyIdx);
        if(!(r.first && r.second)) {
            std::cerr << "Error: failed to retrieve histogram for cell from"
                " file \"" << argv[nFile] << "\", skept." << std::endl;
            continue;
        }
        if(!histograms[0]) {
            assert(!histograms[1]);

            char nameBf[128];

            if(!outFileName.empty()) outFile->Cd("/");
            snprintf( nameBf, sizeof(nameBf), "width-ECAL0:%d-%d-%d"
                    , txIdx, tyIdx, 0 );
            histograms[0] = static_cast<TH2*>(r.first->Clone(nameBf));
            if(!outFileName.empty()) histograms[0]->SetDirectory(outFile);

            snprintf( nameBf, sizeof(nameBf), "width-ECAL0:%d-%d-%d"
                    , txIdx, tyIdx, 1 );
            histograms[1] = static_cast<TH2*>(r.second->Clone(nameBf));
            if(!outFileName.empty()) histograms[1]->SetDirectory(outFile);
        } else {
            assert(histograms[1]);

            histograms[0]->Add(r.first);
            histograms[1]->Add(r.second);
        }
        #endif

        f->Close();
    }

    // use obtained data to fit mean sigma parameter and its spread with
    // different models
    #if 0
    // TODO: currently, the cell is hardcoded
    int xIdx = 2, yIdx = 3, zIdx = 0;  // 2,4,1
    TCanvas * cnv = new TCanvas("cnv", "Fit results");
    outFile->cd();  // XXX?
    fit_hst( histograms[xIdx][yIdx][zIdx]
           , zIdx ? mainSubranges : prsSubranges
           , cnv
           );
    #else
    for(Int_t tzIdx = 0; tzIdx < 2; ++tzIdx) {
        char cnvNameBf[64];
        snprintf(cnvNameBf, sizeof(cnvNameBf), "cnv-%d", tzIdx);
        TCanvas * cnv = reportFile.empty()
                      ? nullptr
                      : new TCanvas(cnvNameBf, "Fit results", 1200, 1200);
        if(outFile) {
            outFile->cd();  // XXX?
        }
        fit_hst( histograms[tzIdx]
               , tzIdx ? mainSubranges : prsSubranges
               , cnv
               );
        if(!reportFile.empty()) {
            cnv->Print((reportFile + (tzIdx ? ")" : "(")).c_str(), "pdf");
        }
        if(outFile) {
            cnv->Write();
        }
    }
    #endif

    // print calib. entry lines
    std::ostream * os = (pointsOutFile.empty() || pointsOutFile == "-")
                      ? &std::cout
                      : new std::ofstream(pointsOutFile);
    for(Int_t tzIdx = 0; tzIdx < 2; ++tzIdx) {
        (*os) << "ECAL0:" << txIdx << "-" << tyIdx << "-" << tzIdx;
        int nSubrange = 0;
        for(auto * sr = tzIdx ? mainSubranges : prsSubranges; sr->nSlices != -1; ++sr, ++nSubrange) {
            for(const auto & r : sr->results) {
                (*os) << "," << r.first << "," << r.second.mean << "," << r.second.spread;
            }
        }
        (*os) << std::endl;
    }
    if(os != &std::cout) {
        delete os;
    }
    
    if(rootApp)
        rootApp->Run(kTRUE);

    #if 0  // multiple histograms at once
    // finalize and cleanup
    outFile->cd();
    for(int i = 0; i < nX; ++i) {
        for(int j = 0; j < nY; ++j) {
            if(histograms[i][j][0]) {
                histograms[i][j][0]->SetDirectory(outFile);
                histograms[i][j][0]->Write();
                delete histograms[i][j][0];
            }
            if(histograms[i][j][1]) {
                histograms[i][j][1]->SetDirectory(outFile);
                histograms[i][j][1]->Write();
                delete histograms[i][j][1];
            }
            delete histograms[i][j];
        }
        delete [] histograms[i];
    }
    delete [] histograms;
    #else  // histograms for particular cell
    if(histograms[0]) {
        histograms[0]->SetDirectory(outFile);
        histograms[0]->Write();
        delete histograms[0];
    }
    if(histograms[1]) {
        histograms[1]->SetDirectory(outFile);
        histograms[1]->Write();
        delete histograms[1];
    }
    #endif

    if(!outFileName.empty())
        outFile->Close();
}

