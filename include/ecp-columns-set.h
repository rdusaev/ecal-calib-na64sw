#ifndef H_ECP_COLUMNS_SET_H
#define H_ECP_COLUMNS_SET_H 1

#include <stdio.h>
#include <stdlib.h>

/**\brief Auxiliary structure keeping semantical info on used columns
 *
 * During filling of cumulative lists this structure keeps track on the
 * columns that were actually used in order to avoid empty (full of zeroes)
 * columns which may appear when stencil did not met certain data items in
 * the input data.
 *
 * Additionaly this helper object provides semantical information on used
 * columns.
 * */
struct ecp_ColumnsSet;

/**\brief Creates new named columns set
 *
 * Allocates structure for further use. Created instance must be deleted
 * with `ecp_columns_set_destroy()`.
 * */
struct ecp_ColumnsSet * ecp_columns_set_new(size_t nMaxColumns);

/**\brief Emplaces new column with given name.
 *
 * If columns with such an index does not exist memorizes its name and index
 * and returns `0`. Otherwise returns `1` (if compiled without `NDEBUG`, an
 * assertion is checked that name matches). */
int ecp_columns_set_add_column(
          struct ecp_ColumnsSet * set
        , const char * name
        , unsigned short n
        );

/**\brief Prints set columns, in order
 *
 * Prints built columns index in multiple lines. */
void ecp_columns_set_print(FILE * stream, const struct ecp_ColumnsSet * set);

/**\brief Deletes named columns set object 
 *
 * Assumed that the instance was previously allocated with
 * `ecp_columns_set_new()` and might be filled with
 * `ecp_columns_set_add_column()`.
 * */
void ecp_columns_set_destroy(struct ecp_ColumnsSet * set);

/**\brief Returns number of columns actually used */
unsigned short ecp_columns_set_n_used_columns(const struct ecp_ColumnsSet * set);

/**\brief Returns name of n-th column
 *
 * Given index must be direct (unmapped). */
const char * ecp_columns_set_get_name(const struct ecp_ColumnsSet * set, size_t nCol);

/**\brief Creates mapping to refer to actual column index from orderly
 *        number
 *
 * Returned array provides mapping from stencil offset (offset within a
 * cumulist) to actual matrix column number. The length of allocated array
 * will be of the `nMaxColumns` parameter specified at `ecp_columns_set_new()`,
 * unused elements will be set to `USHRT_MAX`.
 *
 * \note User code must clear returned array with `free()`.
 **/
unsigned short * ecp_columns_set_build_mapping(const struct ecp_ColumnsSet * set);

/**\brief Creates reverse mapping to refer to orderly column number from actual
 *        column index
 *
 * Returned array has length of `ecp_columns_set_n_used_columns()`.
 *
 * \note User code must clear returned array with `free()`.
 * */
unsigned short * ecp_columns_set_build_reverse_mapping(const struct ecp_ColumnsSet * set);

#endif  /* H_ECP_COLUMNS_SET_H */

