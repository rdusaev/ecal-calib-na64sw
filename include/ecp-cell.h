#ifndef H_ECP_CELL_H
#define H_ECP_CELL_H

#include <stdlib.h>

struct ecp_EDepData {
    /* 3d index of the cell */
    short index[3];
    /* Rectangular sizes of the cell */
    double dims[3][2];

    /* generated energy deposition, without smearing, etc */
    double eDep;
    /* absolute fluctuation */
    double absModelErr;
    /* relative fluctuation */
    double relModelErr;

    /* calibration coefficient in use */
    double calibCoeff;

    /* measurement, after applying smearing
     *
     * calculated by `ecp_eval_cell()` as:
     *  v_m = (E_true + e_abs)*e_rel / C */
    double measuredValue;

    unsigned int isTerminal:1;
    unsigned int isActive:1;
    unsigned int isLeakItem:1;

    /* Value semantics, used to identify column when used as column */
    char * columnLabel;
};

/**\brief Returns index (offset) within a linear array by index triplet
 *
 * Used to map three indeces into linear offset to index within linear
 * arrays (e.g. of cell energy deposition data).
 *
 * \note For indeces outside of limits permitted by \p segmentation, returns
 *       SIZE_MAX. Caller routines should control returned value to avoid
 *       segfault.
 * */
size_t
ecp_idx2offset( unsigned short xIdx, unsigned short yIdx, unsigned short zIdx
              , const unsigned short * segmentation );

/**\brief Translates linear offset into index triplet
 *
 * Used to map linear offset into three indeces to index within various
 * semantical contexts.
 *
 * \returns 0 if conversion suceeded, -1 otherwise.
 * */
int
ecp_offset2idx( const size_t offset
              , unsigned short * xIdx_, unsigned short * yIdx_, unsigned short * zIdx_
              , const unsigned short * segmentation );

/**\brief (Re-)allocates cells data regarding segmentation information
 *
 * Used to (re-)initialize set of cells data respecting desired segmentation
 * information.
 *
 * \note `ecp_EDepData::calibCoeff` *will not be* set to any number and remains
 *       unitialized!
 * \note `ecp_EDepData::columnName` will be set to `NULL` and has to be set by
 *       user code
 * */
int
ecp_init_cells( struct ecp_EDepData ** data_, unsigned short * segmentation
              , const double * dims
              , unsigned int flags
              );

/**\brief Calculates measured value taking into account error terms and
 *        calibration
 *
 * Sets `ecp_EDepData::measuredValue` taking into account absolute and
 * relative errors as \f$= (eDep + absErr)*relErr/calibCoeff\f$.
 */
int ecp_eval_cell( struct ecp_EDepData * data );

#endif  /* H_ECP_CELL_H */
