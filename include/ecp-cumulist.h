#ifndef H_ECP_CUMULATIVE_LIST_H
#define H_ECP_CUMULATIVE_LIST_H

#include <stdlib.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

#include "ecp-stencil.h"
#include "ecp-columns-set.h"

#ifndef ECP_MAX_COLUMNS
/**\brief Max possible columns number
 *
 * Defines permitted number of columns in the matrix being filled. Actual
 * number of used columns will be defined by stencil.
 * */
#   define ECP_MAX_COLUMNS 255
#endif

struct ecp_EDepData;

/**\brief A list type structure accumulating numerical data 
 *
 * This intermediate list items used to temporarily store the numerical data
 * provided by cell data items for algorithms that need it.
 *
 * Sparsely resides in dynamic memory. Due to high fragmentation rate are
 * conservative in memory consumption but rather slow.
 * */
struct ecp_CumulListItem {
    /* number of elements in the item */
    size_t nElementsSet, nElementsAllocated;
    /* elements */
    double * row;
    /* free coefficient (expected overall energy deposition) */
    double   b;
    /* next item in the list */
    struct ecp_CumulListItem * next;
};

/**\brief Initialize new list item */
void ecp_cumulist_init(struct ecp_CumulListItem * item);

/**\brief Sets the j-th element within the current list item */
int
ecp_cumulist_set( struct ecp_CumulListItem * item
                , size_t j
                , double value
                );

/**\brief Turns the cumulative list into GSL matrix */
gsl_matrix *
ecp_cumulist_to_gsl_matrix(struct ecp_CumulListItem * list, gsl_vector **, const struct ecp_ColumnsSet * colSet);

/**\brief Frees the list starting from given head till the last element. */
void
ecp_cumulist_free(struct ecp_CumulListItem * head);

/**\brief Appends cumulist from event
 *
 * Helper function used to convert array of cell energy deposition data to
 * an item of cumulative list.
 * */
struct ecp_CumulListItem *
ecp_cumulist_fill( struct ecp_CumulListItem * row
                 , const struct ecp_EDepData * data
                 , struct ecp_Stencil * stencil
                 , double bValue
                 , struct ecp_ColumnsSet *
                 );

#endif  /* H_ECP_CUMULATIVE_LIST_H */
