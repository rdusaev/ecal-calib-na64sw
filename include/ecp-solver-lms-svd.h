#ifndef H_ECP_SOLVER_LMS_SVD_H
#define H_ECP_SOLVER_LMS_SVD_H

#include "ecp-cumulist.h"

struct ecp_SolverState;
struct ecp_ColumnsSet;

/**\brief Least mean square (LMS) solver based on singular value
 *        decomposition (SVD)
 *
 * Method is based on SVD inversion of some given matrix A (MxN) implemented
 * by GSL in two stages:
 *
 *  1. use `gsl_linalg_SV_decomp()` to obtain matrices U, S, V so
 *     that $A = U \cdot S \cdot V^T$, S is N-component vector
 *  2. use `gsl_linalg_SV_solve()` to obtain solution
 *
 * This is proably one of the most straightforward approach to resolve
 * overdetermined system. Note, that for well-determined system it behaves
 * pretty much as a direct matrix inversion method.
 *
 * During event accumulation it just keeps filling the cumulative list.
 * Evaluation turns the list into MxN matrix (M rows -- a number of samples,
 * N columns, a number of free parameters). Allocation of this matrix `A_{M,N}`
 * and subsequent (pseudo-)inversion operation makes the usage of the method
 * of a limited usage as this can be quite memory-consuming procedure.
 * */
struct LMSSVDSolverState {
    /** Ptr to cumulative list header */
    struct ecp_CumulListItem * head;
    /** Ptr to last item on the list (used for concatenation) */
    struct ecp_CumulListItem * tail;
    /** Ptr to filling stencil function */
    struct ecp_Stencil * stencil;
    /** Reentrant stencil data or state */
    void * stencilUserdata;
    /** Ptr to columns index */
    struct ecp_ColumnsSet * colSet;
};

int
ecp_lms_svd__add_event( struct ecp_SolverState * state_
         , const struct ecp_EDepData * data
         , double bValue
         );
int
ecp_lms_svd__eval(struct ecp_SolverState * state_, double ** x, size_t *, const char *** names);

#endif  /* H_ECP_SOLVER_LMS_SVD_H */
