#ifndef H_ECAL_GRINDHAMMER_H
#define H_ECAL_GRINDHAMMER_H

#include "ecp-types.h"

struct ecp_GrindMaterial {
    double density
         , depth
         , radLen
         ;
    unsigned short A, Z;
    double ECrit;
};

int
ecp_grindhammer_init_mat(struct ecp_GrindMaterial * mat, size_t nMedia);

struct ecp_GrinhammerPars {
    /** Homogeneous part, longit. profile alpha coeff (gamma dist) */
    double alpha;
    /** Homogeneous part, longit. profile beta coeff (gamma dist) */
    double beta;
};

double ecp_grindhammer_parameterisation(double rho, double phi, double z, void *);

#endif  /* H_ECAL_GRINDHAMMER_H */
