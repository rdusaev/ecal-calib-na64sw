#ifndef H_ECP_STENCIL_H
#define H_ECP_STENCIL_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ecp_EDepData;  /* fwd */
struct ecp_Stencil;  /* internal */

/**\brief Instantiates stencil by string description */
struct ecp_Stencil * ecp_stencil_instantiate(const char * name, const unsigned short * segmentation);

/**\brief Used for diagnostic outputs
 *
 * Prints stencil within a two squares of 8x8 symbols of size N. Cells taken
 * into account marked with asterisk (`*`) or plus (`+`), lines delimited with
 * comma+newline. Beware of `?` sign in this printout which shall indicate
 * stencil error.
 * */
void ecp_stencil_print(FILE *, struct ecp_Stencil * stencil);

/**\brief Set stencil's internal parameter "primary cell"
 *
 * A primary cell knowledge is used by stencil object to calculate relative
 * indeces. Setting primary cell outside of segmentation is possible with this
 * function (but `1` is returned). If primary cell is within the segmentation,
 * `0` returned.
 * */
int ecp_stencil_set_primary_cell(struct ecp_Stencil * stencil, unsigned short xIdx, unsigned short yIdx);

/**\brief Evaluates stencil */
int
ecp_stencil_get_offset( const struct ecp_Stencil * stencil
        , const struct ecp_EDepData * eDep
        , unsigned short * nRow, unsigned short * nCol);

/**\brief Deletes stencil object if pointer is not NULL */
void ecp_stencil_free(struct ecp_Stencil * stencil);

/**\brief Returns max number of columns given stencil object may require */
unsigned short ecp_stencil_get_max_columns(struct ecp_Stencil * stencil);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif  /* H_ECP_STENCIL_H */
