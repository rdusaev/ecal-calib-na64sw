#ifndef H_ECP_SOLVER_H
#define H_ECP_SOLVER_H

#include "ecp-cell.h"

/* Never fully defined, ptrs must be reinterpreted depending on solver state
 * payload type */
struct ecp_SolverState;

struct ecp_Solver {
    struct ecp_SolverState * state;
    int (*add_event)(struct ecp_SolverState *, const struct ecp_EDepData *);
    int (*eval)(struct ecp_SolverState *, double **, size_t *);
};

/**\brief Factory constructor for solvers
 *
 * Expected string in form:
 *
 *      <solverName:str>,<par1>,<par2>...
 *
 * Available solvers:
 *  - "LMSSVD"
 * */
struct ecp_Solver *
ecp_solver_create(const char *
        , const unsigned short * segmentation );

#endif  /* H_ECP_SOLVER_H */
