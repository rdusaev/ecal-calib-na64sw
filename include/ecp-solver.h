#ifndef H_ECP_SOLVER_H
#define H_ECP_SOLVER_H

#include "ecp-cell.h"
#include "ecp-stencil.h"

/* Never fully defined, ptrs must be reinterpreted depending on solver state
 * payload type */
struct ecp_SolverState;

struct ecp_Solver {
    struct ecp_SolverState * state;
    int (*add_event)(struct ecp_SolverState *, const struct ecp_EDepData *, double b);
    int (*eval)(struct ecp_SolverState *, double **, size_t *, const char ***);
    /* can be null, but might be used by some solvers */
    int (*set_primary_cell)(struct ecp_SolverState *, unsigned short, unsigned short);
};

/**\brief Factory constructor for solvers
 *
 * Expected string in form:
 *
 *      <solverName:str>,<par1>,<par2>...
 *
 * Available solvers:
 *  - "lms-svd,3x3"
 *  - "lms-svd,full"
 * */
struct ecp_Solver *
ecp_solver_create(const char *
        , const unsigned short * segmentation );

const struct ecp_Stencil *
ecp_solver_get_filling_stencil(struct ecp_SolverState *);

/* TODO: solver_free ? */

#endif  /* H_ECP_SOLVER_H */
