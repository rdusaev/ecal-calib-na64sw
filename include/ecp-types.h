#ifndef H_ECAL_PROFILE_TYPES_H
#define H_ECAL_PROFILE_TYPES_H

#include <stdio.h>
#include <stdbool.h>

/**\brief Shower profile function callback type, cyl coords
 *
 * Shall return $dE/d\rho d\phi d z$.
 */
struct ecp_ProfileCyl {
    void * userdata;
    double (*f)(double rho, double phi, double z, void *);
};

/**\brief Shower profile function callback type, cyl coords
 *
 * Shall return $dE/dx dy dz$, effectively repeats signature for cyl. coords
 * shower profile, but has a different name here to distinct it
 * programmatically
 */
struct ecp_ProfileCart {
    void * userdata;
    double (*f)(double x, double y, double z, void *);
};

/**\brief Returns value of cyl.integrand in cart. system
 *
 * This is an adapter integrand function, returning a value of integrand which
 * is given in cyl. coordinates in userdata from cartesian points */
double ecp_cyl_as_cart(double x, double y, double z, void * profileCyl_);

/**\brief Writes profiles to a file as a grid function available for plotting
 * 
 * Design to be compatible with gnuplot, to depict overimposed profiles.
 * Accepts ranges and desired number of steps. Optionally can integrate over
 * phi, otherwise invokes integrand function with phi=0.
 * */
int
ecp_print_as_profiles_cyl( struct ecp_ProfileCyl * p
                         , FILE * outf
                         , double rhoMin, double rhoMax, size_t nPointsRho
                         , double zMin, double zMax, size_t nPointsZ
                         , bool integratePhi
                         );

#endif  /* H_ECAL_PROFILE_TYPES_H */
