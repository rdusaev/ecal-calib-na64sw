#ifndef H_ECP_PROFILE_H
#define H_ECP_PROFILE_H

#include "ecp-cumulist.h"

#include <stdio.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ecp_ShowerProfile {
    /** Dimensions of the profile object */
    short index[3][2];
    /** Mean and variance arrays */
    double * mean, * variance;
};

/**\brief Reads profile from file
 *
 * Parses given file, expecting usual format.
 * Returns 0 on success, prints error to stderr and returns non-zero in case
 * of errors.
 * No initialization is required for profile instance being read, but for
 * compatibility with `ecp_profile_free()` set 1st index limits pair to
 * equal values (e.g. both to 0). */
//int ecp_profile_read(struct ecp_ShowerProfile * profile, const char * filename);  // xxx
int ecp_profile_read(FILE *, struct ecp_ShowerProfile *, unsigned char flags);

/**\brief Turns relative cell index into linear offset value
 *
 * This offset shall be used to retrieve mean and variance values
 * from `ecp_ShowerProfile` instance. */
unsigned int ecp_profile_cell_offset(const struct ecp_ShowerProfile *, short nX, short nY, short nZ);

/**\brief Deletes shower profile instance if 1st index limit is set to
 * non-equal values */
void ecp_profile_free(struct ecp_ShowerProfile *);

/**\brief Prints profile as two-block ASCII table
 *
 * Output is compatible with `ecp_profile_read()`
 * */
void ecp_profile_print(const struct ecp_ShowerProfile *, FILE * stream);

/**\brief Calcs missed fraction (of 1.0) of energy for cell defined by indeces
 *
 * For the cell (xIdx,yIdx) being illuminated during calibration run this
 * function returns a fraction of the missed energy according to the given
 * profile data with respect to given segmentation.
 *
 * If \p stencil is given, returned value of this function is used to estimate
 * missing energy with respect to defined stencil.
 * If \p normalize is true, shower profile (within a stencil, if stencil is
 * set) is used to normalize full energy deposition.
 * */
double ecp_profile_missed_fraction(const struct ecp_ShowerProfile *
        , unsigned short xIdx, unsigned short yIdx
        , const unsigned short * segmentation
        , const struct ecp_Stencil * stencil
        , bool normalize
        );

#ifdef __cplusplus
}  // extern "C"
#endif

#endif  /* H_ECP_PROFILE_H */

