#ifndef H_ECP_FILE_H
#define H_ECP_FILE_H

#include <stdio.h>

struct ecp_EDepData;  /* fwd */

/**\brief Writes section header to the file
 *
 * Header must begin a block of N simulated events in a file. Header contains
 * layout and calibration coefficients.
 * */
int
ecp_file_write_text_header( FILE * f
                          , const unsigned short * segmentation
                          , const double * dims
                          , const struct ecp_EDepData * data
                          );

int
ecp_file_read_text_header( FILE * f
                         , unsigned short * segmentation
                         , double * dims
                         , struct ecp_EDepData ** data_
                         );

/**\brief Writes volatile variables for the modelled data
 * */
int
ecp_file_write_text( FILE * f, const struct ecp_EDepData * data );

int
ecp_file_read_text( FILE * f, struct ecp_EDepData * data );

#endif  /* H_ECP_FILE_H */
