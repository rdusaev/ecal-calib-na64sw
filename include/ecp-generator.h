#ifndef H_ECP_GENERATOR_H
#define H_ECP_GENERATOR_H

#include "ecp-cell.h"

#include <gsl/gsl_rng.h>
#include <stdbool.h>

/**\brief A generator type
 *
 * Declares generator function with reentrant userdata to be applied to the
 * cell and set cell's value. A general convention for generator dunctions is
 * that it should return positive result on warning requiring user's attention,
 * negative on critical error and zero if went ok.
 * */
struct ecp_Generator {
    void * userdata;
    int (*generate)( struct ecp_EDepData * data
                   , void * userdata);
};

/**\brief Invokes generator for every cell with `isActive` set to 0x1
 *
 * Optionally, can re-run the generator on same cell if the generator returned
 * positive code.
 *
 * User generators are not restricted in setting arbitrary values in the cell
 * instance -- it must be explicitly written in generator's doc string.
 * */
int
ecp_generate_all( struct ecp_Generator * g
                , struct ecp_EDepData * cells
                , bool reRunOnWarning
                );

/**\brief Instantiates generator based on DSL expression 
 *
 * Expected syntax:
 *  <var-name>[/-+*]=<gen-name>[,<par1>[,<par2>...]]
 */
int
ecp_create_generator(const char * expr, struct ecp_Generator * instancePtr);


/**\brief A trivial value setter
 *
 * Implements generator interface to set/modify referenced attribute with/to a
 * constant value */
struct ecp_Generator_Const {
    double value;
    /**\breif Denote offset to a member of type `double` to be set */
    size_t memOffset;
    /**\brief Returned code is forwarded to caller's return code */
    int (*modify)(double * dest, const double randValue);
};

int
ecp_Generator_Const__generate(struct ecp_EDepData * cell, void * userdata);

/**\brief GSL-based uniform value generator
 *
 * A structure's instance should be passed as `userdata` part of `ecp_Generator`
 * instance set with `ecp_Generator_Uniform__generate()` function. This struct
 * defines lower and upper boundaries for uniform values, an offset to a
 * `double` member to be set with ranged random values (result of `offsetof()`
 * macro) and set-function which can be set to user-defined callback for
 * setting/modifying value */
struct ecp_Generator_Uniform {
    gsl_rng * gslR;
    double low, up;
    /**\breif Denote offset to a member of type `double` to be set */
    size_t memOffset;
    /**\brief Returned code is forwarded to caller's return code */
    int (*modify)(double * dest, const double randValue);
};

int
ecp_Generator_Uniform__generate(struct ecp_EDepData * cell, void * userdata);


/**\brief GSL-based Gaussian value generator
 *
 * A structure's instance should be passed as `userdata` part of `ecp_Generator`
 * instance set with `ecp_Generator_Gaussian__generate()` function. This struct
 * defines mean value and a sigma for the resulting distribution, an offset to a
 * `double` member to be set with Gaussian random values (result of `offsetof()`
 * macro) and set-function which can be set to user-defined callback for
 * setting/modifying value */
struct ecp_Generator_Gaussian {
    gsl_rng * gslR;
    double mean, sigma;
    /**\breif Denote offset to a member of type `double` to be set */
    size_t memOffset;
    /**\brief Returned code is forwarded to caller's return code */
    int (*modify)(double * dest, const double randValue);
};

int
ecp_Generator_Gaussian__generate(struct ecp_EDepData * cell, void * userdata);

#endif

