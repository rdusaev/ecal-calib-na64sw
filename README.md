# NA64 ECAL Calibration based on LMS SVD from GSL

This project provides a set of utils used to calibrate ECAL
detector of an NA64 experiment using least mean squares approach using
pseudo-inverted matrix with singualr values decomposition method.

At the same time this project serves as an example of ad-hoc usage
of [NA64sw](https://na64sw.docs.cern.ch/index.html) framework in foreign
projects.

Rest of the documentation (project description, utils/procedure reference, etc)
will appear once we got some reliable procedure.

...

## Note on zero suppression techniques

Despite these techniques are mentioned from time to time here and there in this
project, currently, we **do not** have any reliable versatile zero-suppression
technique that works for arbitrary detector (all mentions are related to
various experiments).

Although feature extraction based on discrete Fourier
transform seemed to be promising, it introduces some complications that will
require us to use some kind of ML enhancement in the future.

Linear sum estimate tends to provide use some false negatives effectively
leading to systematic under-estimation.

Currently, the most reliable way is to apply time clustering and then use
energy deposition estimates only within the primary time cluster.

## Input

Following inputs are required for the Procedure:

 * List of calibration files (ECAL scanning runs of the form `runID`, `cellX`, `cellY`)
 * Reference beam energy (e.g. 100GeV, 50GeV, etc)

## Procedure Outline

1. Base levels definition (pedestals finding, stage 1): for each cell:
   1. (Batch, raw data) extract pedestals with batch `run.01.get-pedestals.yaml`
   2. Fit pedestals with `get-fts` util and convert with `get-pedestals.py`
   3. Use `combine-zeroes.py` (forwards execution to `ecp-add-hst` exec) to
   combine zero-like information from unaffected cells into a new `.root` file,
   common for all the calibration runs (and probably valid for subsequent
   period). Then use `get-fts` executable to get mean values of DFT and sums.
2. Pre-reconstruction (stage-2): For each cell calib. run:
   1. (Batch, raw data) With collected pedestals, run batch
   `run.02.1.fit-peaks.yaml` to generate AvroDST files containing
   "pre-reconstructed" data (with fitted MSADC peaks and discovered time
   clusters).
   2. (Batch, AvroDST) Select "primary cluster", get the raw spectra
   distribution, generate LMS input files.
   3. Use `get-peaks.run.yaml` to obtain scaling coefficients
   4. (Batch, AvroDST) Run `get-lms-input.yaml` to obtain input for LMS
   procedure on the next stage.
3. Calibration. Apply iterative LMS:
   1. Combine LMS input and run procedure on middle/all cells to get
   preliminary/more accurate calib. coefficients
   2. Correct the shower profile fractions on peripherial cells, taking into
   account expected side leaks

## Snippets

Explicit procedure (tmp, will be changed to use HTcondor DAG at some point):

1. Check that `exec/submit-calib.sh` is set up to submit `stage-01.sh` and run:

       $ exec/submit-calib.sh 03-01-024-50GeV-001 presets/calib-runs-2023A-50-GeV.txt

   Wait for jobs done (will generate pedestal files
   in `/eos/home-r/rdusaev/na64/ecal-calib/03-01-024-50GeV-001/`).
2. Edit `exec/submit-calib.sh` to submit `stage-02.sh` and run:

       $ ...

Join widths:
    
    $ rm -f widths.dat ; find /eos/home-r/rdusaev/na64/ecal-calib/08-01-024-50GeV-001/?x?/ -type f -name '*-width.dat' -exec cat {} \; > widths.dat

Deprecated (zero-suppression experiments):

1. Extract DFT freq amps and sums from combined file (from NA64sw build dir):

    $ utils/get-fts/na64sw-get-fts fft-zeros \
        ../na64sw/utils/get-fts/get-fts-config.yaml \
        ~/cernbox/autosync/na64/out.root

    $ utils/get-fts/na64sw-get-fts sums \
        ../na64sw/utils/get-fts/get-fts-config.yaml \
        ~/cernbox/autosync/na64/out.root

   This will result in `dft-zeroes.json` file with DFT fit info
   and `sums-fit.csv` pre-calib information files that `...` handler can
   consume.

2. Fit peaks obtained after getting rid of pile-up and adding limits:

    $  exec/ecp-fit-peaks \
            /eos/user/r/rdusaev/na64/ecal-calib/2022-06-12-ecal-50-GeV-01-stage2/ \
            ./calib-runs-2023A-2.txt \
            ./ecalScalingFactors-023-06-12.dat

    $ ./exec/ecp-resolve -s ecalScalingFactors-023-06-12.dat -b 50 -Slms-svd,full \
            -n 500000 \
            /eos/user/r/rdusaev/na64/ecal-calib/2022-06-12-ecal-50-GeV-01-stage-2/ecal-calib-data-*.dat

    $ ./exec/ecp-resolve $(find /eos/home-r/rdusaev/na64/ecal-calib/06-01-024-50GeV-001/ -name '*-peaks.dat' -printf ' -s %p ') -b 50 -Slms-svd,full -n 500000 /eos/home-r/rdusaev/na64/ecal-calib/06-01-024-50GeV-001/?x?/ecal-calib-data-?x?.dat

## Notes on advancements

Note, that zero-like info extraction can be done also on per-cell basis.
In stage 1, instead of merging the resulting `.root` files one can extract
features for active cell in order to, say, observe cross-talk effects. For that
purpose there is a commented out block in `stage-01.sh` script.

## Results

 * (not very useful) Calib info: pedestals files for calibration runs
 * Calib info: DFT zero-like criteria
 * Calib info on fitted-peaks (peak amp to energy units)
 * Information on mean shower profile


local build snip

    $ PKG_CONFIG_PATH+=$(readlink -f ../../../lib/pkgconfig/) make

