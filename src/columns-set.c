#include "ecp-columns-set.h"

#include <limits.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

struct ecp_ColumnsSet {
    char ** names;
    size_t * bits;
    size_t nMaxColumns;
};

struct ecp_ColumnsSet *
ecp_columns_set_new(size_t nMaxColumns) {
    struct ecp_ColumnsSet * r = malloc(sizeof(struct ecp_ColumnsSet));
    r->nMaxColumns = nMaxColumns;
    size_t nColMaskEls;
    if(nMaxColumns%64) {
        nColMaskEls = ((size_t) (nMaxColumns/(sizeof(size_t)*8))) + 1;
    } else {
        nColMaskEls = ((size_t) (nMaxColumns/(sizeof(size_t)*8)));
    }

    printf("Allocating %zu (size_t) elements to hold %zu columns.\n"
            , nColMaskEls, nMaxColumns );  // XXX

    nColMaskEls *= sizeof(size_t);
    r->bits  = malloc(nColMaskEls);
    bzero(r->bits, nColMaskEls);

    r->names     = malloc(nMaxColumns*sizeof(const char *));
    bzero(r->names, nMaxColumns*sizeof(const char *));

    return r;
}

int
ecp_columns_set_add_column(
          struct ecp_ColumnsSet * set
        , const char * name
        , unsigned short n
        ) {
    assert(n < set->nMaxColumns);
    size_t nBlock = n/64
         , nBit = n%(sizeof(size_t)*8);
    size_t inBlockMask = ((size_t) 0x1) << nBit;
    if(set->bits[nBlock] & inBlockMask) {
        assert(!strcmp(set->names[n], name));
        //assert(set->positions[]
        return 1;
    }
    assert('\0' != name[0]);
    set->names[n] = strdup(name);
    set->bits[nBlock] |= inBlockMask;
    return 0;
}

void
ecp_columns_set_print(FILE * stream, const struct ecp_ColumnsSet * set) {
    for(size_t i = 0; i < set->nMaxColumns; ++i) {
        size_t nBit = ((size_t) 0x1) << i%(sizeof(size_t)*8);
        if(nBit & set->bits[i/(sizeof(size_t)*8)]) {
            fprintf(stream, "%3zu: \"%s\"\n", i, set->names[i]);
        }
    }
}

unsigned short
ecp_columns_set_n_used_columns(const struct ecp_ColumnsSet * set) {
    unsigned short count = 0;
    for(size_t i = 0; i < set->nMaxColumns; ++i) {
        size_t nBit = ((size_t) 0x1) << i%(sizeof(size_t)*8);
        if(nBit & set->bits[i/(sizeof(size_t)*8)]) {
            assert(count < USHRT_MAX);
            ++count;
        }
    }
    return count;
}

const char *
ecp_columns_set_get_name(const struct ecp_ColumnsSet * set, size_t nCol) {
    if(nCol >= set->nMaxColumns) return NULL;
    return set->names[nCol];
}

unsigned short *
ecp_columns_set_build_mapping(const struct ecp_ColumnsSet * set) {
    unsigned short * map = malloc(set->nMaxColumns*sizeof(unsigned short));
    unsigned short count = 0;
    for(size_t i = 0; i < set->nMaxColumns; ++i) {
        size_t nBit = ((size_t) 0x1) << i%(sizeof(size_t)*8);
        if(nBit & set->bits[i/(sizeof(size_t)*8)]) {
            map[i] = count;
            assert(count < USHRT_MAX);
            ++count;
        } else {
            map[i] = USHRT_MAX;
        }
    }
    return map;
}

unsigned short *
ecp_columns_set_build_reverse_mapping(const struct ecp_ColumnsSet * set) {
    const unsigned short N = ecp_columns_set_n_used_columns(set);
    unsigned short * map = malloc(N*sizeof(unsigned short));
    unsigned short count = 0;
    for(size_t i = 0; i < set->nMaxColumns; ++i) {
        size_t nBit = ((size_t) 0x1) << i%(sizeof(size_t)*8);
        if(nBit & set->bits[i/(sizeof(size_t)*8)]) {
            assert(count < USHRT_MAX);
            map[count++] = i;
        }
    }
    return map;
}

void
ecp_columns_set_destroy(struct ecp_ColumnsSet * set) {
    if(!set) return;
    if(!set->nMaxColumns) {
        free(set);
        return;
    }
    for(size_t i = 0; i < set->nMaxColumns; ++i) {
        size_t nBit = ((size_t) 0x1) << i%(sizeof(size_t)*8);
        if(nBit & set->bits[i/(sizeof(size_t)*8)]) {
            free(set->names[i]);
        }
    }
    free(set->names);
    free(set->bits);
    free(set);
}

#if 0
/**\brief Build columns mapping index
 *
 * Returns array of columns that were set.
 * */
unsigned short
ecp_columns_set_build_map(
          const struct ecp_ColumnsSet * set
        , unsigned short *) {
}
#endif

#ifdef STANDALONE_BUILD
int
main(int argc, char * argv[]) {
    const unsigned short els2add[] = {
        2, 12, 6, 7, 5, 12, 5, 6, 98, 98, 98, 1
    };
    
    char nameBuf[32];
    struct ecp_ColumnsSet * set = ecp_columns_set_new(127);
    for(int i = 0; i < sizeof(els2add)/sizeof(*els2add); ++i) {
        snprintf(nameBuf, sizeof(nameBuf), "column#%d", els2add[i]);
        if(0 == ecp_columns_set_add_column(set, nameBuf, els2add[i])) {
            printf("Added column #%d:\n", els2add[i]);
        } else {
            printf("Column #%d already added:\n", els2add[i]);
        }
        
        ecp_columns_set_print(stdout, set);
    }

    ecp_columns_set_destroy(set);
    return 0;
}
#endif  /* STANDALONE_BUILD */

