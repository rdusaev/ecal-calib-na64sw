/* A straightforward implementation of solver based on LMS method with 
 * pseudo-inversion (Penrose-Moore) matrix obtained via GSL SVD */

#include "ecp-cell.h"
#include "ecp-columns-set.h"
#include "ecp-cumulist.h"
#include "ecp-solver.h"
#include "ecp-solver-lms-svd.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_matrix_float.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>

#include <gsl/gsl_vector_double.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

int
ecp_lms_svd__add_event( struct ecp_SolverState * state_
         , const struct ecp_EDepData * data
         , double bValue
         ) {
    struct LMSSVDSolverState * state = (struct LMSSVDSolverState *) state_;
    /* allocate 1st cumulist item for new data */
    struct ecp_CumulListItem * item = malloc(sizeof(struct ecp_CumulListItem));
    ecp_cumulist_init(item);
    struct ecp_CumulListItem * last
        = ecp_cumulist_fill(item, data, state->stencil, bValue, state->colSet);
    if(!last) {
        fputs( "Error: unable to add event to a list for LMS SVD solver."
             , stderr);
        return -1;
    }

    #if 0
    printf("Adding an \"event\" to the cumulist:\n");
    for(struct ecp_CumulListItem * ci = item; ci != NULL; ci = ci->next) {
        printf("  %zu elements in a row\n", ci->nElementsSet);
    }
    #endif

    if(state->head) {
        state->tail->next = item;
        state->tail = last;
    } else {
        state->head = item;
        state->tail = last;
    }
    return 0;
}

int
ecp_lms_svd__eval( struct ecp_SolverState * state_
                 , double ** xOut
                 , size_t * nXOut
                 , const char *** names
                 ) {
    assert(xOut);
    assert(nXOut);
    int rc;
    struct LMSSVDSolverState * state = (struct LMSSVDSolverState *) state_;

    /* coefficients matrix MxN, replaced by U after eval */
    gsl_vector * b;  /* SLE results vector of size M */
    gsl_matrix * A = ecp_cumulist_to_gsl_matrix(state->head, &b, state->colSet);
    if(NULL == A) {
        fputs("Error: matrix A is NOT created.\n", stderr);
        return -1;
    }

    #if 0
    for(size_t i = 0; i < A->size1; ++i) {
        for(size_t j = 0; j < A->size2; ++j) {
            printf("%10.2e", gsl_matrix_get(A, i, j));
        }
        printf("\t= %10.2e\n", gsl_vector_get(b, i));
    }
    #else
    fputs("          |", stdout);
    for(size_t j = 0; j < A->size2; ++j) {
        printf("%10zu", j+1);
        if(j > 7 && A->size2 > 14 && j < A->size2 - 7) {
            fputs(" ... ", stdout);
            j = A->size2 - 7;
            continue;
        }
    }
    putc('\n', stdout);
    for(size_t i = 0; i < A->size1; ++i) {
        
        if(i > 7 && A->size1 > 14 && i < A->size1 - 7) {
            fputs("      ... | ... ... ...\n", stdout);
            i = A->size1 - 7;
            continue;
        } else {
            printf("%9zu |", i+1);
        }
        for(size_t j = 0; j < A->size2; ++j) {
            printf("%10.2e", gsl_matrix_get(A, i, j));
            if(j > 7 && A->size2 > 14 && j < A->size2 - 7) {
                fputs(" ... ", stdout);
                j = A->size2 - 7;
                continue;
            }
        }
        printf("\t= %10.2e\n", gsl_vector_get(b, i));
    }
    #endif

    const size_t M = A->size1
               , N = A->size2
               ;
    printf("Info: using %zux%zu matrix.\n", M, N);

    assert(0 != M);
    assert(0 != N);

    gsl_vector * x = gsl_vector_alloc(N);  /* pseudosolutions vector $\tilde{x}$ */

    gsl_vector * S    = gsl_vector_alloc(N);
    gsl_vector * work = gsl_vector_alloc(N);
    gsl_matrix * V    = gsl_matrix_alloc(N, N);

    //gsl_vector_set_zero(x);
    //gsl_vector_set_zero(S);
    //gsl_vector_set_zero(work);
    //gsl_matrix_set_zero(V);

    rc = gsl_linalg_SV_decomp(A, V, S, work);
    if(0 != rc) {
        fprintf(stderr, "Error: gsl_linalg_SV_decomp() returned %d: %s\n"
                , rc, gsl_strerror(rc));
        return -1;
    }

    rc = gsl_linalg_SV_solve(A, V, S, b, x);

    if(0 != rc) {
        fprintf(stderr, "Error: gsl_linalg_SV_solve() returned %d: %s\n"
                , rc, gsl_strerror(rc));
        return -1;
    }

    gsl_matrix_free(A);
    gsl_matrix_free(V);
    gsl_vector_free(S);
    gsl_vector_free(work);

    /*
     * Solution found, calculate testing metrics
     */
    *xOut = malloc(N*sizeof(double));
    *nXOut = N;
    assert(N == ecp_columns_set_n_used_columns(state->colSet));
    *names = malloc(sizeof(char *)*N);
    unsigned short * revIndex = ecp_columns_set_build_reverse_mapping(state->colSet);
    for(size_t i = 0; i < N; ++i) {
        (*xOut)[i]  = gsl_vector_get(x, i);
        (*names)[i] = ecp_columns_set_get_name(state->colSet, revIndex[i]);
    }
    free(revIndex);
    revIndex = NULL;

    /* Mean square error with respect to free terms:
     *  - iterate over samples, calculate the squared residuals sum */
    gsl_vector * measurements = gsl_vector_alloc(N);
    size_t entryNo = 0;
    double meanSqErr = 0;  /* todo: use scorer? */
    for(struct ecp_CumulListItem * ci = state->head; ci; ci = ci->next, ++entryNo) {
        /* set the vector of free coefficients */
        for(size_t i = 0; i < N; ++i) {
            gsl_vector_set(measurements, i, i >= ci->nElementsSet ? 0. : ci->row[i] );
        }
        /* calculate vector product of coefficients and pseudosolution to get
         * the pseudo-result, result is written in `measurements` */
        int rc = gsl_vector_mul(measurements, x);
        if(rc != 0) {
            fprintf(stderr, "Error: gsl_vector_mul() returned %d", rc);
            return -1;
        }
        #if 0
        double bEst = gsl_vector_sum(measurements)/ci->b - 1;
        #else
        double bEst = 0.;
        for(size_t i = 0; i < measurements->size; ++i) {
            bEst += gsl_vector_get(measurements, i);
        }
        bEst /= ci->b;
        bEst -= 1;
        #endif
        meanSqErr += bEst*bEst;
    }
    gsl_vector_free(measurements);

    assert(entryNo);
    double chi2 = meanSqErr
         , chi2PValue = gsl_cdf_chisq_P(chi2, entryNo);
    meanSqErr /= entryNo;

    printf( "Info: LMS SVD solver done. Results on %zu items:\n"
            "    rel. mean sq.err. ....... : %.2f%%\n"
            "    sum squared err. ........ : %.3e\n"
            "    chi2 p.val. ............. : %.4e (%e / %zu)\n"
          , entryNo
          , meanSqErr*100
          , chi2
          , 1. - chi2PValue
          , chi2, entryNo
          );

    return isfinite(meanSqErr) ? 0 : 1;
}

