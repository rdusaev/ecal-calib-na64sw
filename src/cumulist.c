#include "ecp-cumulist.h"
#include "ecp-cell.h"
#include "ecp-columns-set.h"

#include <limits.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <math.h>

void
ecp_cumulist_init(struct ecp_CumulListItem * item) {
    item->nElementsSet = 0;
    item->nElementsAllocated = 60;
    item->row = malloc(item->nElementsAllocated*sizeof(double));
    bzero(item->row, item->nElementsAllocated*sizeof(double));
    item->b = 0.;
    item->next = NULL;
}

int
ecp_cumulist_set( struct ecp_CumulListItem * item
                , size_t j
                , double value
                ) {
    assert(item);
    assert(isfinite(value));
    if(j >= item->nElementsAllocated) {
        const size_t newSize = j + 1;
        /* requested placement is bigger than what currently stored here:
         * extend, initialize, copy */
        void * newData;
        if(NULL == (newData = realloc(item->row, newSize*sizeof(double)))) {
            perror("Failed to (re)allocate memory for new element:");
            return -1;
        }
        /*printf("xxx extending list with %zu items (value is %e): realloc(%zu), bzero(%zu, %zu)\n"
                , item->nElements, value, j+1, item->nElements, j + 1 - item->nElements );*/
        item->nElementsAllocated = newSize;
        item->row = (double*) newData;
        bzero( item->row + item->nElementsSet
             , sizeof(double)*(item->nElementsAllocated - item->nElementsSet)
             );
        item->nElementsSet = j+1;
    }
    if(item->nElementsSet < j+1) item->nElementsSet = j + 1;
    assert(item->row);
    item->row[j] = value;
    #if 0
    if(j == 59) {
        printf("xxx offset=59, value=%e\n", value);
    }
    #endif
    return 0;
}

gsl_matrix *
ecp_cumulist_to_gsl_matrix( struct ecp_CumulListItem * list
        , gsl_vector ** bPtr
        , const struct ecp_ColumnsSet * colSet
        ) {
    /* Get counts for rows and columns:
     * - rows is the number of items in the list, M
     * - columns is the largest number of elements among all the items, N
     */
    size_t M = 0, N = 0;
    N = ecp_columns_set_n_used_columns(colSet);
    for(struct ecp_CumulListItem * it = list; it; it = it->next, ++M) {
        //if( N < it->nElementsSet ) N = it->nElementsSet;
    }
    if(0 == M || 0 == N) {
        fprintf(stderr, "Error: empty matrix (M=%zu, N=%zu).\n", M, N);
        return NULL;
    }
    printf("Info: allocating GSL matrix %zux%zu, (~%.2fMb required).\n", M, N
            , M*N*sizeof(double)/(1024.*1024));
    /* allocate matrix */
    gsl_matrix * m = gsl_matrix_alloc(M, N);
    gsl_matrix_set_zero(m);
    *bPtr = gsl_vector_alloc(M);
    gsl_vector_set_zero(*bPtr);
    if(!m) {
        fprintf(stderr, "Failed to allocate GSL matrix of size %lux%lu to"
                " copy values from cumulative list.\n", M, N );
        return NULL;
    }
    /* build index of orderly number from cumulist to actual column number in
     * a matrix */
    unsigned short * columnsMapping = ecp_columns_set_build_mapping(colSet);
    /* copy values */
    size_t nRow = 0;
    for(struct ecp_CumulListItem * it = list; it; it = it->next, ++nRow) {
        for(size_t nCol = 0; nCol < N; ++nCol) {
            const unsigned short nColMapped = columnsMapping[nCol];
            if(nCol >= it->nElementsSet) {
                assert(nColMapped != USHRT_MAX);
                gsl_matrix_set(m, nRow, nColMapped, 0.);
            } else {
                if(!isfinite(it->row[nCol])) {
                    fprintf(stderr, "Error: non-finite value %e met in row"
                             " %zu, column %zu (mapped %u, \"%s\").\n"
                           , it->row[nCol]
                           , nRow, nCol
                           , nColMapped, ecp_columns_set_get_name(colSet, nCol) );
                    exit(1);
                }
                gsl_matrix_set(m, nRow, nCol, it->row[nCol]);
            }
        }
        gsl_vector_set(*bPtr, nRow, it->b);
    }
    free(columnsMapping);
    return m;
}

void
ecp_cumulist_free(struct ecp_CumulListItem * head) {
    for(; head; head = head->next) {
        struct ecp_CumulListItem * next = head->next;
        free(head);
        head = next;
    }
}

struct ecp_CumulListItem *
ecp_cumulist_fill( struct ecp_CumulListItem * row
                 , const struct ecp_EDepData * data
                 , struct ecp_Stencil * stencil
                 , double bValue
                 , struct ecp_ColumnsSet * colSet
                 ) {
    /* array keeping rows requested by stencil starting from current, extended
     * by demand -- if stencil indicates row j which is greater than currently
     * allocated rows list, a new row is created. Note, that one event should
     * create at least one row and events can not share rows in this scheme
     */
    struct ecp_CumulListItem ** rows
        = (struct ecp_CumulListItem **) malloc(1*sizeof(struct ecp_CumulListItem **));
    assert(rows);
    *rows = row;
    unsigned short rowsInUse = 1;

    unsigned short i, j;
    for(size_t nItem = 0; ; ++nItem, ++data) {
        /* a stencil defines which i,j (of the resulting matrix) must be used 
         * to emplace data from current item */
        int rc = ecp_stencil_get_offset(stencil, data, &i, &j);
        if(rc < 0) {
            fprintf(stderr, "Error: matrix filling stencil"
                    " returned %d <0 on item %zu.\n", rc, nItem);
            return NULL;
        }
        if(0 == rc) {
            if(data->isTerminal) break;
            continue;  /* stencil forces this cell to be ignored */
        }

        assert(data->columnLabel);
        if(!isfinite(data->measuredValue)) {
            assert('\0' == data->columnLabel[0]);
            if(data->isTerminal) break;
            continue;
        }
        if(rc < 0) {
            fputs("Error: filling stencil error.\n", stderr);
            return NULL;
        }
        if(i > rowsInUse) {
            /* TODO: reallocate rowsInUse, allocate and init new ptrs with
             * ecp_cumulist_init(), bind `next` attributes */
            assert(false);
        }
        if( 0 != (rc = ecp_cumulist_set( *(rows + i)
                    , j
                    , isfinite(data->measuredValue) ? data->measuredValue : 0
                    ) ) ) {
            /* TODO: free allocated stuff (emergency) */
            fprintf(stderr, "Error: ecp_cumulist_set() returned %d.\n", rc);
            return NULL;
        }
        /* add the value */
        if(isnan(bValue) || !isfinite(bValue))
            (*(rows + i))->b += data->eDep;
        /* add/assure column is used */
        assert(j < USHRT_MAX);
        assert(data->columnLabel);
        ecp_columns_set_add_column(colSet, data->columnLabel, j);
        if(data->isTerminal) break;
    }
    if(!(isnan(bValue) || !isfinite(bValue)))
        (*(rows + i))->b = bValue;
    assert(isfinite((*(rows + i))->b));
    assert(0. != (*(rows + i))->b);
    return *(rows + rowsInUse - 1);
}
