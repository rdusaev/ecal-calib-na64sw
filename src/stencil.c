#include "ecp-cell.h"
#include "ecp-stencil.h"

#include <limits.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

/** A callback type implementing filling stencil
 *
 * For given edep item should return:
 *  - 1 and set `nRow`, `nCol` values to a certain numbers if cell must be
 *    considered;
 *  - 0 if cell must be ignored;
 *  - -1 if index overflow error occured;
 *  - other numbers if other kind of error occured.
 * */
typedef int StencilCallback( const struct ecp_EDepData *
                       , unsigned short * nRow, unsigned short * nCol
                       , const struct ecp_Stencil * userdata
                       );

struct ecp_Stencil {
    /** Segmentation in use */
    unsigned short segmentation[3];
    /** Current primary cell index */
    unsigned short xIdx, yIdx;

    /** When bitmasks are used -- dimensions of bitmasks (any is not gt 8) */
    unsigned short bmSize[4];  /* 2 for prs, 2 for main */
    /** Bitmasks, when used */
    unsigned long preshowerBitmask, mainBitmask;

    StencilCallback * callback;
};

/** A simplest possible stencil filling every item without any cut-off
 * or renormalization. */
static int
_full_stencil( const struct ecp_EDepData * data
             , unsigned short * nRow, unsigned short * nCol
             , const struct ecp_Stencil * userdata
             ) {
    assert(USHRT_MAX != userdata->xIdx);
    assert(USHRT_MAX != userdata->yIdx);

    *nRow = 0;
    size_t offset = ecp_idx2offset(data->index[0], data->index[1], data->index[2]
            , userdata->segmentation );
    if(offset == SIZE_MAX) return -1;  /* overflow */
    /* TODO: if(data->isLeakItem) ... */
    *nCol = (unsigned short) offset;
    return 1;  /* 1 for continue, 0 for ignoring, -1 for overflow, <0 for error */
}

/** A 3x3 stencil filling only items in +/-1 range of some primary cell */
static int
_3x3_stencil( const struct ecp_EDepData * data
            , unsigned short * nRow, unsigned short * nCol
            , const struct ecp_Stencil * userdata
            ) {
    assert(USHRT_MAX != userdata->xIdx);
    assert(USHRT_MAX != userdata->yIdx);
    /* get number of current cell wrt primary cell */
    int dx = ((int) data->index[0]) - userdata->xIdx
      , dy = ((int) data->index[1]) - userdata->yIdx
      ;
    //printf( "XXX %d-%d=%d ; %d-%d=%d\n"
    //      , data->index[0], userdata->xIdx, dx
    //      , data->index[1], userdata->yIdx, dy
    //      );
    /* just drop items that are not within 3x3 stencil wrt primary cell (0
     * means "ignore")*/
    if(dx > 1 || dx < -1) return 0;
    if(dy > 1 || dy < -1) return 0;

    *nRow = 0;
    size_t offset = ecp_idx2offset(data->index[0], data->index[1], data->index[2]
            , userdata->segmentation );
    if(offset == SIZE_MAX) return -1;  /* overflow */
    /* TODO: if(data->isLeakItem) ... */
    assert(offset < SIZE_MAX);
    *nCol = (unsigned short) offset;
    return 1;  /* 1 for continue, 0 for ignoring, <0 for error */
}

/** A 5x5 stencil filling only items in +/-2 range of some primary cell */
static int
_5x5_stencil( const struct ecp_EDepData * data
            , unsigned short * nRow, unsigned short * nCol
            , const struct ecp_Stencil * userdata
            ) {
    assert(USHRT_MAX != userdata->xIdx);
    assert(USHRT_MAX != userdata->yIdx);
    /* get number of current cell wrt primary cell */
    int dx = ((int) data->index[0]) - userdata->xIdx
      , dy = ((int) data->index[1]) - userdata->yIdx
      ;
    /* just drop items that are not within 5x5 stencil wrt primary cell (0
     * means "ignore")*/
    if(dx > 2 || dx < -2) return 0;
    if(dy > 2 || dy < -2) return 0;

    *nRow = 0;
    size_t offset = ecp_idx2offset(data->index[0], data->index[1], data->index[2]
            , userdata->segmentation );
    if(offset == SIZE_MAX) return -1;  /* overflow */
    /* TODO: if(data->isLeakItem) ... */
    assert(offset < SIZE_MAX);
    *nCol = (unsigned short) offset;
    return 1;  /* 1 for continue, 0 for ignoring, <0 for error */
}

static const struct {
    const char name[32];
    const char idxPrs[64], idxMain[64];
} _gPatternedStencils[] = {
    { "3x3-tight",
        " + ,"
        "+++,"
        " + ",

        "+++,"
        "+++,"
        "+++"
    }, { "5x5-tight-spot",

        " + ,"
        "+++,"
        " + ",

        "  +  ,"
        " +++ ,"
        "+++++,"
        " +++ ,"
        "  +  "
    }, { "5x5-wide-spot",

        "+++,"
        "+++,"
        "+++",

        " +++ ,"
        "+++++,"
        "+++++,"
        "+++++,"
        " +++ "
    }, { "5x5-tight-prs-wide-spot",

        " + ,"
        "+++,"
        " + ",

        " +++ ,"
        "+++++,"
        "+++++,"
        "+++++,"
        " +++ "
    }
};

static int
_parse_bitmask( unsigned long * bitmask
              , unsigned short * nCellsX, unsigned short * nCellsY
              , const char * strprs
              ) {
    /* count dimensions */
    unsigned short ncCellX = 0;
    *nCellsX = 0;
    *nCellsY = 0;
    for(const char * c = strprs; '\0' != *c; ++c) {
        if(',' == *c) {  /* row delimiter */
            ++(*nCellsY);
            if(ncCellX > *nCellsX) *nCellsX = ncCellX;
            ncCellX = 0;
            continue;
        }
        ++ncCellX;
    }
    ++(*nCellsY);
    if(*nCellsX*(*nCellsY) > sizeof(unsigned long)*8) return -1;  /*stencil is too large*/
    if((*nCellsX)%2 != 1) return -2;  /* number of x cells is not odd */
    if((*nCellsY)%2 != 1) return -3;  /* number of y cells is not odd */
    /* set bitmask */
    ncCellX = 0;
    *bitmask = 0x0;
    int nCommas = 0;
    for(const char * c = strprs; '\0' != *c; ++c) {
        if(',' == *c) {
            ++nCommas;
            continue;
        }
        if(' ' != *c)
            *bitmask |= (((unsigned long) 0x1) << (c - strprs - nCommas));
    }
    return 0;
}

static int
_test_index( const unsigned long mask
           , const unsigned short dimX, const unsigned short dimY
           , short relXIdx, short relYIdx
           ) {
    if(relXIdx >  dimX/2) return 0;
    if(relYIdx >  dimY/2) return 0;
    if(relXIdx < -dimX/2) return 0;
    if(relYIdx < -dimY/2) return 0;
    short xIdx = relXIdx + dimX/2
        , yIdx = relYIdx + dimY/2;
    return mask & (0x1 << (yIdx*dimX + xIdx));
}

/** A stencil filling only closest neighbouring (non-diagonal) items for
 * preshower, 3x3 + closest neghbouring +1 for main, i.e.:
 *
 * Preshower:
 *    x
 *  x 0 x
 *    x
 * Main part:
 *    x x x
 *  x x x x x
 *  x x 0 x x
 *  x x x x x
 *    x x x
 * */
static int
_bitmask_stencil( const struct ecp_EDepData * data
            , unsigned short * nRow, unsigned short * nCol
            , const struct ecp_Stencil * userdata
            ) {
    assert(USHRT_MAX != userdata->xIdx);
    assert(USHRT_MAX != userdata->yIdx);
    /* get number of current cell wrt primary cell (relative index) */
    int dx = ((int) data->index[0]) - userdata->xIdx
      , dy = ((int) data->index[1]) - userdata->yIdx
      ;
    int tst;
    if(data->index[2] == 1) {
        tst = _test_index( userdata->mainBitmask
                          , userdata->bmSize[2], userdata->bmSize[3]
                          , dx, dy
                          ) ? 1 : 0;
    } else if(data->index[2] == 0) {
        tst = _test_index( userdata->preshowerBitmask
                          , userdata->bmSize[0], userdata->bmSize[1]
                          , dx, dy
                          ) ? 1 : 0;
    } else {
        fputs("Error: z-index for energy deposition entry is set to value"
                " other than 0 or 1; star5 stencil is not applicable.", stderr);
        exit(EXIT_FAILURE);
    }
    assert(tst == 0 || tst == 1);
    if(0 == tst) return 0;

    *nRow = 0;
    size_t offset = ecp_idx2offset(data->index[0], data->index[1], data->index[2]
            , userdata->segmentation );
    if(offset == SIZE_MAX) return -1;  /* overflow */
    /* TODO: if(data->isLeakItem) ... */
    assert(offset < SIZE_MAX);
    *nCol = (unsigned short) offset;
    return 1;  /* 1 for continue, 0 for ignoring, <0 for error */
}

struct ecp_Stencil *
ecp_stencil_instantiate( const char * name
                       , const unsigned short * segmentation
                       ) {
    struct ecp_Stencil * r = malloc(sizeof(struct ecp_Stencil));
    r->segmentation[0] = segmentation[0];
    r->segmentation[1] = segmentation[1];
    r->segmentation[2] = segmentation[2];
    r->xIdx = USHRT_MAX;
    r->yIdx = USHRT_MAX;
    r->bmSize[0] = 0;
    r->bmSize[1] = 0;
    r->preshowerBitmask = 0x0;
    r->mainBitmask = 0x0;
    if(!strcmp("full", name)) {
        r->callback = _full_stencil;
        return r;
    }
    if(!strcmp("3x3", name)) {
        r->callback = _3x3_stencil;
        return r;
    }
    if(!strcmp("5x5", name)) {
        r->callback = _5x5_stencil;
        return r;
    }

    for(int i = 0; i < sizeof(_gPatternedStencils)/sizeof(*_gPatternedStencils); ++i) {
        if(strcmp(_gPatternedStencils[i].name, name)) continue;
        /* otherwise -- set bitmasks */
        _parse_bitmask(&r->preshowerBitmask
                      , r->bmSize, r->bmSize + 1
                      , _gPatternedStencils[i].idxPrs
                      );
        _parse_bitmask(&r->mainBitmask
                      , r->bmSize + 2, r->bmSize + 3
                      , _gPatternedStencils[i].idxMain
                      );
        r->callback = _bitmask_stencil;
        return r;
    }
    free(r);
    return NULL;
}

void
ecp_stencil_print( FILE * stream
                 , struct ecp_Stencil * stencil
                 ) {
    struct ecp_EDepData mockEDep;

    fputs("Preshower stencil:\n", stream);
    unsigned short mockNRow, mockNCol;
    for(int k = 0; k < 2; ++k) {
        if(0 == k) {
            fputs("Preshower part stencil:\n", stream);
        } else {
            fputs("Main part stencil:\n", stream);
        }
        mockEDep.index[2] = k;
        for(int j = 0; j < 8; ++j) {
            mockEDep.index[1] = j;
            for(int i = 0; i < 8; ++i) {
                mockEDep.index[0] = i;
                int rc = stencil->callback(&mockEDep, &mockNRow, &mockNCol, stencil);
                if(0 == rc) {
                    fputs("       ", stdout);
                } else if(1 == rc) {
                    fprintf(stream, "%3d/%3d", mockNRow, mockNCol);
                } else if(-1 == rc) {
                    fputs("  ?/  ?", stdout);
                } else {
                    fputs("Err/Err", stdout);
                }
                putc(' ', stdout);
            }
            putc(',', stdout);
            putc('\n', stdout);
        }
    }
}

int
ecp_stencil_set_primary_cell(struct ecp_Stencil * stencil, unsigned short xIdx, unsigned short yIdx) {
    stencil->xIdx = xIdx;
    stencil->yIdx = yIdx;
    if(xIdx > stencil->segmentation[0]) return 1;
    if(yIdx > stencil->segmentation[1]) return 1;
    return 0;
}

int
ecp_stencil_get_offset( const struct ecp_Stencil * stencil
        , const struct ecp_EDepData * eDep
        , unsigned short * nRow, unsigned short * nCol) {
    return stencil->callback(eDep, nRow, nCol, stencil);
}

unsigned short
ecp_stencil_get_max_columns(struct ecp_Stencil * stencil) {
    #warning "TODO: max columns used by stencil"
    // for a while this should be fine, we must foresee more general solution
    // here...
    return 255;  // TODO
}

void
ecp_stencil_free(struct ecp_Stencil * stencil) {
    if(stencil) free(stencil);
}

#ifdef STANDALONE_BUILD_1
int main(int argc, char * argv[]) {
    int rc;
    for(size_t i = 0; i < sizeof(_gPatternedStencils)/sizeof(*_gPatternedStencils); ++i) {
        unsigned long bitmask;
        unsigned short nCellsX, nCellsY;
        rc = _parse_bitmask(&bitmask, &nCellsX, &nCellsY, _gPatternedStencils[i].idxPrs);
        if(0 != rc) {
            fprintf(stderr, "failed to parse preshower stencil"
                    " \"%s\" (dims %dx%d): %d\n"
                    , _gPatternedStencils[i].name, nCellsX, nCellsY, rc);
        }
        printf("Preshower of %s (%dx%d):\n", _gPatternedStencils[i].name
                , nCellsX, nCellsY );
        for(int j = -4; j < 4; ++j) {
            for(int i = -4; i < 4; ++i) {
                if(_test_index(bitmask, nCellsX, nCellsY, i, j)) {
                    putc('*', stdout);
                } else {
                    putc(' ', stdout);
                }
                putc(' ', stdout);
            }
            putc(',', stdout);
            putc('\n', stdout);
        }

        rc = _parse_bitmask(&bitmask, &nCellsX, &nCellsY, _gPatternedStencils[i].idxMain);
        if(0 != rc) {
            fprintf(stderr, "failed to parse main stencil"
                    " \"%s\" (dims %dx%d): %d\n"
                    , _gPatternedStencils[i].name, nCellsX, nCellsY, rc);
        }
        printf("Main of %s (%dx%d):\n", _gPatternedStencils[i].name
                , nCellsX, nCellsY );
        for(int j = -4; j < 4; ++j) {
            for(int i = -4; i < 4; ++i) {
                if(_test_index(bitmask, nCellsX, nCellsY, i, j)) {
                    fputs('*', stdout);
                } else {
                    fputs(' ', stdout);
                }
                putc(' ', stdout);
            }
            putc(',', stdout);
            putc('\n', stdout);
        }
        // ...
    }
}
#endif

#ifdef STANDALONE_BUILD_2
int main(int argc, char * argv[]) {
    if(6 != argc) {
        fputs("Usage:\n    $ ", stderr);
        fputs(argv[0], stderr);
        fputs("<segmentation-x:int> <segmentation-y:int> <prime-X:int> <prime-Y:int> <stencil-name:str>\n", stderr);
        exit(EXIT_FAILURE);
    }
    int sX = atoi(argv[1])
      , sY = atoi(argv[2])
      , pX = atoi(argv[3])
      , pY = atoi(argv[4])
      ;
    unsigned short segmentation[] = {sX, sY, 2};
    struct ecp_Stencil * stencil = ecp_stencil_instantiate(argv[5], segmentation);
    if(!stencil) {
        fputs("Error: failed to Instantiate stencil.\n", stderr);
        exit(EXIT_FAILURE);
    }
    ecp_stencil_set_primary_cell(stencil, pX, pY);
    ecp_print_stencil(stdout, stencil);
    ecp_stencil_free(stencil);
    return 0;
}
#endif

