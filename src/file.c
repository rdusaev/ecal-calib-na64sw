#include "ecp-file.h"
#include "ecp-cell.h"

#include <stdio.h>
#include <stdbool.h>

int
ecp_file_write_text_header( FILE * f
                          , const unsigned short * segmentation
                          , const double * dims
                          , const struct ecp_EDepData * data
                          ) {
    fprintf( f, "header:%u %e %u %e %u %e\n"
           , segmentation[0], dims[0]
           , segmentation[1], dims[1]
           , segmentation[2], dims[2]
           );
    for(const struct ecp_EDepData * c = data; ; ++c) {
        fprintf( f, "%4u %4u %4u %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e\n"
               , c->index[0], c->index[1], c->index[2]
               , c->dims[0][0], c->dims[0][1]
               , c->dims[1][0], c->dims[1][1]
               , c->dims[2][0], c->dims[2][1]
               , c->calibCoeff
               );
        if(c->isTerminal) break;
    }
    return 0;
}

int
ecp_file_read_text_header( FILE * f
                         , unsigned short * segmentation
                         , double * dims
                         , struct ecp_EDepData ** data_
                         ) {
    int rc = fscanf(f, "header:%hu %le %hu %le %hu %le\n"
           , segmentation,      dims
           , segmentation + 1,  dims + 1
           , segmentation + 2,  dims + 2 );
    if(rc != 6) {
        fprintf(stderr, "Error: failed to read file header (%d).\n", rc);
        return -1;
    }
    if(0 == dims[0] || 0 == dims[1] || 0 == dims[1]) {
        fputs("Error: zero dimension(s) read from header.", stderr);
        return -1;
    }
    rc = ecp_init_cells(data_, segmentation, dims, 0x0);
    if(0 != rc) {
        fputs("Error initializing grid.", stderr);
        return -1;
    }
    for(struct ecp_EDepData * c = *data_; ; ++c) {
        rc = fscanf( f, "%hu %hu %hu %le %le %le %le %le %le %le\n"
               , c->index, c->index+1, c->index+2
               , c->dims[0], c->dims[0] + 1
               , c->dims[1], c->dims[1] + 1
               , c->dims[2], c->dims[2] + 1
               , &c->calibCoeff
               );
        if(rc != 10) {
            fputs("Error reading header", stderr);
            return -1;
        }
        if(c->isTerminal) break;
    }
    return 0;
}

int
ecp_file_write_text( FILE * f, const struct ecp_EDepData * data ) {
    while(true) {
        fprintf( f, "%c %c %17.7e %17.7e %17.7e %17.7e\n"
               , data->isActive ? '1' : '0'
               , data->isTerminal ? '1' : '0'
               , data->eDep
               , data->absModelErr
               , data->relModelErr
               , data->measuredValue
               );
        if(data->isTerminal) break;
        ++data;
    }
    return 0;
}

int
ecp_file_read_text( FILE * f, struct ecp_EDepData * data ) {
    unsigned int isActiveFlag, isTerminalFlag;
    while(true) {
        int rc = fscanf( f, "%u %u %le %le %le %le\n"
               , &isActiveFlag
               , &isTerminalFlag
               , &data->eDep
               , &data->absModelErr
               , &data->relModelErr
               , &data->measuredValue
               );
        if(rc != 6) {
            if(feof(f)) return 1;
            return -1;
        }
        data->isActive = isActiveFlag;
        if(0x0 != (data->isTerminal = isTerminalFlag)) break;
        ++data;
    }
    return 0;
}

