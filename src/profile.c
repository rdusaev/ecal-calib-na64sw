#include "ecp-cell.h"
#include "ecp-profile.h"
#include "ecp-stencil.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <assert.h>
#include <math.h>

#if 1
struct ecp_ProfileReadingStruct {
    int idx[3];
    double s, ss;
    size_t n;
};

int
ecp_profile_read( FILE * profileFile
            , struct ecp_ShowerProfile * profile
            , unsigned char flags  /* 0x1 -- renorm, 0x2 -- nullify negative */
            ) {
    assert(profileFile);
    /* tmp buffer to not re-read the file */
    struct ecp_ProfileReadingStruct * rr
        = malloc(1024*sizeof(struct ecp_ProfileReadingStruct));
    profile->index[0][0] = profile->index[1][0] = profile->index[2][0] = SHRT_MAX;
    profile->index[0][1] = profile->index[1][1] = profile->index[2][1] = SHRT_MIN;
    size_t nItemsRead = 0;
    double eDepSum = 0;
    assert(profile->index[2][0] > 0);

    {
        size_t nLine = 0;
        ssize_t nread;
        size_t len = 0;
        char * linebuf = NULL;
        size_t nAssigned;
        while((nread = getline(&linebuf, &len, profileFile)) != -1) {
            ++nLine;
            if(nread < 1) continue;  /* just skip blank lines */
            /* parse line into reading entry */
            nAssigned = sscanf( linebuf, "%d %d %d %le %le %zu"
                  , rr[nItemsRead].idx, rr[nItemsRead].idx + 1, rr[nItemsRead].idx + 2
                  , &rr[nItemsRead].s, &rr[nItemsRead].ss
                  , &rr[nItemsRead].n
                  );
            if(6 != nAssigned) {
                fprintf(stderr, "error reading profile file at line %zu\n"
                        , nLine );
                free(linebuf);
                goto emexit;
            }
            /* adjust profile index to handle newly read item */
            if(profile->index[0][0] > rr[nItemsRead].idx[0]) profile->index[0][0] = rr[nItemsRead].idx[0];
            if(profile->index[1][0] > rr[nItemsRead].idx[1]) profile->index[1][0] = rr[nItemsRead].idx[1];
            if(profile->index[2][0] > rr[nItemsRead].idx[2]) profile->index[2][0] = rr[nItemsRead].idx[2];

            if(profile->index[0][1] < rr[nItemsRead].idx[0]) profile->index[0][1] = rr[nItemsRead].idx[0];
            if(profile->index[1][1] < rr[nItemsRead].idx[1]) profile->index[1][1] = rr[nItemsRead].idx[1];
            if(profile->index[2][1] < rr[nItemsRead].idx[2]) profile->index[2][1] = rr[nItemsRead].idx[2];
            /* account sum for (possible) re-normalization
             * - only finite items with n > 0 are accounted
             * - if (0x2 & flags) is set, negative amps are NOT accounted */
            if( isfinite(rr[nItemsRead].s) && rr[nItemsRead].n > 0
             && (rr[nItemsRead].s > 0 || !(0x2 & flags)) ) {
                eDepSum += rr[nItemsRead].s / rr[nItemsRead].n;
            }
            ++nItemsRead;
        }
        free(linebuf);
    }
    /* reading profile done -- now allocate it and copy values from tmp reading
     * structs */
    size_t nItems = ecp_profile_cell_offset(profile, 
              profile->index[0][1]
            , profile->index[1][1]
            , profile->index[2][1]
            ) + 1;
    profile->mean     = malloc(sizeof(double)*nItems);
    profile->variance = malloc(sizeof(double)*nItems);
    for(size_t n = 0; n < nItems; ++n) {
        profile->mean[n] = profile->variance[n] = nan("0");
    }
    for(size_t n = 0; n < nItemsRead; ++n) {
        /*printf( "%3d %3d %3d  %16.8e %16.8e %9zu\n"
              , rr[n].idx[0], rr[n].idx[1], rr[n].idx[2]
              , rr[n].s, rr[n].ss, rr[n].n
              );*/
        size_t offset = ecp_profile_cell_offset(profile
                , rr[n].idx[0], rr[n].idx[1], rr[n].idx[2]);
        double mean = rr[n].s/rr[n].n
             , variance
             ;
        if(isfinite(mean) && rr[n].n > 0 ) {
            variance = rr[n].ss/rr[n].n - mean*mean;
        } else {
            variance = nan("0");
        }

        if(0x1 & flags) {  /* re-normalize */
            mean /= eDepSum;
        }
        if(mean < 0 && (0x2 & flags)) { /* nullify negative */
            mean = 0.;
        }
        profile->mean[offset] = mean;
        profile->variance[offset] = variance;
    }

    return 0;
emexit:
    free(rr);
    return -1;
}
#else  // depr format
int
ecp_profile_read(struct ecp_ShowerProfile * profile, const char * filename) {
    FILE * file = fopen(filename, "r");
    if(!file) {
        perror("Can't open profile file for reading:");
        return -1;
    }

    char * line = NULL;
    size_t len = 0, nread;
    size_t nLine = 0;
    bool isIndexLine = false;
    int nIndex = 0;
    int idxX, idxY = INT_MIN, idxZ = 0;
    while((nread = getline(&line, &len, file)) != -1) {
        ++nLine;
        /* strip comments */
        char * comment = strchr(line, '#');
        if(NULL != comment) {
            *comment = '\0';
        }
        /* skip beginning whitespaces */
        char * c = line;
        /* check if starts with "index:" -- the rest tokens should be
         * interpreted as index limits */
        if(!!(isIndexLine = !strncmp(c, "index:", strlen("index:")))) {
            c += strlen("index:");
        }
        for(; *c != '\0' && isspace(*c); ++c) {}
        /* if resulting line is empty, just skip it */
        if(*c == '\0') continue;
        /* otherwise, tokenize remaining */
        char * tok = strtok(c," \t");
        /* for non-index lines (usual semantics, after "index: ..." is parsed)
         * re-set x-index to the beginning */
        if(nIndex) idxX = profile->index[0][0];
        int nTok = 0;
        while( tok != NULL) {
            if(isIndexLine) {
                if(nIndex > 3) {
                    fprintf(stderr, "%s:%zu error: too many index limits given.\n"
                            , filename, nLine );
                    return -2;
                }
                char * cc = strchr(tok, ':');
                if(NULL == cc) {
                    fprintf(stderr, "%s:%zu error: no index delimiter in index token.\n"
                            , filename, nLine );
                    return -2;
                }
                int idxBgn = atoi(tok)
                  , idxEnd = atoi(cc+1)
                  ;
                if(idxBgn < -100 || idxEnd > 100 || idxBgn >= idxEnd) {
                    fprintf(stderr, "%s:%zu error: index #%d range \"%s\" is too"
                            " large or invalid (parsed as (%d:%d).\n"
                            , filename, nLine
                            , nIndex
                            , tok
                            , idxBgn, idxEnd);
                    return -2;
                }
                profile->index[nIndex][0] = idxBgn;
                profile->index[nIndex][1] = idxEnd;
                ++nIndex;
            } else {  /* non-index, non-empty line */
                /* Check index ranges were set */
                if(nIndex != 3) {
                    fprintf(stderr, "%s:%zu error: index range were not given"
                              " before profile description started or wrong"
                              " index header was given before.\n"
                            , filename, nLine );
                    return -3;
                }
                if(NULL != strstr(tok, "+/-")) {
                    /* this is value/error delimiter, simply omit it quietly */
                } else {
                    /* otherwise, parse token as double value */
                    assert(tok);
                    assert('\0' != *tok);
                    double v = strtod(tok, NULL);
                    int subNTok = nTok%3;
                    assert(subNTok == 0 || subNTok == 2);
                    unsigned int itemOffset = ecp_profile_cell_offset(profile, idxX, idxY, idxZ);
                    /*printf("XXX [%d]x[%d]x[%d] -> %u\n", idxX, idxY, idxZ, itemOffset);  // XXX*/
                    assert(itemOffset != UINT_MAX);
                    if(subNTok == 0) {
                        /* this is (mean) value */
                        profile->mean[itemOffset] = v;
                    } else {
                        /* this is variance */
                        profile->variance[itemOffset] = v;
                        /* Assuming std.dev token finalized cell description,
                         * increment X index */
                        ++idxX;
                    }
                }
            }
            tok = strtok(NULL, " \t");
            ++nTok;
        }  /* tokenization loop */
        if(!isIndexLine) {
            /* assuming not index non-empty line, increment Y index */
            if(idxY != profile->index[1][1]) {
                ++idxY;
            } else {
                idxY = profile->index[1][0];
                ++idxZ;
            }
        } else {
            /* index line was just read -- allocate the profile data array */
            size_t nValues = (profile->index[0][1] - profile->index[0][0] + 1)
                           * (profile->index[1][1] - profile->index[1][0] + 1)
                           * (profile->index[2][1] - profile->index[2][0] + 1)
                           ;
            printf("Info: allocating %zu elements to store"
                   " (%d:%d)x(%d:%d)x(%d:%d) profile values,\n", nValues
                   , profile->index[0][0], profile->index[0][1]
                   , profile->index[1][0], profile->index[1][1]
                   , profile->index[2][0], profile->index[2][1]
                   );
            profile->mean       = malloc(nValues*sizeof(double));
            profile->variance   = malloc(nValues*sizeof(double));
            /* set Y and Z indeces */
            idxY = profile->index[1][0];
            idxZ = profile->index[2][0];
        }
        /*printf("xxx line %zu parsed\n", nLine);  // XXX*/
    }  /* line-reading loop */
    fclose(file);
    if(0 == nLine) {
        fprintf(stderr, "Error: empty file %s\n", filename);
        return -1;
    }
    printf("Info: file %s read.\n", filename);
    return 0;
}
#endif

unsigned int
ecp_profile_cell_offset( const struct ecp_ShowerProfile * profile
        , short nX, short nY, short nZ ) {
    if(nX < profile->index[0][0] || nX > profile->index[0][1]) return UINT_MAX;
    if(nY < profile->index[1][0] || nY > profile->index[1][1]) return UINT_MAX;
    if(nZ < profile->index[2][0] || nZ > profile->index[2][1]) return UINT_MAX;
    const size_t NX = (profile->index[0][1] - profile->index[0][0]) + 1
               , NY = (profile->index[1][1] - profile->index[1][0]) + 1
               ;
    return (nZ - profile->index[2][0])*NX*NY
         + (nY - profile->index[1][0])*NX
         + (nX - profile->index[0][0])
         ;
}

void
ecp_profile_free(struct ecp_ShowerProfile * profile) {
    if(profile->index[0] == profile->index[1]) return;

    free(profile->mean);
    profile->mean = NULL;
    free(profile->variance);
    profile->variance = NULL;
}

void
ecp_profile_print(const struct ecp_ShowerProfile * profile, FILE * stream) {
    fprintf(stream, "index: %d:%d %d:%d %d:%d\n"
                   , profile->index[0][0], profile->index[0][1]
                   , profile->index[1][0], profile->index[1][1]
                   , profile->index[2][0], profile->index[2][1]
                   );
    float qaSum = 0.;
    for(short zIdx = profile->index[2][0]; zIdx <= profile->index[2][1]; ++zIdx) {
        if(0 == zIdx)
            fputs("# Preshower:\n", stream);
        else
            fputs("# Main part:\n", stream);
        for(short yIdx = profile->index[1][0]; yIdx <= profile->index[1][1]; ++yIdx) {
            for(short xIdx = profile->index[0][0]; xIdx <= profile->index[0][1]; ++xIdx) {
                unsigned short itemOffset = ecp_profile_cell_offset(profile
                        , xIdx, yIdx, zIdx);
                qaSum += profile->mean[itemOffset];
                fprintf(stream, "%13.4e +/- %10.4e"
                        , profile->mean[itemOffset], profile->variance[itemOffset]);
            }
            fputc('\n', stream);
        }
    }
    fprintf(stream, "# sum of means=%f\n", qaSum);
}

double
ecp_profile_missed_fraction(const struct ecp_ShowerProfile * profile
        , unsigned short primeXIdx, unsigned short primeYIdx
        , const unsigned short * segmentation
        , const struct ecp_Stencil * stencil
        , bool normalize
        ) {
    assert(primeXIdx < segmentation[0]);
    assert(primeYIdx < segmentation[1]);
    double S = 0.;  /* sum of profile fractions (used to normalize) */
    /* iterate within the profile, collect the cells that drop off the
     * segmentation limits */
    double missedE = 0;
    for(short zIdx = profile->index[2][0]; zIdx <= profile->index[2][1]; ++zIdx) {
        for(short yIdx = profile->index[1][0]; yIdx <= profile->index[1][1]; ++yIdx) {
            for(short xIdx = profile->index[0][0]; xIdx <= profile->index[0][1]; ++xIdx) {
                /* NOTE: [xyz]Idx are indexes within the conv.profile, here
                 * they should be interpreted relatively to primary cell */

                /* get absolute x,y index */
                int cXIdx = primeXIdx + xIdx
                  , cYIdx = primeYIdx + yIdx
                  ;
                /* if stencil is given, check whether the cell belongs to the
                 * cells permitted by the stencil */
                if(stencil) {
                    struct ecp_EDepData mockEDep;
                    mockEDep.index[0] = cXIdx;
                    mockEDep.index[1] = cYIdx;
                    mockEDep.index[2] = zIdx;
                    mockEDep.isActive = 0x1;
                    unsigned short nRow, nCol;
                    int rc = ecp_stencil_get_offset(stencil, &mockEDep, &nRow, &nCol);
                    /* ^^^ 0 means ignore by stencil, 1 is for taking into
                     *     account, other code(s) are for stencil error.
                     *     -1 is for overflow, but here we should interpret
                     *     it as missing energy */
                    if(rc == 0) {
                        //assert(xIdx == 0 && yIdx == 0);
                        //printf(" ...rel index %dx%dx%d (abs %dx%d) ignored by stencil\n", xIdx, yIdx, zIdx
                        //    , cXIdx, cYIdx );
                        //printf("xxx %2d %2d %2d: dropped by stencil\n", xIdx, yIdx, zIdx);
                        assert(!(xIdx == 0 && yIdx == 0));  /* what kind of stencil whould ignore 0x0 ?! */
                        continue;  /* cell ignored due to the stencil */
                    }
                }
                /* otherwise increase full sum norm */
                size_t showerItemOffset = ecp_profile_cell_offset(profile, xIdx, yIdx, zIdx);
                const double eDepEst = profile->mean[showerItemOffset] > 0 ? profile->mean[showerItemOffset] : 0;
                S += eDepEst;
                /* skip shower item if it is within the segmentation */
                if( cXIdx >= 0 && cXIdx < segmentation[0]
                 && cYIdx >= 0 && cYIdx < segmentation[1]
                 ) {
                    /*printf(" ...rel index %dx%dx%d within: %.3e\n", xIdx, yIdx, zIdx
                        , profile->mean[showerItemOffset] );*/
                    //printf("xxx %2d %2d %d: %e contributes to sum\n", xIdx, yIdx, zIdx, eDepEst);
                    continue;
                }
                //printf("xxx %2d %2d %2d: %e\n", xIdx, yIdx, zIdx, eDepEst);
                //size_t showerItemOffset = ecp_profile_cell_offset(profile, xIdx, yIdx, zIdx);
                /* add the energy fraction to the missed energy estimate */
                //printf(" ...rel index %dx%dx%d (abs %dx%d) missed: %.3e\n", xIdx, yIdx, zIdx
                //        , cXIdx, cYIdx
                //        , profile->mean[showerItemOffset] );
                //printf("xxx %2d %2d %d: %e is missed\n", xIdx, yIdx, zIdx, eDepEst);
                missedE += eDepEst;
            }
        }
    }
    if(fabs(S - missedE) < 1e-8) {
        fprintf(stderr, "Error: prime cell %ux%u with selected stencil yields"
                " no energy deposition (segmentation %ux%u)\n"
                , primeXIdx, primeYIdx
                , segmentation[0], segmentation[1]
                );
    }
    //puts("stub exit"); exit(EXIT_FAILURE);  /*XXX*/
    //printf("=> overall missed for %dx%d: %.3e (S=%.3e)\n", primeXIdx, primeYIdx, missedE, S);
    if(!normalize)  /* return unnormalized missed energy estimate */
        return missedE;
    /* return normalized missed energy estimate */
    return missedE/S;
}

