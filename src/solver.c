#include "ecp-columns-set.h"
#include "ecp-solver.h"
#include "ecp-cell.h"
#include "ecp-cumulist.h"
#include "ecp-solver-lms-svd.h"
#include "ecp-stencil.h"

#include <stdbool.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>

int
ecp_set_primary_cell( struct ecp_SolverState * state_
                    , unsigned short xIdx
                    , unsigned short yIdx
                    ) {
    /* TODO: can be wrong once we'll introduce another solver types... */
    struct LMSSVDSolverState * state = (struct LMSSVDSolverState *) state_; 
    return ecp_stencil_set_primary_cell(state->stencil, xIdx, yIdx);
}

const struct ecp_Stencil *
ecp_solver_get_filling_stencil(struct ecp_SolverState * state_) {
    /* TODO: can be wrong once we'll introduce another solver types... */
    struct LMSSVDSolverState * state = (struct LMSSVDSolverState *) state_; 
    return state->stencil;
}

struct ecp_Solver *
ecp_solver_create( const char * expr_
                 , const unsigned short * segmentation
                 ) {
    char * expr = alloca(strlen(expr_));
    strcpy(expr, expr_);
    char * toks[64] = {NULL}, ** cTok = toks;
    char ** tokEnd = toks + sizeof(toks);
    char * saveptr;

    while(!!(*cTok = strtok_r(*toks ? NULL : expr, ",", &saveptr))) {
        ++cTok;
        if(cTok >= tokEnd) {
            fputs("Error: too many tokens in solver expression.\n", stderr);
            return NULL;
        }
    }

    if(cTok == toks) {
        fputs("Error: empty solver parameters list.\n", stderr);
        return NULL;
    }

    struct ecp_Solver * solver = malloc(sizeof(struct ecp_Solver));
    if(0 == strcmp("lms-svd", toks[0])) {  /* ....... GSL-based LMS SVD solver */
        /* Expected parameters:
         *  1. stencil name
         *  ...
         * */
        puts("Info: GSL-based LMS SVD solver in use.");
        struct LMSSVDSolverState * state = malloc(sizeof(struct LMSSVDSolverState));
        state->tail = state->head = NULL;
        if(NULL == toks[1]) {
            free(state);
            free(solver);
            fputs("Error: stencil parameter is required for lms-svd solver.", stderr);
            return NULL;
        }
        /* init stencil */
        state->stencil = NULL;
        state->stencil = ecp_stencil_instantiate(toks[1], segmentation);
        if(!state->stencil) {
            fprintf(stderr, "Error: could not instantiate stencil \"%s\".\n"
                    , toks[1]);
            exit(EXIT_FAILURE);
        }
        /* init columns index object */
        state->colSet = ecp_columns_set_new(ecp_stencil_get_max_columns(state->stencil));

        solver->add_event = ecp_lms_svd__add_event;
        solver->eval = ecp_lms_svd__eval;
        solver->set_primary_cell = ecp_set_primary_cell;
        solver->state = (struct ecp_SolverState *) state;
    } else {
        fprintf(stderr, "Error: unknown solver type \"%s\".\n", toks[0]);
        return NULL;
    }
    return solver;
}

