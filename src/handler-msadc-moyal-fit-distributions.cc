#include "na64detID/detectorID.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"
#include "na64util/TDirAdapter.hh"
#include "na64util/str-fmt.hh"

#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>

#include <unordered_map>

namespace na64dp {
namespace handlers {

/**\brief Plots per-detector distribution of Moyal model parameters
 *
 * \code{.yaml}
 *     - _type: MSADCPlotMoyalDistributions
 *       # ...; str, opt
 *       foo: bar
 *       # ...; int, required
 *       bar: 123
 * \endcode
 *
 * */
class MSADCPlotMoyalDistributions 
        : public util::TDirAdapter
        , public AbstractHitHandler<event::SADCHit> {
protected:
    const float _ranges[4][2];
    const size_t _nBins[4];
    const std::string _hstBaseNameDist, _hstBaseNameCorr
                    , _hstDescriptionDist, _hstDescriptionCorr
                    , _overridenPath
                    ;

    struct Histograms {
        TH1D * muDist, * sigmaDist, * SDist, * maxAmpDist;
        TH2D * muVsSigmaCorr
           , * muVsSCorr
           , * muVsMaxCorr
           , * sigmaVsSCorr
           , * maxVsSigmaCorr
           , * SVsMaxCorr
           ;

        void fill(double mu, double sigma, double S, double maxAmp) {
            if(muDist)      muDist->Fill(mu);
            if(sigmaDist)   sigmaDist->Fill(sigma);
            if(SDist)       SDist->Fill(S);
            if(maxAmpDist)  maxAmpDist->Fill(maxAmp);

            if(muVsSigmaCorr)   muVsSigmaCorr->Fill(mu, sigma);
            if(muVsSCorr)       muVsSCorr->Fill(mu, S);
            if(muVsMaxCorr)     muVsMaxCorr->Fill(mu, maxAmp);

            if(sigmaVsSCorr)    sigmaVsSCorr->Fill(S, sigma);
            if(maxVsSigmaCorr)  maxVsSigmaCorr->Fill(maxAmp, sigma);
            if(SVsMaxCorr)      SVsMaxCorr->Fill(S, maxAmp);
        }
    };

    //std::unordered_map<DetID, std::tuple<TH1D*, TH1D *, TH1D*>> _hists;
    std::unordered_map<DetID, Histograms> _hists;
public:
    /// ... features of the ctr, if any
    MSADCPlotMoyalDistributions( calib::Dispatcher & cdsp
                , const std::string & selection
                , const std::string & baseNameDistPat, const std::string & baseNameCorrPat
                , const std::string & baseDescriptionDistPat, const std::string & baseDescriptionCorrPat
                , float muMin, float muMax, size_t nBinsMu
                , float sigmaMin, float sigmaMax, size_t nBinsSigma
                , float SMin, float SMax, size_t nBinsS
                , float maxAmpMin, float maxAmpMax, size_t nBinsMaxAmp
                , log4cpp::Category & logCat
                , const std::string & overridenPath=""
                , const std::string & namingSubclass="default"
                )
        : util::TDirAdapter(cdsp)
        , AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _ranges{{muMin, muMax}, {sigmaMin, sigmaMax}, {SMin, SMax}, {maxAmpMin, maxAmpMax}}
        , _nBins{nBinsMu, nBinsSigma, nBinsS, nBinsMaxAmp}
        , _hstBaseNameDist(baseNameDistPat), _hstBaseNameCorr(baseNameCorrPat)
        , _hstDescriptionDist(baseDescriptionDistPat), _hstDescriptionCorr(baseDescriptionCorrPat)
        , _overridenPath(overridenPath)
        {}
    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
    void finalize() override;
};  // class MSADCPlotMoyalDistributions


bool
MSADCPlotMoyalDistributions::process_hit(EventID, DetID did, event::SADCHit & hit) {
    if( !hit.rawData ) return true;  // no raw data associated with hit
    if( hit.rawData->maxima.empty() ) return true;
    auto it = _hists.find( did );
    if( _hists.end() == it ) {
        struct Histograms hsts;
        // no histograms exist for this detector entity -- create and insert
        auto substCtx = util::TDirAdapter::subst_dict_for(did, _hstBaseNameDist);

        std::string hstName, description;

        if(_nBins[0]) {
            substCtx["variable"] = "mu";
            hstName = util::str_subst( _hstBaseNameDist, substCtx );
            description = util::str_subst( _hstDescriptionDist, substCtx );
            auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            hsts.muDist = new TH1D(hstName.c_str(), description.c_str()
                    , _nBins[0], _ranges[0][0], _ranges[0][1]);
            hsts.muDist->SetOption("colz");
        }

        if(_nBins[1]) {
            substCtx["variable"] = "sigma";
            hstName = util::str_subst( _hstBaseNameDist, substCtx );
            description = util::str_subst( _hstDescriptionDist, substCtx );
            auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            hsts.sigmaDist = new TH1D(hstName.c_str(), description.c_str()
                    , _nBins[1], _ranges[1][0], _ranges[1][1]);
            hsts.sigmaDist->SetOption("colz");
        }

        if(_nBins[2]) {
            substCtx["variable"] = "S";
            hstName = util::str_subst( _hstBaseNameDist, substCtx );
            description = util::str_subst( _hstDescriptionDist, substCtx );
            auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            hsts.SDist = new TH1D(hstName.c_str(), description.c_str()
                    , _nBins[2], _ranges[2][0], _ranges[2][1]);
            hsts.SDist->SetOption("colz");
        }

        if(_nBins[3]) {
            substCtx["variable"] = "maxAmp";
            hstName = util::str_subst( _hstBaseNameDist, substCtx );
            description = util::str_subst( _hstDescriptionDist, substCtx );
            auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            hsts.maxAmpDist = new TH1D(hstName.c_str(), description.c_str()
                    , _nBins[3], _ranges[3][0], _ranges[3][1]);
            hsts.maxAmpDist->SetOption("colz");
        }

        substCtx = util::TDirAdapter::subst_dict_for(did, _hstBaseNameCorr);

        if(_nBins[0] && _nBins[1]) {
            substCtx["variableX"] = "mu";
            substCtx["variableY"] = "sigma";
            hstName = util::str_subst( _hstBaseNameCorr, substCtx );
            description = util::str_subst( _hstDescriptionCorr, substCtx );
            auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            hsts.muVsSigmaCorr = new TH2D(hstName.c_str(), description.c_str()
                    , _nBins[0], _ranges[0][0], _ranges[0][1]
                    , _nBins[1], _ranges[1][0], _ranges[1][1]
                    );
            hsts.muVsSigmaCorr->SetOption("colz");
        }

        if(_nBins[0] && _nBins[2]) {
            substCtx["variableX"] = "mu";
            substCtx["variableY"] = "S";
            hstName = util::str_subst( _hstBaseNameCorr, substCtx );
            description = util::str_subst( _hstDescriptionCorr, substCtx );
            auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            hsts.muVsSCorr = new TH2D(hstName.c_str(), description.c_str()
                    , _nBins[0], _ranges[0][0], _ranges[0][1]
                    , _nBins[2], _ranges[2][0], _ranges[2][1]
                    );
            hsts.muVsSCorr->SetOption("colz");
        }

        if(_nBins[0] && _nBins[3]) {
            substCtx["variableX"] = "mu";
            substCtx["variableY"] = "maxAmp";
            hstName = util::str_subst( _hstBaseNameCorr, substCtx );
            description = util::str_subst( _hstDescriptionCorr, substCtx );
            auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            hsts.muVsMaxCorr = new TH2D(hstName.c_str(), description.c_str()
                    , _nBins[0], _ranges[0][0], _ranges[0][1]
                    , _nBins[3], _ranges[3][0], _ranges[3][1]
                    );
            hsts.muVsMaxCorr->SetOption("colz");
        }

        if(_nBins[1] && _nBins[2]) {
            substCtx["variableX"] = "sigma";
            substCtx["variableY"] = "S";
            hstName = util::str_subst( _hstBaseNameCorr, substCtx );
            description = util::str_subst( _hstDescriptionCorr, substCtx );
            auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            hsts.sigmaVsSCorr = new TH2D(hstName.c_str(), description.c_str()
                    , _nBins[2], _ranges[2][0], _ranges[2][1]
                    , _nBins[1], _ranges[1][0], _ranges[1][1]
                    );
            hsts.sigmaVsSCorr->SetOption("colz");
        }

        if(_nBins[1] && _nBins[3]) {
            substCtx["variableX"] = "maxAmp";
            substCtx["variableY"] = "sigma";
            hstName = util::str_subst( _hstBaseNameCorr, substCtx );
            description = util::str_subst( _hstDescriptionCorr, substCtx );
            auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            hsts.maxVsSigmaCorr = new TH2D(hstName.c_str(), description.c_str()
                    , _nBins[3], _ranges[3][0], _ranges[3][1]
                    , _nBins[1], _ranges[1][0], _ranges[1][1]
                    );
            hsts.maxVsSigmaCorr->SetOption("colz");
        }

        if(_nBins[2] && _nBins[3]) {
            substCtx["variableX"] = "S";
            substCtx["variableY"] = "maxAmp";
            hstName = util::str_subst( _hstBaseNameCorr, substCtx );
            description = util::str_subst( _hstDescriptionCorr, substCtx );
            auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            hsts.SVsMaxCorr = new TH2D(hstName.c_str(), description.c_str()
                    , _nBins[2], _ranges[2][0], _ranges[2][1]
                    , _nBins[3], _ranges[3][0], _ranges[3][1]
                    );
            hsts.SVsMaxCorr->SetOption("colz");
        }

        auto ir = _hists.emplace(did, hsts);
        it = ir.first;
        assert(ir.second);  // something wrong with detector ID
    }

    for(const auto & peak : hit.rawData->maxima) {
        if(!peak.second->moyalFit) continue;
        it->second.fill( peak.second->moyalFit->mu
                , peak.second->moyalFit->sigma
                , peak.second->moyalFit->S
                , peak.second->amp );
    }
    
    return true;
}

void
MSADCPlotMoyalDistributions::finalize() {
    if( _hists.empty() ) {
        this->log().warn("No objects were created for \"%s\" (\"%s\")."
                , _hstBaseNameDist.c_str()
                , _hstDescriptionDist.c_str() );
    }
    #if 0
    for( auto idHstPair : _hists ) {
        //if(util::TDirAdapter::tdirectories()[idHstPair.first]) {
        //    assert(util::TDirAdapter::tdirectories()[idHstPair.first]);
        //    util::TDirAdapter::tdirectories()[idHstPair.first]->cd();
        //} else {
        //    gFile->cd();
        //}
        #if 0
        if(std::get<0>(idHstPair.second)) {
            std::get<0>(idHstPair.second)->Write();
        }
        if(std::get<1>(idHstPair.second)) {
            std::get<1>(idHstPair.second)->Write();
        }
        if(std::get<2>(idHstPair.second)) {
            std::get<2>(idHstPair.second)->Write();
        }
        #endif
    }
    #endif
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( MSADCPlotMoyalDistributions, cdsp, cfg
                , "Plots per-detector Moyal parameters distribution"
                ) {
    using namespace na64dp;
    return new handlers::MSADCPlotMoyalDistributions( cdsp
            , aux::retrieve_det_selection(cfg)
            , cfg["baseNameDist"].as<std::string>(), cfg["baseNameCorr"].as<std::string>()
            , cfg["baseDescriptionDist"].as<std::string>(), cfg["baseDescriptionCorr"].as<std::string>()
            , cfg["mu"]["range"][0].as<float>(),      cfg["mu"]["range"][1].as<float>(),        cfg["mu"]["nBins"].as<size_t>()
            , cfg["sigma"]["range"][0].as<float>(),   cfg["sigma"]["range"][1].as<float>(),     cfg["sigma"]["nBins"].as<size_t>()
            , cfg["S"]["range"][0].as<float>(),       cfg["S"]["range"][1].as<float>(),         cfg["S"]["nBins"].as<size_t>()
            , cfg["maxAmp"]["range"][0].as<float>(),  cfg["maxAmp"]["range"][1].as<float>(),    cfg["maxAmp"]["nBins"].as<size_t>()
            , aux::get_logging_cat(cfg)
            , cfg["path"] ? cfg["path"].as<std::string>() : ""
            , aux::get_naming_class(cfg)
            );
}

