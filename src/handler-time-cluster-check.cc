#include "na64detID/cellID.hh"
#include "na64dp/abstractHandler.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/event.hh"
#include "na64event/data/sadc.hh"

#include <climits>
#include <cmath>
#include <log4cpp/Priority.hh>
#include <sstream>

namespace na64dp {
namespace handlers {

/**\brief Does rough checks on uncalibrated items whithin time clusters
 *
 * For given selection, discriminates event if:
 *  - largest amplitudes in all z layers (main and prs for ECAL) are of the
 *    same time cluster
 *  - max amp cell of max cluster matches given index (TODO)
 *  - max amp among all the selection A_{max, 1} is lesser or equal to
 *    r*A_{max,2} where A_{max, 1} and A_{max, 2} are from distinct time
 *    clusters
 *  - mean time of leading cluster (the one with A_{max,1} is within given
 *    time limits (TODO)
 *
 * This handler motly designed to filter events used for calibration.
 *
 * \code{.yaml}
 *     - _type: RawECALTimeClusterChecks
 *       # Output file name; str, opt
 *       output: ...
 * \endcode
 *
 * */
class RawECALTimeClusterChecks : public AbstractHitHandler<event::SADCHit> {
public:
    enum Reason {
        kUnknown=0,
        kNoHits,
        kDifferentClusters,
        kBadLargestAmplitude,
        kMaxCellDiffers,
        kBadRatio,
        kOffTime,
    };
protected:
    const bool _setHitValues;
    const int _xIdx, _yIdx;
    const float _timeWindow[2];
    const bool _invert;
    const float _largestAmpRatio;

    std::unordered_map<Reason, size_t> _discriminated;

    int _largestClusterLabel;

    bool _select_largest_cluster(const event::Event &);
    void _count_discriminated(Reason r) {
        ++_discriminated.emplace(r, 0).first->second;
    }
public:
    /// Constructs a handler immediately opening output file for writing data
    RawECALTimeClusterChecks( calib::Dispatcher & cdsp
                , const std::string & selection
                , bool setHitValues
                , int xIdx, int yIdx
                , float largestAmpRatio
                , float minTime, float maxTime
                , log4cpp::Category & logCat
                , bool invert
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _setHitValues(setHitValues)
        , _xIdx(xIdx), _yIdx(yIdx)
        , _timeWindow{minTime, maxTime}
        , _invert(invert)
        , _largestAmpRatio(largestAmpRatio)
        , _largestClusterLabel(-1)
        {
        _discriminated.emplace(kUnknown, 0);
        _discriminated.emplace(kNoHits, 0);
        _discriminated.emplace(kDifferentClusters, 0);
        _discriminated.emplace(kBadLargestAmplitude, 0);
        _discriminated.emplace(kMaxCellDiffers, 0);
        _discriminated.emplace(kBadRatio, 0);
        _discriminated.emplace(kOffTime, 0);
    }
    /// Writes event header and forwards execution to parent's `process_event()`
    /// to write the hits
    ProcRes process_event(event::Event &) override;
    /// Writes hits
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
    void finalize() override;
};  // class MyHitHandler

bool
RawECALTimeClusterChecks::_select_largest_cluster(const event::Event & event) {
    std::ostringstream oss;
    for(unsigned int zIdx = 0; zIdx < 2; ++zIdx) {  // iterate over parts (layers -- prs, main)
        // populate index of (amp -> cluster)
        std::multimap<float, std::pair<int, DetID>> clusterByAmps;
        for(auto it = event.sadcHits.begin(); it != event.sadcHits.end(); ++it) {
            if(!it->second->rawData) continue;
            // skip unselected items
            if(!SelectiveHitHandler<event::SADCHit>::matches(it->first)) continue;
            // skip not matching zIdx
            if(zIdx != it->first.payload_as<CellID>()->get_z()) continue;

            for(const auto & maximaItem : it->second->rawData->maxima) {
                clusterByAmps.emplace(maximaItem.second->amp
                        , std::pair<int, DetID>(maximaItem.second->timeClusterLabel, it->first)
                        );
            }
        }

        // check that event actually has time clusters in this layer
        if(clusterByAmps.empty()) {
            oss << "Discriminating event " << event.id << " as no hits found.";
            this->log().warn(oss.str());
            _count_discriminated(kNoHits);
            return false;  // no hits?
        }

        // check that largest amplitude is present for both layers (prs and
        // main) and have same time cluster label
        if(_largestClusterLabel == -2) {
            _largestClusterLabel = clusterByAmps.rbegin()->second.first;
        } else if(_largestClusterLabel != clusterByAmps.rbegin()->second.first) {
            oss << "Discriminating event " << event.id << " as largest amps"
                " in different Z belong to different time clusters ("
                << _largestClusterLabel << " for prs, "
                << clusterByAmps.rbegin()->second.first
                << " for main part)."
                ;
            this->log().info(oss.str());
            _count_discriminated(kDifferentClusters);
            return false;
        }
        float largestAmp = clusterByAmps.rbegin()->first;

        // check that cluster is really a cluster (not 0 -- standalone pulse
        // or -1 -- not clustered)
        if(_largestClusterLabel < 1) {
            oss << "Discriminating event " << event.id << " as largest (raw)"
                " amplitude "
                << largestAmp << " is labeled as " << _largestClusterLabel
                << "(in " << naming()[clusterByAmps.rbegin()->second.second] << ")";
            this->log().info(oss.str());
            _count_discriminated(kBadLargestAmplitude);
            return false;
        }

        // check that max amp cell is what we assume for this run
        // This condition is generally not mandatory for non-leaking cases, but
        // to properly account leaks, one has to cosider this
        if(INT_MIN != _xIdx && INT_MIN != _yIdx) {
            auto maxDetID = clusterByAmps.rbegin()->second.second;
            CellID cellID(maxDetID.payload());
            if( cellID.get_x() != (unsigned int) _xIdx
             || cellID.get_y() != (unsigned int) _yIdx) {
                oss << "Discriminating event " << event.id << " because"
                    " max amp at " << naming()[maxDetID] << " does not match"
                    " expected index: (" << _xIdx << ", " << _yIdx << ")";
                this->log().info(oss.str());
                _count_discriminated(kMaxCellDiffers);
                return false;
            }
        }

        // find 2nd largest amp from other cluster, check that it does not
        // exceed ratio -- pretty rough check, relies on the fact that gain
        // is set to roughly adjust peaks within prs or within main part
        // iterate over index backwards (descending)
        if(std::isfinite(_largestAmpRatio)) {
            for(auto it = clusterByAmps.rbegin(); it != clusterByAmps.rend(); ++it) {
                if(_largestClusterLabel == it->second.first) continue;
                float r = it->first / largestAmp;
                if( r > _largestAmpRatio) {
                    oss << "Discriminating event " << event.id
                            << " because pulse amplitude "
                            << it->first << " in "
                            << naming()[it->second.second]
                            << " from time cluster "
                            << it->second.first
                            << " exceeds permitted ratio "
                            << _largestAmpRatio
                            << " with respect to largest amplitude "
                            << largestAmp
                            << " in "
                            << naming()[clusterByAmps.rbegin()->second.second]
                            << " ratio " << r;
                    this->log().info(oss.str());
                    _count_discriminated(kBadRatio);
                    return false;
                }
                break;
            }
        }
    }  // by zIdx

    // check cluster mean time is within window
    if(std::isfinite(_timeWindow[0]) || std::isfinite(_timeWindow[1])) {
        assert(_largestClusterLabel > 0);
        double meanTime = 0;
        size_t nPulsesAccounted = 0;
        for(auto it = event.sadcHits.begin(); it != event.sadcHits.end(); ++it) {
            if(!SelectiveHitHandler<event::SADCHit>::matches(it->first)) continue;
            for(const auto & maximaItem : it->second->rawData->maxima) {
                if(maximaItem.second->timeClusterLabel != _largestClusterLabel) continue;
                meanTime += maximaItem.second->time;
                ++nPulsesAccounted;
            }
        }
        assert(nPulsesAccounted > 0);
        meanTime /= nPulsesAccounted;
        bool timeCheckResult = true;
        if(std::isfinite(_timeWindow[0]) && meanTime < _timeWindow[0]) {
            timeCheckResult = false;
        }
        if(std::isfinite(_timeWindow[1]) && meanTime > _timeWindow[1]) {
            timeCheckResult = false;
        }
        if(!timeCheckResult) {
            oss << "Discriminating event " << event.id
                    << " because cluster mean time "
                    << meanTime*nPulsesAccounted << " / " << nPulsesAccounted << " = "
                    << meanTime << " is outside od presumed time window ["
                    << _timeWindow[0] << ", " << _timeWindow[1] << "]";
            this->log().info(oss.str());
            _count_discriminated(kOffTime);
            return false;
        }
    }

    // set hit values to cluster's ones
    if(_setHitValues) {
        for(auto it = event.sadcHits.begin(); it != event.sadcHits.end(); ++it) {
            if(!SelectiveHitHandler<event::SADCHit>::matches(it->first)) continue;
            auto & hit = *it->second;
            

            hit.rawData->maxAmp = 0.;
            hit.rawData->maxAmpError = 0.;
            hit.rawData->sum = 0.;
            //hit.rawData-> ... width?
            size_t nConsidered = 0;
            for(const auto & maximaItem : it->second->rawData->maxima) {
                if(maximaItem.second->timeClusterLabel != _largestClusterLabel) continue;
                //meanTime += maximaItem.second->time;
                hit.rawData->maxAmp += maximaItem.second->amp;
                hit.rawData->maxAmpError += maximaItem.second->amp
                                          * maximaItem.second->ampErr
                                          ;
                hit.rawData->sum += maximaItem.second->sumFine;
                ++nConsidered;
            }
            // hit.rawData->maxAmp is just a sum
            hit.rawData->maxAmpError /= nConsidered;
            hit.rawData->maxAmpError = sqrt(hit.rawData->maxAmpError);
            //hit.rawData-> ... width?
        }
    }

    return true;
}

RawECALTimeClusterChecks::ProcRes
RawECALTimeClusterChecks::process_event(event::Event & event) {
    _largestClusterLabel = -2;
    if(!_select_largest_cluster(event)) {
        if(!_invert)
            return kDiscriminateEvent;
    } else if(_invert)
        return kDiscriminateEvent;
    return kOk;
}

bool
RawECALTimeClusterChecks::process_hit( EventID
                         , DetID did
                         , event::SADCHit & hit
                         ) {
    // ...?
    return true;
}

void
RawECALTimeClusterChecks::finalize() {
    std::ostringstream oss;
    oss << "Counts by reason of discrimination:" << std::endl
        << "  no hits in event ............... " << _discriminated[kNoHits] << std::endl
        << "  clusters dont match ............ " << _discriminated[kDifferentClusters] << std::endl
        << "  largest amp is not clustered ... " << _discriminated[kBadLargestAmplitude] << std::endl
        << "  wrong max cell ................. " << _discriminated[kMaxCellDiffers] << std::endl
        << "  bad ratio ...................... " << _discriminated[kBadRatio] << std::endl
        << "  out of time .................... " << _discriminated[kOffTime] << std::endl
        ;
    this->log().info(oss.str());
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( RawECALTimeClusterChecks, cdsp, cfg
                , "..."
                ) {
    using namespace na64dp;
    return new handlers::RawECALTimeClusterChecks( cdsp
            , aux::retrieve_det_selection(cfg)
            , cfg["setHitValues"].as<bool>(true)
            , cfg["xIdx"].as<int>(INT_MIN), cfg["yIdx"].as<int>(INT_MIN)
            , cfg["ratio"].as<float>(0.001)
            , cfg["timeWindow"][0].as<float>(), cfg["timeWindow"][1].as<float>()
            , aux::get_logging_cat(cfg)
            , cfg["invert"].as<bool>(false)
            , aux::get_naming_class(cfg)
            );
}

