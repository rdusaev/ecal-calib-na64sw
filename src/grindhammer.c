#include "ecp-grindhammer.h"

#include <math.h>
#include <gsl/gsl_randist.h>


static const gEs = 21.2;  /* MeV, \ref A.2.1 */

int
ecp_grindhammer_init_mat(struct ecp_GrindMaterial * mat, size_t nMedia) {
    double w[nMedia];
    double wSum = 0.;
    for(size_t i = 0; i < nMedia; ++i) {
        wSum += mat[i].density*mat[i].depth;
        if(isnan(mat[i].ECrit))
            mat[i].ECrit = 2.66 * pow(mat[i].radLen * mat[i].Z/mat[i].A, 1.1);
    }
    double Zeff = 0.   /* eff. mat. charge num */
         , Aeff = 0.   /* eff. mat. atomic num */
         , X0eff = 0.  /* eff. rad.len */
         , RMeff = 0.  /* eff. Moliere radius */
         , ECeff = 0.  /* eff. crit. E */
         ;
    for(size_t i = 0; i < nMedia; ++i) {
        w[i] = mat[i].density*mat[i].depth / wSum;

        Zeff += w[i]*mat[i].Z;
        Aeff += w[i]*mat[i].A;
        X0eff += w[i]/mat[i].radLen;
        RMeff += w[i]*mat[i].ECrit / mat[i].radLen; 
    }

    X0eff = 1/X0eff;

    RMeff /= gEs;
    RMeff = 1/RMeff;

    /* sampling frequency: */
    //Fs = X0eff / (da + dp);
    /* "sufficiently good" approximation of e/(mip), [12] */
    //e = 1 / (1+ 7e-4 (Zp - Za);
}

static double
_ecp_grindhammer_hom_longit(const double alpha, const double beta, const double t) {
    return gsl_ran_gamma_pdf(t, alpha, beta);
}
