#include "na64detID/cellID.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/event.hh"
#include "na64event/data/sadc.hh"

#include <cmath>
#include <fstream>
#include <limits>
#include <log4cpp/Priority.hh>

namespace na64dp {
namespace handlers {

/**\brief Writes maxAmp and sum of SADC hits to file in binary form
 *
 * Highly-specialized handler producing binary file output containing
 * `RawDataSADC::maxAmp` and `RawDataSADC::sum` values.
 *
 * Output file is of rather simplistic form, prefixed with event description
 * block:
 *  - 1 byte unsigned int - marker byte denoting beginning of next event (`0xff`)
 *  - 32 bytes unsigned int - event's run number
 *  - 32 bytes unsigned int - event's spill number
 *  - 64 bytes unsigned int - event-in-spill number
 * Then following structure is repeated:
 *  - 1 byte unsigned int - denotes x-index of the cell
 *  - 1 byte unsigned int - denotes y-index of the cell
 *  - 64-byte float - max amp
 *  - 64-byte float - sum
 * End-of-sequence marker assumed to be beginning of next event. In that case
 * an xIdx is set to begin-of-event marker value.
 *
 * Handler omits hits without raw data (no `SADCHit::rawData` instance), hits
 * with both `maxAmp` and `sum` being unset (nan).
 *
 * \code{.yaml}
 *     - _type: DumpLMSInput
 *       # Output file name; str, opt
 *       output: ...
 * \endcode
 *
 * */
class DumpLMSInput : public AbstractHitHandler<event::SADCHit> {
protected:
    /// Destination file stream
    std::ofstream _ofs;
public:
    /// Constructs a handler immediately opening output file for writing data
    DumpLMSInput( calib::Dispatcher & cdsp
                , const std::string & selection
                , const std::string & outFileName
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _ofs(outFileName)
        {
    }
    /// Writes event header and forwards execution to parent's `process_event()`
    /// to write the hits
    ProcRes process_event(event::Event &) override;
    /// Writes hits
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
    /// Closes output file
    void finalize() override {
        _ofs.close();
    }
};  // class MyHitHandler

DumpLMSInput::ProcRes
DumpLMSInput::process_event(event::Event & event) {
    const char eventBgnMarker = 0xff;
    _ofs.write(&eventBgnMarker, 1);
    uint64_t nEvent = event.id.event_no();
    uint32_t runNo = event.id.run_no()
           , spillNo = event.id.spill_no();
    // write event header
    this->log() << log4cpp::Priority::DEBUG
        << "Writing event header " << event.id;
    _ofs.write(reinterpret_cast<char*>(&runNo),   sizeof(runNo));
    _ofs.write(reinterpret_cast<char*>(&spillNo), sizeof(spillNo));
    _ofs.write(reinterpret_cast<char*>(&nEvent),  sizeof(nEvent));
    return AbstractHitHandler<event::SADCHit>::process_event(event);
}

bool
DumpLMSInput::process_hit( EventID
                         , DetID did
                         , event::SADCHit & hit
                         ) {
    if(!hit.rawData) return true;
    if(std::isnan(hit.rawData->maxAmp)) return true;
    // obtain ids into 1-byte ints
    CellID cellID(did.payload());
    unsigned char xIdx = cellID.get_x()
                , yIdx = cellID.get_y()
                , zIdx = cellID.get_z();
    // write hit header
    _ofs.write(reinterpret_cast<char*>(&xIdx), sizeof(xIdx));
    _ofs.write(reinterpret_cast<char*>(&yIdx), sizeof(yIdx));
    _ofs.write(reinterpret_cast<char*>(&zIdx), sizeof(zIdx));
    // write hit payload; make float type explicit here as event's attrs are of
    // type StdFloat_t which can vary
    double maxAmp = hit.rawData->maxAmp
         , ampSum = hit.rawData->sum
         , width = std::numeric_limits<double>::quiet_NaN()
         ;
    // ...
    _ofs.write(reinterpret_cast<char*>(&maxAmp), sizeof(double));
    _ofs.write(reinterpret_cast<char*>(&ampSum), sizeof(double));
    _ofs.write(reinterpret_cast<char*>(&width),  sizeof(double));

    this->log() << log4cpp::Priority::DEBUG
        << "hit " << (int) xIdx << "x" << (int) yIdx << "x" << (int) zIdx
        << " written: amp=" << maxAmp << ", sum=" << ampSum;

    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( DumpLMSInput, cdsp, cfg
                , "Writes maxAmp and sum of SADC hits to file in binary form"
                ) {
    using namespace na64dp;
    return new handlers::DumpLMSInput( cdsp
            , aux::retrieve_det_selection(cfg)
            , cfg["output"].as<std::string>()
            , aux::get_logging_cat(cfg)
            , aux::get_naming_class(cfg)
            );
}

