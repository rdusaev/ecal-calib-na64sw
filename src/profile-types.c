#include "ecp-types.h"

#include <math.h>
#include <assert.h>

double
ecp_cyl_as_cart(double x, double y, double z, void * profileCyl_) {
    struct ecp_ProfileCyl * profileCyl = (struct ecp_ProfileCyl *) profileCyl_;
    /* calc new variables */
    const double rho = sqrt(x*x + y*y)
               , phi = atan2(x, y);
    /* cart/cyl Jacobian brings \rho factor here: */
    return rho * profileCyl->f(rho, phi, z, profileCyl->userdata);
}

int
ecp_print_as_profiles_cyl( struct ecp_ProfileCyl * p
                         , FILE * outf
                         , double rhoMin, double rhoMax, size_t nPointsRho
                         , double zMin, double zMax, size_t nPointsZ
                         , bool integratePhi
                         ) {
    if(nPointsRho < 2) {
        fputs("#points to print by rho is < 1", stderr);
        return -1;
    }
    if(nPointsZ < 2) {
        fputs("#points to print by z is < 1", stderr);
        return -1;
    }
    const double stepRho = (rhoMax - rhoMin)/(nPointsRho - 1)
               , stepZ = (zMax - zMin)/(nPointsZ - 1)
               ;
    for(size_t nZ = 0; nZ < nPointsZ; ++nZ) {
        const double z = zMin + nZ*stepZ;
        for(size_t nRho = 0; nRho < nPointsRho; ++nRho) {
            if(nRho) fputc('\t', outf);
            const double rho = rhoMin + nRho*stepRho;
            if(!integratePhi) {
                double value = p->f(rho, 0., z, p->userdata);
                fprintf(outf, "%12.4e", value);
            } else {
                assert(false);  // TODO
            }
        }
        fputc('\n', outf);
    }
    return 0;
}

