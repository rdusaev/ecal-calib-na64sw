#include "ecp-cell.h"
#include "ecp-generator.h"

#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <stdio.h>
#include <regex.h>
#include <assert.h>
#include <string.h>

int
ecp_generate_all( struct ecp_Generator * g
                , struct ecp_EDepData * cells
                , bool reRunOnWarning
                ) {
    for( struct ecp_EDepData * cell = cells; ; ++cell ) {
        if(!cell->isActive) continue;
        for(;;) {
            int rc = g->generate(cell, g->userdata);
            if(rc < 0) {
                fprintf(stderr, "Error: generator returned %d at cell %ux%ux%u,"
                          " interrupting generator loop.", rc
                        , cell->index[0], cell->index[1], cell->index[2] );
                return -1;
            } else if(rc > 0) {
                fprintf(stderr, "Warning: generator returned %d at cell %ux%ux%u"
                        , rc
                        , cell->index[0], cell->index[1], cell->index[2] ); 
                if(reRunOnWarning) continue;
            }
            break;
        }
        if(cell->isTerminal) break;
    }
    return 0;
}

#define _M_apply_offset_dbl_value(obj, offs) \
    ((double *) (((char *) obj) + offs))

int
ecp_Generator_Const__generate(struct ecp_EDepData * cell, void * userdata) {
    struct ecp_Generator_Const * g = (struct ecp_Generator_Const *) userdata;
    return g->modify( _M_apply_offset_dbl_value(cell, g->memOffset)
                    , g->value );
}

int
ecp_Generator_Uniform__generate(struct ecp_EDepData * cell, void * userdata) {
    struct ecp_Generator_Uniform * g = (struct ecp_Generator_Uniform *) userdata;
    return g->modify( _M_apply_offset_dbl_value(cell, g->memOffset)
                    , g->low + gsl_rng_uniform(g->gslR)*(g->up - g->low));
}

int
ecp_Generator_Gaussian__generate(struct ecp_EDepData * cell, void * userdata) {
    struct ecp_Generator_Gaussian * g = (struct ecp_Generator_Gaussian *) userdata;
    return g->modify( _M_apply_offset_dbl_value(cell, g->memOffset)
                    , g->mean + gsl_ran_gaussian(g->gslR, g->sigma) );
}

#undef _M_apply_offset_dbl_value


static const char gGenSRx[]
    = "^([[:alpha:]][[:alnum:]]+)([+*-/])?=([[:alnum:]]+)([^[:space:]]+)$";
/*      ^ 1, var nm              ^ 2, mod ^ 3, gen.nm   ^ 4, pars*/
regex_t gGenRx;
static bool _rxCompiled = false;

static int
_compile_generator_rx() {
    int rc;
    if(0 != (rc = regcomp(&gGenRx, gGenSRx, REG_ICASE|REG_EXTENDED))) {
        fprintf(stderr, "App error: failed to compile generator regex: %d\n", rc);
        switch(rc) {
            case REG_BADBR:
                fputs("Invalid use of back reference operator.", stderr);
                break;
            case REG_BADPAT:
                fputs("Invalid use of pattern operators such as group or list.", stderr);
                break;
            case REG_BADRPT:
                fputs("Invalid use of repetition operators such as using '*' as the first character.", stderr);
                break;
            case REG_EBRACE:
                fputs("Un-matched brace interval operators.", stderr);
                break;
            case REG_EBRACK:
                fputs("Un-matched bracket list operators.", stderr);
                break;
            case REG_ECOLLATE:
                fputs("Invalid collating element.", stderr);
                break;
            case REG_ECTYPE:
                fputs("Unknown character class name.", stderr);
                break;
            case REG_EEND:
                fputs("Nonspecific error.", stderr);
                break;
            case REG_EESCAPE:
                fputs("Trailing backslash.", stderr);
                break;
            case REG_EPAREN:
                fputs("Un-matched parenthesis group operators.", stderr);
                break;
            case REG_ERANGE:
                fputs("Invalid use of the range operator", stderr);
                break;
            case REG_ESIZE:
                fputs("Compiled regular expression requires a pattern buffer larger than 64 kB.", stderr);
                break;
            case REG_ESPACE:
                fputs("The regex routines ran out of memory.", stderr);
                break;
            case REG_ESUBREG:
                fputs("Invalid back reference to a subexpression.", stderr);
                break;
        }
        return -1;
    }
    return 0;
}

static int
_modify_set(double * dest, const double v) {
    *dest = v;
    return 0;
}

static int
_modify_mul(double * dest, const double v) {
    *dest *= v;
    return 0;
}

static int
_modify_div(double * dest, const double v) {
    *dest /= v;
    return 0;
}

static int
_modify_add(double * dest, const double v) {
    *dest += v;
    return 0;
}

static int
_modify_sub(double * dest, const double v) {
    *dest -= v;
    return 0;
}

static int
_parse_arglist(char * expr, double * args, int nArgs) {
    assert(nArgs > 0);
    char * token, * saveptr, * str, * ep;
    int n;
    for(n = 0, str = expr; ; ++n, str = NULL ) {
        token = strtok_r(str, ",", &saveptr);
        if(NULL == token) break;
        if(n >= nArgs) {
            fprintf( stderr
                   , "Error: at least %d parameters are given, only %d expected.\n"
                   , n + 1, nArgs );
            return -1;
        }
        args[n] = strtod(token, &ep);
        if('\0' != *ep) {
            fprintf( stderr
                   , "Error: can't interpret \"%s\" as floating point number.\n"
                   , token );
            return -1;
        }
    }
    if(n != nArgs) {
        fprintf( stderr
               , "Error: only %d parameters are given, only %d expected.\n"
               , n + 1, nArgs );
        return -1;
    }
    return 0;
}

int
ecp_create_generator(const char * expr, struct ecp_Generator * instance) {
    if(!_rxCompiled) {
        if(_compile_generator_rx()) {
            return -1;
        }
    }
    regmatch_t parts[5];
    int rc;
    if(0 != (rc = regexec(&gGenRx, expr, 5, parts, 0x0 ))) {
        if(REG_NOMATCH) {
            fprintf(stderr, "Generator expression \"%s\" is not valid.\n", expr);
            return -1;
        }
        assert(false);  // must be unreachable
    }
    printf("Expression \"%s\" expanding to:\n", expr);
    size_t attrOffset;
    #define _M_if(n, tok) \
    if(0 == strncmp(tok, expr + parts[n].rm_so, parts[n].rm_eo - parts[n].rm_so))

    /* variable name, get offset */
    assert(-1 != parts[1].rm_so);
    #define _M_set_offset(attr) _M_if(1, #attr) { \
        puts("  attr." #attr ); \
        attrOffset = offsetof(struct ecp_EDepData, attr); \
    }
    _M_set_offset(eDep) else
    _M_set_offset(absModelErr) else
    _M_set_offset(relModelErr) else
    _M_set_offset(calibCoeff) else {
        char nm[64];
        strncpy(nm, expr + parts[1].rm_so, parts[1].rm_eo - parts[1].rm_so);
        nm[parts[1].rm_eo - parts[1].rm_so] = '\0';
        fprintf(stderr, "Generator expr. error: unknown attribute name \"%s\"\n"
                , nm );
        return -1;
    }
    #undef _M_set_offset

    /* modifier type */
    int (*modifier)(double *, const double v);
    if(-1 == parts[2].rm_so) {
        puts("  set");
        modifier = _modify_set;
    } else {
        switch(expr[parts[2].rm_so]) {
            case '+':
                puts("  add");
                modifier = _modify_add;
                break;
            case '-':
                puts("  sub");
                modifier = _modify_sub;
                break;
            case '/':
                puts("  div");
                modifier = _modify_div;
                break;
            case '*':
                puts("  mul");
                modifier = _modify_mul;
                break;
            default:
                assert(false);  /* unreachable due to rx */
                exit(EXIT_FAILURE);
        };
    };

    /* copy arguments list string (will be modified) */
    char * argsStr = NULL;
    if(parts[4].rm_so != -1) {
        argsStr = strdup(expr + parts[4].rm_so);
    }
    /* generator name */
    if(-1 == parts[3].rm_so) {
        struct ecp_Generator_Const * g
            = (struct ecp_Generator_Const *) malloc(sizeof(struct ecp_Generator_Const));
        g->memOffset = attrOffset;
        assert(modifier);
        g->modify = modifier;
        assert(-1 != parts[4].rm_so);
        assert('\0' != expr[parts[4].rm_so]);
        if(_parse_arglist(argsStr, &(g->value), 1)) {
            fputs("Failed to parse args for const \"generator\"\n", stderr);
            free(argsStr);
            free(g);
            return -1;
        }
        printf("  const value, value=%.3e\n", g->value);
        instance->userdata = g;
        instance->generate = ecp_Generator_Const__generate;
    } else _M_if(3, "gaus") {
        struct ecp_Generator_Gaussian * g
            = (struct ecp_Generator_Gaussian *) malloc(sizeof(struct ecp_Generator_Gaussian));
        g->memOffset = attrOffset;
        assert(modifier);
        g->modify = modifier;
        assert(-1 != parts[4].rm_so);
        assert('\0' != expr[parts[4].rm_so]);
        double args[2];
        if(_parse_arglist(argsStr, args, 2)) {
            fputs("Failed to parse args for Gaussian generator.\n", stderr);
            free(argsStr);
            free(g);
            return -1;
        }
        g->mean = args[0];
        g->sigma = args[1];
        g->gslR = gsl_rng_alloc(gsl_rng_default);
        printf("  gaussian dist., mean=%.3e, sigma=%.3e\n", g->mean, g->sigma);
        instance->userdata = g;
        instance->generate = ecp_Generator_Gaussian__generate;
    } else _M_if(3, "uniform") {
        struct ecp_Generator_Uniform * g
            = (struct ecp_Generator_Uniform *) malloc(sizeof(struct ecp_Generator_Uniform));
        g->memOffset = attrOffset;
        assert(modifier);
        g->modify = modifier;
        assert(-1 != parts[4].rm_so);
        assert('\0' != expr[parts[4].rm_so]);
        double args[2];
        if(_parse_arglist(argsStr, args, 2)) {
            fputs("Failed to parse args for uniform generator.\n", stderr);
            free(argsStr);
            free(g);
            return -1;
        }
        g->low = args[0];
        g->up = args[1];
        g->gslR = gsl_rng_alloc(gsl_rng_default);
        printf("  uniform dist., low=%.3e, up=%.3e\n", g->low, g->up);
        instance->userdata = g;
        instance->generate = ecp_Generator_Uniform__generate;
    } else {
        free(argsStr);
        char nm[64];
        strncpy(nm, expr + parts[3].rm_so, parts[3].rm_eo - parts[3].rm_so);
        nm[parts[3].rm_eo - parts[3].rm_so] = '\0';
        fprintf(stderr, "Error: unknown generator type: \"%s\"\n", nm);
        return -1;
    }

    #undef _M_if
    return 0;
}

