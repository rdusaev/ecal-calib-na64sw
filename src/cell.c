#include "ecp-cell.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <assert.h>

size_t
ecp_idx2offset( unsigned short xIdx, unsigned short yIdx, unsigned short zIdx
              , const unsigned short * segmentation ) {
    if(xIdx > segmentation[0]) return SIZE_MAX;
    if(yIdx > segmentation[1]) return SIZE_MAX;
    if(zIdx > segmentation[2]) return SIZE_MAX;
    return (zIdx*segmentation[1] + yIdx)*segmentation[0] + xIdx;
}

int
ecp_offset2idx( const size_t offset
              , unsigned short * xIdx_, unsigned short * yIdx_, unsigned short * zIdx_
              , const unsigned short * segmentation ) {
    const size_t n1 = segmentation[0]*segmentation[1];
    size_t xIdx = offset % segmentation[0]
         , zIdx = offset / n1
         , yIdx = (offset - zIdx*n1)/segmentation[0]
         ;
    if(xIdx >= segmentation[0]) return -1;
    if(yIdx >= segmentation[1]) return -1;
    if(zIdx >= segmentation[2]) return -1;
    *xIdx_ = (unsigned short) xIdx;
    *yIdx_ = (unsigned short) yIdx;
    *zIdx_ = (unsigned short) zIdx;
    return 0;
}

int
ecp_init_cells( struct ecp_EDepData ** data_
              , unsigned short * segmentation
              , const double * dims
              , unsigned int flags
              ) {
    const size_t nCells = segmentation[0]*segmentation[1]*segmentation[2];
    if(0 == nCells) {
        fputs("Error: segmentation resulted in zero cells number.\n", stderr);
        return -1;
    }
    if((nCells+1)*sizeof(struct ecp_EDepData) > 1024*1024*1024) {
        fprintf( stderr
               , "Warning: initializing %hux%hux%hu +1 array of cells will"
                 " require %.3fGb of RAM.\n"
               , segmentation[0], segmentation[1], segmentation[2]
               , (nCells + 1)*sizeof(struct ecp_EDepData)/(1024.*1024*1024)
               );
    }
    if(!*data_) {
        /* allocate memory, +1 cell is optional, used only when leak terms are
         * enabled */
        *data_ = malloc(sizeof(**data_)*(nCells + 1));
    } else {
        *data_ = realloc(*data_, sizeof(**data_)*(nCells + 1));
    }
    if(!*data_) {
        fprintf(stderr, "Error: failed to (re-)allocate %zu cells\n", nCells + 1);
        return -1;
    }
    
    const double xStep = (dims[0]/2)/segmentation[0]
               , yStep = (dims[1]/2)/segmentation[1]
               , zStep =  dims[2]   /segmentation[2]
               ;
    size_t offset;
    for(unsigned short xIdx = 0; xIdx < segmentation[0]; ++xIdx) {
        const double xBgn = -dims[0]/2 + xIdx*xStep
                   , xEnd = xBgn + xStep;
        for(unsigned short yIdx = 0; yIdx < segmentation[1]; ++yIdx) {
            const double yBgn = -dims[1]/2 + yIdx*yStep
                       , yEnd = yBgn + yStep;
            for(unsigned short zIdx = 0; zIdx < segmentation[2]; ++zIdx) {
                const double zBgn = -dims[2]/2 + zIdx*zStep
                           , zEnd = zBgn + zStep;
                offset = ecp_idx2offset(xIdx, yIdx, zIdx, segmentation);
                struct ecp_EDepData * data = *data_ + offset;
                data->index[0] = xIdx; data->dims[0][0] = xBgn; data->dims[0][1] = xEnd;
                data->index[1] = yIdx; data->dims[1][0] = yBgn; data->dims[1][1] = yEnd;
                data->index[2] = zIdx; data->dims[2][0] = zBgn; data->dims[2][1] = zEnd;
                data->isTerminal = 0x0;
                data->isActive = 0x1;
                data->isLeakItem = 0x0;
                /* set some initial values for data expected to be
                 * generated/externally set*/
                data->eDep = data->absModelErr = data->relModelErr = data->measuredValue = NAN;
                /*data->calibCoeff remains unchanged! */
                data->columnLabel = NULL;
            }  /* zIdx */
        }  /* yIdx */
    }  /* xIdx */
    if(flags & 0x1) {
        (*data_)[offset+1].isLeakItem = 0x1;
        (*data_)[offset+1].isTerminal = 0x1;
        (*data_)[offset+1].measuredValue = 1.0;
    } else {
        (*data_)[offset].isTerminal = 0x1;
    }
    return 0;
}

int
ecp_eval_cell( struct ecp_EDepData * data ) {
    data->measuredValue = data->eDep;
    if(!isnan(data->absModelErr)) {
        data->measuredValue += data->absModelErr;
    }
    if(!isnan(data->relModelErr)) {
        data->measuredValue *= data->relModelErr;
    }
    data->measuredValue /= data->calibCoeff;
    return 0;
}

