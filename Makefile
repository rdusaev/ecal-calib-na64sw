override CFLAGS+=$(shell gsl-config --cflags) -Wall -g -ggdb -Iinclude/ -Wfatal-errors
override CXXFLAGS+=$(shell root-config --cflags) -Wall -g -ggdb -Iinclude/ -Wfatal-errors
override LDFLAGS+=$(shell gsl-config --libs) $(shell root-config --libs)

all: exec/ecp-generate \
	 exec/ecp-resolve-test \
	 exec/ecp-fit-peaks \
	 exec/ecp-resolve \
	 exec/ecp-add-hst \
	 exec/ecp-get-widths \
	 libECALCalibHandlers.so

obj/main-%.o: main-%.c
	$(CC) -x c $(CFLAGS) -c -o $@ $^

obj/main-%.o: main-%.cc
	$(CC) $(CXXFLAGS) -c -o $@ $^

obj/%.o: src/%.c
	$(CC) -x c $(CFLAGS) -c -o $@ $^

# "toy MC" generator app:
exec/ecp-generate: obj/generator.o \
			  obj/cell.o \
			  obj/file.o \
			  obj/main-generator.o
	g++ -o $@ $^ $(LDFLAGS)

# solver test app:
exec/ecp-resolve-test: obj/generator.o \
			 obj/stencil.o \
			 obj/cell.o \
			 obj/file.o \
			 obj/cumulist.o \
			 obj/columns-set.o \
			 obj/solver.o \
			 obj/solver-lms.o \
			 obj/main-resolve-test.o
	g++ -o $@ $^ $(LDFLAGS)

# solver app:
exec/ecp-resolve: obj/generator.o \
             obj/stencil.o \
			 obj/cell.o \
			 obj/file.o \
			 obj/cumulist.o \
			 obj/columns-set.o \
			 obj/solver.o \
			 obj/solver-lms.o \
			 obj/main-resolve.o \
			 obj/profile.o
	g++ -o $@ $^ $(LDFLAGS)

# hst combining app
exec/ecp-add-hst: obj/main-add-hst.o
	g++ -o $@ $^ $(LDFLAGS)

# SADC-dump module
libECALCalibHandlers.so: src/handler-dump-lms-input.cc \
						 src/handler-time-cluster-check.cc
	g++ $(shell root-config --cflags) $(CXXFLAGS) -Wall -g -ggdb $^ -shared -fPIC -o $@ $(shell pkg-config na64sw --cflags --libs)

# Retrieves width correlation for each cell from set of files
exec/ecp-get-widths: obj/main-get-width.o
	g++ -g -ggdb $^ $(shell root-config --cflags --libs --glibs --evelibs) -o $@

# Peak-fitting helper app, figuring out scaling factors for subsequent
# analysis
exec/ecp-fit-peaks: obj/main-fit-peaks.o
	g++ -g -ggdb $^ $(shell root-config --cflags --libs --glibs --evelibs) -o $@

clean:
	rm -f obj/*
	rm -f exec/ecp-generate \
		  exec/ecp-resolve \
		  exec/ecp-add-hst \
		  exec/ecp-resolve-test \
		  exec/ecp-get-widths \
		  exec/ecp-fit-peaks \
		  libECALCalibHandlers.so

.PHONY: all clean
