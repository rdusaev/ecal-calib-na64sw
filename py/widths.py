import sys, os, subprocess
import ecalCells

"""
Utility script collecting information on signal-like sigma distribution.

For cell (i,j) should yield list of .root files contining Moyal max.amp./sigma
correlation plots which have to be combined into resulting plots for main part
and preshower within `ecp-get-width' util.

Script invocation syntax example:
    $ widths.py 3x3_plus presets/calib-runs-2023A-50-GeV.txt /path/to/calib/dirs
Snippet of running `ecp-get-widths' app:
    $ ecp-get-widths -r report.pdf \\
            -c 2x3 -o width-cal-2x3.csv -O widths-2x3.root \\
            ~/cernbox/autosync/na64/ecal-calib.2analyze/prefit-processed-[123]x[234].root 
"""

def get_files_list_for_cell(tCellIdx, dirs):
    # list of input files
    inputFiles = []
    for cellIdx in ecalCells.non_zero_cells_3x3_plus(*tCellIdx, *dirs.topology):
        p = dirs.file_path(cellIdx, f'prefit-processed-{cellIdx[0]}x{cellIdx[1]}.root')
        if p: inputFiles.append(p)
    return inputFiles

if "__main__" == __name__:
    baseDirPath = os.getenv('BASE_DIR', './')
    if len(sys.argv) < 4:
        sys.stderr.write("Error: wrong number of cmd line args\n")
        sys.exit(1)
    dirs = ecalCells.CalibDirectories(sys.argv[2], sys.argv[3])

    stencil = None
    if sys.argv[1] == '3x3':
        stencil = ecalCells.non_zero_cells_3x3
    elif sys.argv[1] == '3x3_plus':
        stencil = ecalCells.non_zero_cells_3x3_plus
    else:
        sys.stderr.write("Error: unknown stencil\n")
        sys.exit(1)

    # Iterate over all cells
    for cellIdx in dirs.allCells:
        filesList = get_files_list_for_cell(cellIdx, dirs)
        if not filesList:
            sys.stderr.write('Warning: no input files found for'
                    ' cell %dx%d, skept.\n'%(cellIdx[0], cellIdx[1]))
            continue
        # call the app producing
        cellIDStr = '%dx%d'%(cellIdx[0], cellIdx[1])
        rc = subprocess.run([
            str(os.path.join(baseDirPath, "exec/ecp-get-widths"))
                , "-c", cellIDStr
                , "-r", os.path.join(sys.argv[3], cellIDStr, "width-fitreport-%s.pdf"%cellIDStr)
                , "-o", os.path.join(sys.argv[3], cellIDStr, "width-%s.csv"%cellIDStr)
                , "-O", os.path.join(sys.argv[3], cellIDStr, "width-%s.root"%cellIDStr)
            ] + filesList)
        if rc.returncode != 0:
            sys.stderr.write("Error occured in subprocess: %d, stderr dump:"%rc.returncode)
            sys.stderr.write(rc.stderr)
            continue

