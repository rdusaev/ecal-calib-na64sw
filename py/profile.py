"""
Reads ecal calibration input files iteratively, applying calibration provided
and calculates mean energy deposition and its variance in the periphery cells
to obtain mean energy deposition fractions.
"""

import os, sys, math, collections, re, copy, argparse
import numpy as np
from kahan_sum import KahanSum
from histogram1d import Histogram1D
from sadc_dump_file import SADCDumpFile

#
# Read calibration coefficients
def read_calibration_file(f):
    """
    Reads calibration info output from ASCII file, returns dictionary of
    calibration coefficients indexed by cell ID.
    """
    ccs = {}
    for _, line in enumerate(f.readlines()):
        line = line.split('#')[0].strip()
        if not line: continue
        toks = line.split()
        if toks[0] != 'ECAL': continue  # just a precaution
        zIdx, xIdx, yIdx = map(lambda s : int(s), toks[1:4])
        #print('xxx read', (zIdx, xIdx, yIdx), float(toks[4]))  # XXX
        ccs[(xIdx, yIdx, zIdx)] = float(toks[4])
    return ccs

def cell_invariant_index(primaryIndex, hitIndex):
    return ( hitIndex[0] - primaryIndex[0]
           , hitIndex[1] - primaryIndex[1]
           , hitIndex[2]
           )

class Profile(object):
    """
    Collects profile statistics -- mean energy deposition per cell and
    counters.

    This object can be filled up incrementally, from multiple files. To do this
    it operates with "invariant index".

    Note, that for every event one has to make a distinction on:
        - cells with zero measurement -- the ones from calorimeter that
          actually measured 0
        - leaks -- cells in invariant index space that were absent 
    """
    def __init__(self, segmentation):
        self._segm   = copy.copy(segmentation)
        self._sums   = collections.defaultdict(KahanSum)
        self._sqSums = collections.defaultdict(KahanSum)
        self._nItems = collections.defaultdict(int)

    @property
    def segmentation(self):
        return self._segm

    def consider_hit(self, primaryIdx, localIndex, value):
        #print(primaryIdx, localIndex)  # XXX
        idx = cell_invariant_index(primaryIdx, localIndex)
        self._sums[idx]   += value
        self._sqSums[idx] += value*value
        self._nItems[idx] += 1

    def consider_event(self, primaryIndex, hits):
        for idx in np.ndindex(*self._segm):
            self.consider_hit(primaryIndex, idx, hits.get(idx, 0.))

    def get_as_array(self, tp):
        assert self._segm[2] == 2
        r = []
        for zIdx in (0, 1):
            rows = []
            for yIdx in range(-self._segm[1] + 1, self._segm[1]):
                lineValues = []
                for xIdx in range(-self._segm[0] + 1, self._segm[0]):
                    v = None
                    n = self._nItems[(xIdx, yIdx, zIdx)]
                    s = self._sums[(xIdx, yIdx, zIdx)].s if n else np.nan
                    ss = self._sqSums[(xIdx, yIdx, zIdx)].s if n else np.nan
                    mean = s/n if n else np.nan
                    variance = ss/n - mean*mean if n else np.nan
                    if 'counts' == tp:
                        v = self._nItems[(xIdx, yIdx, zIdx)]
                    elif 'mean' == tp:
                        v = mean
                    elif 'stddev' == tp:
                        v = math.sqrt(variance)
                    elif 'variance' == tp:
                        v = variance
                    elif 'rel-error' == tp:
                        v = math.sqrt(variance)/math.fabs(mean) if n and mean else np.nan
                    else:
                        raise RuntimeError(f'Unknown print type: "{tp}"')
                    lineValues.append(v)
                rows.append(lineValues)
            r.append(rows)
        return r

    def get_sum_of_means(self, idx=None, nullifyNegative=False):
        S = 0.
        for nPart, part in enumerate(self.get_as_array('mean')):
            if idx is not None and idx != nPart: continue
            for row in part:
                for cell in row:
                    if not np.isfinite(cell): continue
                    if nullifyNegative and cell < 0: continue
                    S += cell
        return S
        

    def print(self, f, fmt=None, tp='mean'):
        intS = 0.  # used only if t == 'mean'
        for zIdx, part in enumerate(self.get_as_array(tp)):
            f.write('# main:\n' if zIdx else '# prs:\n')
            for yIdx, row in enumerate(reversed(part)):
                for xIdx, v in enumerate(row):
                    # this check is to make sure we did not messed up with
                    # indexing at get_as_array():
                    if tp == 'count': assert v == self._nItems[(xIdx, yIdx, zIdx)]
                    if fmt is None:
                        f.write(str(v))
                    else:
                        f.write(fmt%v)
                f.write('\n')
            f.write('\n')
        if 'mean' == tp:
            f.write(f'# S={intS:e}\n')

    def save(self, f):
        """
        Used to save collected profile data for further usage with ecp-resolve.
        Saves sum, sq.sums, number of items
        """
        #f.write(f'{self._segm[0]} {self._segm[1]} {self._segm[2]}\n') # XXX
        keys = sorted(self._sums.keys())
        for k in keys:
            f.write( f'{k[0]:3d} {k[1]:3d} {k[2]:3d}  '
                f'{self._sums[k].s:16.8e} {self._sqSums[k].s:16.8e} {self._nItems[k]:9d}\n')
            
def read_file_to_profile_with_calibration(primaryCellIndex, profile, dumpFile, ccs
            , sumHistogram=None ):
    hitsDict = {}
    nEvts = 0
    for _, hitsArray in dumpFile:  # 1st is event ID
        eSum = 0.  # energy sum in this event
        for xIdx, yIdx, zIdx, ampMax, _, _ in hitsArray:  # 2nd: sum, 3rd: width
            k = (xIdx, yIdx, zIdx)
            #print(k)  # XXX
            #if xIdx >= segmentation[0] \
            #or yIdx >= segmentation[1] \
            #or zIdx >= segmentation[2]:
            #    # Possible culprits:
            #    #   - wrong ECAL design (e.g. 6x6 instead of 5x6)
            #    #   - not only ECAL was used to generate the dump (e.g. VETO-6)
            #    raise RuntimeError(f"Data file provided item with index (xIdx={xIdx}"
            #            f", yIdx={yIdx}, zIdx={zIdx}) which exceeds assumed"
            #            f" segmentation {segmentation[2]}x{segmentation[0]}x{segmentation[1]}.")
            if k in ccs.keys():
                calibFact = ccs[k]
            else:
                raise RuntimeError("No calibration coefficient for ECAL%d-%d-%d"%(zIdx, xIdx, yIdx))
            eDep = ampMax*calibFact
            eSum += eDep
            hitsDict[(xIdx, yIdx, zIdx)] = eDep
        nEvts += 1
        profile.consider_event(primaryCellIndex, hitsDict)
        if sumHistogram is not None:
            sumHistogram.add(eSum)
    return nEvts

def plot_report(outfile, profile, histograms=None):  # {{{
    try:
        import seaborn as sns
        from matplotlib import pyplot as plt
        from matplotlib.backends.backend_pdf import PdfPages
        from matplotlib.colors import LogNorm
    except ImportError as e:
        sys.stderr.write('Unable to import one of the modules required for'
                         f'graphical report: {str(e)}\n' )
        return
    figs = []
    sns.set_theme()

    # Plot main values as annotated heatmaps
    #
    profileFig, profileAxs = plt.subplots(4, 2, figsize=(4*8.27, 4*11.69))
    figs.append(profileFig)
    for nPlot, (nm, logScale, fmt) in enumerate([
                          ('counts', False, 'd')
                        , ('rel-error', True, '.1f')
                        , ('mean', True, '.3f')
                        , ('stddev', False, '.2e')
                        ]):
        for nPart, data in enumerate(profile.get_as_array(nm)):
            sns.heatmap(data, annot=True
                       , norm=LogNorm() if logScale else None
                       , fmt=fmt
                       #, square=True
                       , ax=profileAxs[nPlot][nPart]
                       )
            title = f'{nm}, {"main" if nPart else "prs"}'
            if 'mean' == nm:
                if 0 == nPart:
                    title += f', S={profile.get_sum_of_means(0):.2e}, S_+={profile.get_sum_of_means(0, True):.2e}'
                else:
                    title += f', S={profile.get_sum_of_means(1):.2e}, S_+={profile.get_sum_of_means(1, True):.2e}'
            profileAxs[nPlot][nPart].set_title(title)
            profileAxs[nPlot][nPart].invert_yaxis()

    # Plot sum spectra for primary cells being read
    #
    if histograms is not None:
        sumSpectraFig, sumSpectraArgs = plt.subplots(
                profile.segmentation[1], profile.segmentation[0],  # nrows, ncols
                figsize=(4*8.27, 4*11.69))
        for xIdx in range(profile.segmentation[0]):
            for yIdx in range(profile.segmentation[1]):
                k = (xIdx, yIdx)
                if k not in histograms.keys(): continue
                h = histograms[k]
                binCenters = h.binCenters
                counts = h.counts
                plotYIdx = profile.segmentation[1] -yIdx - 1  # flipped!
                sumSpectraArgs[plotYIdx][xIdx].hist(
                        binCenters, weights=counts, bins=binCenters )
                sumSpectraArgs[plotYIdx][xIdx].set_title(
                        f'{xIdx}x{yIdx}, {h.mean:.2e}+/-{h.stddev:.1e} (full: {h.fullMean:.2e}+/-{h.fullStddev:.1e})')
        figs.append(sumSpectraFig)

    # Save pdf file
    #
    with PdfPages(outfile) as pp:
        for fig_ in figs:
            pp.savefig(fig_, bbox_inches='tight')
    # }}}

def main(calibFilePath, cellDumpFiles
         , cellSumHistArgs=None, plotFilePath=None, profileFileOutputPath=None
         ):
    if len(sys.argv) < 3:
        sys.stderr.write('Error: at least two command line args expected.\n')
        sys.exit(1)
    
    # Read calibration coefficients
    with open(calibFilePath, 'r') as f:
        ccs = read_calibration_file(f)
    segmentation = [0, 0, 0]
    for idx in ccs.keys():
        for i in range(3):
            if idx[i] > segmentation[i]: segmentation[i] = idx[i]
    for i in range(3):
        segmentation[i] += 1
    sys.stdout.write(f'# Segmentation in use: {segmentation[0]}x{segmentation[1]}x'
            f'{segmentation[2]} (obtained from calib file)\n')
    # Read events from every file given and fill up the table
    tables = {}
    profile = Profile(segmentation)
    histograms = None
    if cellSumHistArgs:
        histograms = collections.defaultdict(lambda: Histogram1D(*cellSumHistArgs))
    for fileName in cellDumpFiles:
        strCellID = re.findall(r'\d-\d', os.path.basename(fileName) )
        if not strCellID:  # or 1 != len(strCellID):
            sys.stderr.write(f'Error: can\'t assume cell index for file "{fileName}"')
            sys.exit(1)
        primeCellIdx = tuple(map(lambda i: int(i), strCellID[0].split('-')))
        assert primeCellIdx not in tables.keys()
        assert len(primeCellIdx) == 2
        sys.stdout.write(f'# Reading file {fileName} (assuming primary cell {primeCellIdx}) ... ')
        sys.stdout.flush()
        with open(fileName, 'rb') as f:
            ff = SADCDumpFile(f)
            nEvents = read_file_to_profile_with_calibration( primeCellIdx
                    , profile, ff(), ccs
                    , sumHistogram=(histograms[primeCellIdx] if histograms is not None else None))
        sys.stdout.write(f'{nEvents} read\n')
    # save profile file if need
    if profileFileOutputPath:
        with open(profileFileOutputPath, 'w') as f:
            profile.save(f)
    # print report if need
    for plotType, fmt in [ ("counts", "%12d")
                  , ("mean", "%12.3e")
                  , ("stddev", "%12.3e")
                  , ("rel-error", "%12.3e")
                  ]:
        sys.stdout.write(f'# {plotType}\n')
        profile.print(sys.stdout, tp=plotType, fmt=fmt)
    # print histograms if need
    if histograms:
        for k, hst in histograms.items():
            sys.stdout.write(f'# Sums with primary cell {k}:\n')
            hst.print(sys.stdout, stats=True)
    # plot the stuff into .pdf file if need
    if plotFilePath:
        plot_report(plotFilePath, profile, histograms=histograms)

if "__main__" == __name__:
    p = argparse.ArgumentParser(prog='ecp-profile')
    p.add_argument('-c', '--calibrations', help='Calibration output generated by'
                   ' ecp-resolve to be applied to cell dump files to obtain'
                   ' energy depositions'
                   , required=True
                   , dest='calibFilePath'
                   )
    p.add_argument('-o', '--output', help='Writes extracted profile in the'
                   ' format compatible with ecp-resolve (ASCII table wih'
                   ' enlarged precision).'
                   , dest='profileFileOutputPath'
                   )
    p.add_argument('-p', '--report', help='Given path turns on report'
                   ' file containing illustrative plots. Requires seaborn'
                   ' module in current Python installation.'
                   , dest='plotFilePath'
                   )
    p.add_argument('cellDumpFiles', nargs='+')  # action='append')
    args = p.parse_args()  # sys.argv) -- dont! see: https://stackoverflow.com/questions/17118999/python-argparse-unrecognized-arguments
    args = dict(vars(args))
    args['cellSumHistArgs'] = (.5, 1.5, 40)
    sys.exit(main(**args))

