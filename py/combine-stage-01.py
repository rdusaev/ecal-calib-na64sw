import sys, os, collections, subprocess, shutil

inputFile=sys.argv[1]
inputDir=sys.argv[2]

filesToMerge = collections.defaultdict(list)

with open(inputFile) as f:
    for line in f:
        line = line.strip()
        line = line.split('#')[0]
        if not line: continue
        chunk, runID, chunkID, cellX, cellY, avroDST = line.split()
        summaryFile = os.path.join(inputDir, f'processed.prelim.{runID}-{chunkID}.root')
        if not os.path.isfile(summaryFile):
            sys.stderr.write(f'Warning: no file {summaryFile}, skip\n')
            continue
        if os.path.getsize(summaryFile) < 1024:
            sys.stderr.write(f'Warning: file {summaryFile} is <1kb, skip\n')
            continue
        filesToMerge[(int(cellX), int(cellY))].append(summaryFile)

#print(filesToMerge)

filesToProcess = {}
for cellIdx, files in filesToMerge.items():
    resultFileBasename = f'prelim.{cellIdx[0]}x{cellIdx[1]}.root'
    resultFile = os.path.join(inputDir, resultFileBasename)
    tmpFile = os.path.join("/tmp", resultFileBasename)
    if os.path.isfile(resultFile):
        sys.stderr.write(f'Warning: file {resultFile} exists, omit {cellIdx}.\n')
        continue
    if os.path.isfile(tmpFile):
        os.remove(tmpFile)
    # run hadd
    cmd = f'hadd {tmpFile} {" ".join(files)}'
    print(f'Merging prelim stats for {cellIdx}...')
    r = subprocess.run(cmd.split()
            , stdout=subprocess.PIPE
            , stderr=subprocess.PIPE
            )
    if r.returncode != 0:
        sys.stderr.write(f'Error: hadd returned {r.returncode} for {cellIdx}\n * * * stderr dump:')
        sys.stderr.write(r.stderr.decode())
        sys.stderr.write(' * * * stdout dump:\n')
        sys.stderr.write(r.stdout.decode())
        continue
    shutil.move(tmpFile, resultFile)
