import glob, os

"""
Utilities to work with ECAL cells topology.
"""

def all_cells(xNCells, yNCells):
    """
    Generates set of all indeces for given topology.
    """
    for yCellIdx in range(yNCells):
        for xCellIdx in range(xNCells):
            yield (xCellIdx, yCellIdx)

def non_zero_cells_3x3(xIdx, yIdx, xNCells, yNCells):
    """
    Generates cell indeces that assumed as having non-zero energy deposition
    within 3x3 pattern.
    """
    xStart, xEnd = xIdx - 1, xIdx + 1
    if xStart < 0: xStart = 0
    if xEnd >= xNCells: xEnd = 0
    yStart, yEnd = yIdx - 1, yIdx + 1
    if yStart < 0: yStart = 0
    if yEnd >= yNCells: yEnd = 0
    for yCellIdx in range(yStart, yEnd+1):
        for xCellIdx in range(xStart, xEnd+1):
            yield (xCellIdx, yCellIdx)

def non_zero_cells_3x3_plus(xIdx, yIdx, xNCells, yNCells):
    """
    Generates cell indeces that assumed as having non-zero energy deposition
    within 3x3 pattern without diagonal periphery (i.e. within +-like shape).
    """
    deltas = [(0, 0), (0, +1), (0, -1), (-1, 0), (+1, 0)]
    for d in deltas:
        xi = xIdx + d[0]
        yi = yIdx + d[1]
        if xi < 0 or xi >= xNCells: continue
        if yi < 0 or yi >= yNCells: continue
        yield (xi, yi)

def zero_cells_3x3(xIdx, yIdx, xNCells, yNCells):
    """
    Generates cell indeces that assumed as having zero energy deposition
    outside the given pattern.
    """
    allCells = set(all_cells(xNCells, yNCells))
    nonZeroCells = set(non_zero_cells_3x3(xIdx, yIdx, xNCells, yNCells))
    zeroCells = allCells - nonZeroCells
    for idx in sorted(zeroCells):
        yield idx

#
# File reading procedures

def cells_in_dir(path):
    for cellPath_ in glob.glob(path + '/?x?/'):
        nrmPath = os.path.normpath(cellPath_)
        cellDir = os.path.basename(nrmPath)
        cellIdx = tuple(int(idx) for idx in cellDir.split('x'))
        yield (cellIdx, nrmPath)

def tbname_to_idx(nm):
    """Turns `ECAL0:4-5-0` into (4, 5, 0)"""
    detName, detIdx = nm.split(':')
    assert detName == 'ECAL0'
    return tuple(int(i) for i in detIdx.split('-'))

def idx_to_tbname(xi, yi, zi):
    """Turns (4, 5, 0) into `ECAL0:4-5-0`"""
    return f'ECAL0:{xi}-{yi}-{zi}'

class CalibDirectories(object):
    """
    Represents dir topology related to calib. dirs
    """
    def __init__( self
                , runsTable  #'/eos/user/r/rdusaev/calib-runs-2023A-2.txt'
                , dirPath
                , topology=None
                ):
        self.runsTable = runsTable
        self.dirPath = dirPath  # TODO: os.path.normpath()
        self.discoveredCells = set()
        self.topology = topology or [0, 0] # note: can be overriden by arg
        for cellIdx, _ in cells_in_dir(dirPath):
            self.discoveredCells.add(cellIdx)
            # NOTE: topology should be taken from runs table, not from dir
        self.cell2RunsIds = {}
        with open(runsTable) as f:
            for nLine, line_ in enumerate(f):
                line = line_.split('#')
                line = line[0].strip()
                if not line: continue
                toks = line.split()
                runIds = list(int(rStr) for rStr in toks[0].split(','))
                cellIdx = tuple(int(iStr) for iStr in toks[1].split('x'))
                assert 2 == len(cellIdx)
                if not topology:
                    if self.topology[0] < cellIdx[0]: self.topology[0] = cellIdx[0]
                    if self.topology[1] < cellIdx[1]: self.topology[1] = cellIdx[1]
                self.cell2RunsIds[cellIdx] = runIds
                # ... other useful info from runs table
        if not topology:
            self.topology = (self.topology[0] + 1, self.topology[1] + 1)  # by 1 gt max value found

    @property
    def missedCells(self):
        return set(all_cells(*self.topology)) - self.discoveredCells

    @property
    def allCells(self):
        """
        Generates list of all cells foreseen by current topology (even if
        the cell is missed)
        """
        for xIdx in range(self.topology[0]):
            for yIdx in range(self.topology[1]):
                yield (xIdx, yIdx)

    def file_path(self, cellIdx, fileName):
        """
        Returns true if file exists in cell's dir.
        """
        if cellIdx in self.missedCells: return None
        p = os.path.join(self.dirPath, '%dx%d'%(cellIdx[0], cellIdx[1]), fileName)
        if os.path.isfile(p): return p
        return None


    #def zero_info_for_cell(self, stencil, idx):
    #    """
    #    Yields a sequence of files paths that correspond
    #    to calibration runs when cell given as index has predominantly zero
    #    amplitude
    #    according to given stencil.
    #    """

    def __str__(self):
        return f'{self.topology[0]}x{self.topology[1]},' \
             + (' ' + ', '.join(str(idx) for idx in  sorted(self.missedCells)) + ' missed' if self.missedCells else '') \
             + f' {len(self.discoveredCells)} discovered towers'
