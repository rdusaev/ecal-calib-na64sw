
class KahanSum(object):
    """
    Kahan summator object.
    """
    def __init__(self):
        self.summ = 0.
        self.c = 0.

    def __iadd__(self, value):
        y = value - self.c
        t = self.summ + y
        #print(f'y={y}, t={t}; summ={self.summ}, c={self.c}')
        self.c = (t - self.summ) - y
        self.summ = t
        return self

    @property
    def s(self):
        return self.summ

if "__main__" == __name__:
    import numpy.random

    entries = []
    #for 

