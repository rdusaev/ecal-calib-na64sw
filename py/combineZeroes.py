#!/usr/venv python3

import os, sys, glob, json
import collections
import subprocess
import ecalCells

"""
This script operates with the collection of outputs produced by set of
calibration runs. The workflow is guided by indeces of illuminated cells --
depending on which cell was, in fact, illuminated, this script takes into
account (expected) zero information (zero-like sum amps, DFT amplitudes, etc).

One may consider 3x3 or 5x5 (TODO) stencil assuming (almost) all the energy was
deposited in those cells. Then, depending on index (say, 2x3).

TODO: some of the classes used here were moved to cells.py,.
"""

#                           * * *   * * *   * * *

def read_dft_fit_results(filePath):
    try:
        with open(filePath, 'r') as f:
            return json.load(f)
    except Exception as e:
        sys.stderr.write(f'Error with file "{filePath}": {str(e)}, skipping.')
        return None

def parse_fft_means_line(line):
    toks = line.split(',')
    # First three tokens are cell ID, low and up freqs for rel amps
    detName, relFreqLow, relFreqUp = toks[0], int(toks[1]), int(toks[2])
    toks = toks[3:]
    assert len(toks)%2 == 0  # number of remaining frequencies is even
    # transform remaining toks into list of tuples (mean, variance)
    values = []
    for i in range(0, len(toks), 2):
        values.append((float(toks[i], float(toks[i+1]))))
    return [detName, (relFreqLow, relFreqUp), values]

def fft_means_file(filename):
    with open(filename, 'r') as f:
        for nLine, line in enumerate(f):
            try:
                yield parse_fft_means_line(line)
            except Exception as e:
                sys.stderr.write(f'Error at {filename}:{nLine}:\n')
                sys.stderr.write(str(e))
                raise e

#                           * * *   * * *   * * *

def zero_info_files(cds, excludeCells=None):
    """
    Assumes there are `processed-MxN-zeros.root` files in the
    cds:CalibDirectories.
    Note: `excludeCells` meaning is rather intuitive -- indeces from this set
    won't be considered as ones for which zero info must be extracted.
    """
    if excludeCells is None:
        excludeCells=set([])  # empty
    # for every cell, get the list of cells where we can retrieve zero
    # information
    zeroInfoRuns = collections.defaultdict(list)
    for actCellIdx, actRunsIds in cds.cell2RunsIds.items():
        for cellIdx in ecalCells.zero_cells_3x3(*actCellIdx, *cds.topology):  # <- TODO: parameterized stencil here?
            if cellIdx in excludeCells: continue
            zeroInfoRuns[cellIdx].append(actCellIdx)
    # Iterate over all the cells found according to given stencil
    for zeroCellIdx, calibCellIdx in zeroInfoRuns.items():
        paths = []
        for i in calibCellIdx:
            p = os.path.join(cds.dirPath, '%dx%d'%(i[0], i[1]), f'processed-{i[0]}x{i[1]}-zeros.root')
            paths.append(p)
        yield zeroCellIdx, paths

#                           * * *   * * *   * * *
# Entry point

if "__main__" == __name__:
    if sys.argv[1] == 'combine':
        # Create calib. dirs logic wrapper
        calibDirs = ecalCells.CalibDirectories( sys.argv[2]  # runs table file
                , sys.argv[3]  # directories
                )
        print('Info:', str(calibDirs))

        #
        # Combine zero info into new .root file
        hstAddExec = 'exec/ecp-add-hst'
        zeroOutFile = './out.root'  # TODO: configurable
        # Target histogram being combined
        #   Note absence of leading slash here. It seems, ROOT has troubles
        #   mkdir()'ing this directory otherwise:
        zeroHistInfTPaths = [
                  'ECAL/x{idx[0]}/y{idx[1]}/spectra-dist-abs-amp-{idx[0]}-{idx[1]}-{idx[2]}'
                , 'ECAL/x{idx[0]}/y{idx[1]}/linear-sum-fine-{idx[0]}-{idx[1]}-{idx[2]}'
                ]

        # Combine zero information (stage I of ecal calib)
        # - if file referenced by `zeroOutFile' exists, remove it
        try:
            os.remove(zeroOutFile)
        except OSError:
            pass
        # - iterate over cells, combine histograms and write to `zeroOutFile' using
        #   `exec/ecp-add-hst' app, run for each histogram name
        for cellIdx_, filesList in zero_info_files(calibDirs):  # over cells
            for zIdx in range(2):
                idx = (cellIdx_[0], cellIdx_[1], zIdx)
                for hstTPathTemplate in zeroHistInfTPaths:
                    tpath = hstTPathTemplate.format(idx=idx)
                    # subprocess
                    subprocess.run( [hstAddExec, zeroOutFile, tpath] + filesList
                                  , check=True )
    elif sys.argv[1] == 'collect-dft':
        # NOTE: code below just gets mean and stddev values, ignoring fit info
        # which we can utilize in future to build PDF/CDF for more advanced
        # zero suppression techniques
        with open(sys.argv[2]) as f:
            obj = json.load(f)
        r = collections.defaultdict(list)
        for dftItem in obj['detectors']:
            for ff in dftItem['freqFits']:
                # One can iterate over fitted functions:
                #for fitFuncName in ff['fitResults'].keys():
                #    if ff['fitResults'][fitFuncName] is None: continue
                #    for ffPName in ff['fitResults'][fitFuncName]["parameters"].keys():
                #        #assert len(ffPName) > 0
                #        if 0 == len(ffPName):
                #            sys.stderr.write(f"Warning: ignoring empty fit func's parameter name; function \"{fitFuncName}\"\n")
                #            continue
                #        fitFuncParameterNames[fitFuncName].add(ffPName)
                # However, currently we just use mean values per frequency
                r[dftItem['tbname']].append((ff['meanAmp'], ff['stdDev']))
        for tbname in sorted(r.keys()):
            # Line syntax is: <tbname>,<freq-bgn>,<freq-end>,<a|r>,<17x2-mean-and-stddev>
            sys.stdout.write(f'{tbname},0,0,a,' + ','.join(f'{item[0]:e},{item[1]:e}' for item in r[tbname]) + '\n')

