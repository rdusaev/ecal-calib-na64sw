"""
Provides calib dump file reading class.
Note: sync with:
    1. src/handler-dump-lms-input.cc writing function
    2. src/solver.c reading function
"""

import struct
import numpy as np
import sys

class SADCDumpFile(object):
    """
    SADC calibration file representation.
    Provides generator interface to read the data from ECAL calibration file,
    via __call__()
    """
    def __init__(self, f):
        self.f = f
        # formats
        # - eventID
        self.eventIDFmt = '=IIQ'  # runNo:uint32_t, spillNo:uint32_t, nEvent:uint64_t
        self.eventIDSize = struct.calcsize(self.eventIDFmt)
        self.markerFmt = '=B'
        self.markerSize = struct.calcsize(self.markerFmt)
        self.hitFmt = '=BBddd' # yIdx:uint8_t, zIdx:uint8_t, maxAmp:double, ampSum:double, width:double
        self.hitSize = struct.calcsize(self.hitFmt)
        self._nEventsRead = 0

    def _read_event_id(self):
        bEventID = self.f.read(self.eventIDSize)
        if not bEventID: return None
        return struct.Struct(self.eventIDFmt).unpack_from(bEventID)

    def __call__(self, ampMax=4096):
        """
        A generator method yielding events as tuples of the following form:
            ( (<run-no>, <spill-no>, <event-in-spill-no>)
            , [
                (xIdx, yIdx, zIdx, maxAmp, ampSum, width)
                ...
              ]
            )
        """
        eventID = None
        hits = []
        eventIsGood = True
        while True:
            # read first byte
            bMarker = self.f.read(self.markerSize)
            if not bMarker: break  # legitimate exit of reading loop -- no more events
            xIdxOrEnd = struct.Struct(self.markerFmt).unpack_from(bMarker)[0]
            if xIdxOrEnd == 0xff:  # a marker indicating new event starts
                eventIDNew = self._read_event_id()
                if eventIDNew is None:
                    sys.stderr.write('Error: file truncated (cant read event ID)).\n')
                    break
                if eventID and eventIsGood:
                    # having new event, finalize and yield previous
                    self._nEventsRead += 1
                    yield (eventID, hits)
                eventIsGood = True
                hits = []
                eventID = eventIDNew
                # re-read xIdx
                bXIdx = self.f.read(self.markerSize)
                if not bXIdx:
                    sys.stderr.write('Error: file truncated (can\'t read xIdx)).\n')
                    break
                xIdxOrEnd = struct.Struct(self.markerFmt).unpack_from(bXIdx)[0]
            # otherwise read other two bytes
            bHitData = self.f.read(self.hitSize)
            if not bHitData:
                sys.stderr.write('Error: file truncated (can\'t read hit data).\n')
                break
            hitData = struct.Struct(self.hitFmt).unpack_from(bHitData)
            assert hitData[0] < 7
            assert hitData[1] < 3
            hits.append( (xIdxOrEnd,) +  hitData)
            # mark event as not valid, if ampMax not finite or exceeds the
            # limit
            if hitData[2] > ampMax or not np.isfinite(hitData[2]):
                eventIsGood = False

    @property
    def nEventsRead(self):
        return self._nEventsRead
