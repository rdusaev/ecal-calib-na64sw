import sys, math
from collections import defaultdict

"""
A short script generating pedestals file for direct usage
in run.02.get-zero-levels.yaml.
"""

# Old format:
#extPedFitFields = [ "detName", "pedType"
#    , "mean", "rms", "skewness", "kurtosis"
#    , "width", "widthErr", "mpv", "mpvErr", "area", "areaErr"
#    , "sigma", "sigmaErr", "chi2", "ndf", "nItems"]
extPedFitFields = ["detName","pedType","hist.mean","hist.stddev","gaus.mean","gaus.stddev"]

items = defaultdict(lambda: [None, None])

with open(sys.argv[1], 'r') as f:
    for lineNo, line in enumerate(f):
        if line.startswith('#'): continue
        toks = line.strip().split(',')
        assert len(toks) == len(extPedFitFields)
        values = dict(zip(extPedFitFields, toks))
        for k in values.keys():
            if k in ("detName", "pedType"): continue
            values[k] = float(values[k])
        meanPedValue, meanPedValueVariance = None, None
        if not math.isnan(values["gaus.mean"]):
            meanPedValue = values["gaus.mean"]
            meanPedValueVariance = values["gaus.stddev"]
        else:
            assert not math.isnan(values["hist.mean"])
            meanPedValue = values["hist.mean"]
            meanPedValueVariance = values["hist.stddev"]
        items[values["detName"]][ 0 if values["pedType"].lower() == "even" else 1] = (meanPedValue, meanPedValueVariance)

with open(sys.argv[2], 'w') as f:
    for k in sorted(items.keys()):
        v = items[k]
        f.write(f"{k},{v[0][0]:e},{v[0][1]:e},{v[1][0]:e},{v[1][1]:e}\n")

