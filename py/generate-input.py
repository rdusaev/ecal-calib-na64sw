"""
Draft script to convert calib runs list ASCII table into input tables for
`spread-jobs-table.sh' script.

Example:
    
    $ python3 py/generate-input.py presets/calib-runs-2024A-100GeV.txt ~/na64-files-list.25.02.025.txt > presets/input/2024A-100GeV.txt

TODO: format string is now a hardcoded regexp, must be composed dynamically in
      the future
"""

import sys, re, json

srcTableFile=sys.argv[1]
filesListCache=sys.argv[2]
chunkInputTable=sys.argv[3]
cellInputTable=sys.argv[4]

rxChunkFileName = re.compile(r'^(?P<file>cdr(?P<chunkID>\d+)-(?P<fullRunID>0*(?P<runID>\d+))\.dat)$')

# Expected items:
#  - xCell:int
#  - yCell:int
#  - prs:float
#  - main:float
#  - run ID:int
gItemTypes = {
        'xCell': int, 'yCell': int,
        'prs': float, 'main': float,
        'runID': int, 'nSpills': int
    }
# possible fmt strings (TODO: get from file annotations?)
# <xCell>x<yCell> <prs> <main> <runNo>
rxLine=re.compile(r'^\s*(?P<xCell>\d+)x(?P<yCell>\d+)\s+(?P<prs>[\d\.]+)\s+(?P<main>[\d\.]+)\s+(?P<runID>\d+)\s*(?P<nSpills>\d+)?$')

# Read items from input ASCII table
calibRunsDict = {}  # dict of cells being calibrated
runsOfInterestCache = set()  # list of runs we're interested in (cache)
with open(srcTableFile, 'r') as f:  # TODO: argparse
    for nLine, line_ in enumerate(f):
        line = line_.strip()  # drop spaces
        if line.startswith('#'): continue  # omit lines starting with #
        m = rxLine.match(line)
        if not m:
            raise RuntimeError(f'{srcTableFile}:{nLine+1} does not match line format')
        entry = m.groupdict()
        for k in entry.keys():
            assert k in gItemTypes
            entry[k] = gItemTypes[k](entry[k])
        key = (entry['xCell'], entry['yCell'])
        entry.pop('xCell')
        entry.pop('yCell')
        runsOfInterestCache.add(entry['runID'])
        if key in calibRunsDict:
            calibRunsDict[key].append(entry)
        else:
            calibRunsDict[key] = [entry]

#print(calibRunsDict)  # dbg printout

# Retrieve chunks list relying on files list cache
with open(filesListCache, 'r') as f:  # TODO: argparse
    # this "cache files" is literally a 
    #  $ ls /eos/experiment/na64/data/cdr/ > cache.txt
    # so its format is just
    #  -rw-r--r--. 1 na64eos def-cg    3207204 Jul 18  2015 cdr01001-000016.dat
    for line_ in f:
        line = line_.strip().split()
        if not line: continue
        chunkFile = line[-1]
        if not (chunkFile.startswith('cdr') and chunkFile.endswith('.dat')): continue
        m = rxChunkFileName.match(chunkFile)
        if not m: continue
        if int(line[4]) < 1024: continue  # drop chunks with size < 1kb
        chunkItem = m.groupdict()
        chunkItem['runID']   = int(chunkItem['runID'])
        chunkItem['chunkID'] = int(chunkItem['chunkID']) - 1000
        runID = chunkItem['runID']
        if runID not in runsOfInterestCache: continue  # skip run we're not interested in
        # find run entry this chunk belongs to (in calibRunsDict)
        for cellID, runEntries in calibRunsDict.items():
            for runEntry in runEntries:
                if runEntry['runID'] != runID: continue
                # append chunk list
                if 'chunks' in runEntry.keys():
                    runEntry['chunks'].append(chunkItem)
                else:
                    runEntry['chunks'] = [chunkItem]

# Chunk input table
#

#print(calibRunsDict)
with open(chunkInputTable, 'w') as f:
    f.write('# columns: inFile runID chunkID cellX cellY outFile_DST\n')
    for cellID in sorted(calibRunsDict.keys()):
        runItems = calibRunsDict[cellID]
        for runItem in runItems:
            for chunkItem in runItem['chunks']:
                # chunk file, run ID, chunk ID
                s  = f'{chunkItem["file"]} {runItem["runID"]} {chunkItem["chunkID"]:03}'
                # cellX, cellY
                s += f' {cellID[0]} {cellID[1]}'
                # avro file for this chunk
                s += f' {runItem["runID"]}-{chunkItem["chunkID"]:03}.avrodst.db'
                f.write(s + '\n')

# Cell input table
#

with open(cellInputTable, 'w') as f:
    f.write('# columns: cellX cellY chunks\n')
    for cellID in sorted(calibRunsDict.keys()):
        runItems = calibRunsDict[cellID]
        if not runItems: continue  # warn?
        s = f'{cellID[0]} {cellID[1]} '
        isFirst = True
        for runItem in runItems:
            for chunkItem in runItem['chunks']:
                if isFirst:
                    isFirst = False
                else:
                    s += ':'
                # chunk file, run ID, chunk ID
                s += f'{runItem["runID"]}-{chunkItem["chunkID"]:03}'
        f.write(s + '\n')


