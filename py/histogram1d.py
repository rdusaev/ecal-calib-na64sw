import numpy as np
import math, sys

class Histogram1D(object):
    """
    Simple 1D histogram implementation
    TODO: better to use something more elaborated?
    """
    def __init__(self, mn, mx, nBins):
        self._bins = np.zeros(nBins+2).astype('uint32')  # under/overflow bins
        self._ranges = [mn, mx, (mx-mn)/nBins]
        self._sum = [0., 0.]
        self._sqsum = [0., 0.]
        self._nItems = [0, 0]

    @property
    def binCenters(self):
        return np.linspace(self._ranges[2]/2 + self._ranges[0]
                , self._ranges[1] + self._ranges[2]/2
                , len(self._bins) - 2)

    @property
    def counts(self):
        return self._bins[1:-1]

    def add(self, v):
        self._nItems[1] += 1
        self._sum[1] += v
        self._sqsum[1] += v*v

        if v < self._ranges[0]:
            self._bins[0] += 1
            return
        if v > self._ranges[1]:
            self._bins[self._bins.shape[0] - 1] += 1
            return
        nBin = int((v - self._ranges[0])/self._ranges[2]) + 1
        assert nBin > 0
        assert nBin < self._bins.shape[0]
        self._bins[nBin] += 1
        self._nItems[0] += 1
        self._sum[0] += v
        self._sqsum[0] += v*v

    @property
    def mean(self):
        return self._sum[0]/self._nItems[0] if self._nItems[0] else np.nan

    @property
    def fullMean(self):
        return self._sum[1]/self._nItems[1] if self._nItems[1] else np.nan

    @property
    def variance(self):
        if self._nItems[0]:
            return math.sqrt( self._sqsum[0]/self._nItems[0] - self.mean*self.mean)
        else:
            return np.nan

    @property
    def fullVariance(self):
        if self._nItems[1]:
            return math.sqrt( self._sqsum[1]/self._nItems[1] - self.fullMean*self.fullMean)
        else:
            return self._nItems[0]

    @property
    def stddev(self):
        return math.sqrt(self.variance)

    @property
    def fullStddev(self):
        return math.sqrt(self.fullVariance)

    def print(self, f, stats=True):
        if stats:
            f.write(f'# in range: mean={self.mean:e} stddev={self.stddev:e}\n')
            f.write(f'# full: mean={self.fullMean:e} stddev={self.fullStddev:e}\n')
        for i in range(self._bins.shape[0]):
            if i != 0 and i != self._bins.shape[0] - 1:
                xLow = (i - 1 )*self._ranges[2] + self._ranges[0]
                xUp  = (i     )*self._ranges[2] + self._ranges[0]
            else:
                xLow = xUp = np.nan
            f.write(f'{i:3} {xLow:.2e} {xUp:.2e} {self._bins[i]}\n')
        

if __name__ == '__main__':
    h = Histogram(0, 1, 3)

    h.add(-1)
    h.add(-1e-12)
    h.add(0)
    h.add(1e-12)
    h.add(0.21)
    h.add(.67)
    h.add(.69)
    h.add(99)
    h.add(1)
    h.add(.9)
    h.add(1.1)
    h.add(2)

    h.print(sys.stdout)

